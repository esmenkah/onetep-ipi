\documentclass[english]{article}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage[letterpaper]{geometry}
\geometry{verbose,lmargin=3cm,rmargin=3cm}
\usepackage{amstext}
\usepackage[parfill]{parskip}

\makeatletter
\newcommand{\lyxmathsym}[1]{\ifmmode\begingroup\def\b@ld{bold}
  \text{\ifx\math@version\b@ld\bfseries\fi#1}\endgroup\else#1\fi}
\makeatother

\usepackage{babel}


\begin{document}

\title{Implicit solvation in ONETEP}


\author{Jacek Dziedzic}


\date{June 2011, last updated August 2016}

\maketitle
This manual pertains to newer ONETEP versions -- v4.4.$x$, where $x\ge6$; and v4.5.1.0 and later. These versions feature a number of changes to defaults and input syntax. If you are using an older version of ONETEP, please consult ``Implicit Solvation (older versions)'' instead.

\section{The model}

For a brief overview of the model used in ONETEP, see \cite{EPL}. For a more detailed description, including the optimal choice of parameters, see \cite{lysozyme}.

ONETEP includes solvation effects by defining a smooth dielectric
cavity around the solvated molecule. In contrast to PCM-based approaches,
the transition from a dielectric permittivity of 1 to the bulk value
of the solvent is smooth, rather than discontinuous. Thus, there is no
\textquotedblleft{}cavity surface\textquotedblright{}, strictly speaking,
but rather a thin region of space where the transition takes place.
The cavity and the transition are defined by a simple function relating
the dielectric permittivity, $\epsilon(r)$, to the electronic density,
$\rho(r)$, yielding an isodensity model. This function, defined in
\cite{Scherlis}, eq.7, and in \cite{EPL} eq.1 depends on three parameters \textendash{} $\epsilon_{\infty}$,
the bulk permittivity of the solvent; $\rho_{0}$, the electronic
density threshold, where the transition takes place and $\beta$,
which controls the steepness of the change in $\epsilon$. The nonhomogeneous
Poisson equation (NPE) is then solved to obtain the potential due
to the molecular density in the nonhomogeneous dielectric. A more general case, where salt ions can be added to the solvent (specified via concentrations), requires solving the Poisson-Boltzmann (PB) equation.

\subsection{The model: smeared-ions}

Because the NPE is solved for the molecular density, yielding the
molecular potential, a numerical trick termed the smeared-ion formalism
is used to reconcile this with the usual DFT way of thinking in terms
of valence-electronic and core densities separately. In this formalism
nuclear cores are modelled by narrow positive Gaussian distributions
and the usual energy terms are re-cast (cf.~\cite{lysozyme},~Appendix{}):
\begin{itemize}
\item the usual Hartree energy is now replaced by the ``molecular Hartree
energy'', that is the electrostatic energy of the molecule's total charge
distribution in the potential this charge distribution generates,
in the presence of dielectric, under open boundary conditions;
\item the local pseudopotential energy is corrected by an extra term that
takes the smeared-ion nature of the cores into account;
\item a self-interaction correction term is added to the total
energy to account for the added Gaussian distributions (each of them
self-interacts). This term does not depend on the electronic degrees of freedom, but depends on the ionic positions;
\item a non-self-interaction correction term is added to the total
energy to account for the added Gaussian distributions (they interact
with each other). This term does not depend on the electronic degrees of freedom, but depends on the ionic positions.
\end{itemize}
In principle, the total energy of the system is unchanged by the application
of the smeared-ion formalism, however, due to minor numerical inaccuracies
some discrepancies may be observed. These cancel out when calculating
energy differences between solvated and \emph{in vacuo} systems, provided
the smeared-ion formalism is used for the vacuum calculation as well.
\label{sec:smeared}
There is one parameter to the smeared-ion formalism, $\sigma$, which
controls the width of the Gaussians placed on the ions. See \cite{lysozyme} for more details on the choice of this parameter. The default value is almost always OK.

\subsection{The model: open boundary conditions}

In the smeared-ion formalism the molecular Hartree energy is not obtained
in reciprocal space, like in standard ONETEP calculations, but rather by solving
the Poisson equation (homogeneous in vacuum, nonhomogeneous in solution)
in real space, under open boundary conditions (BCs). This is another reason why solvated calculations should be compared only with \emph{in vacuo} calculations performed with smeared ions -- the boundary conditions need to be consistent for the energy differences to be physically meaningful.

Since the (molecular) Hartree energy is calculated under open boundary
conditions, the remaining energy terms must use open BCs for consistency.
The core-core energy is evaluated by means of a direct Coulombic sum
instead of the Ewald technique. The local pseudopotential term is
calculated in real-space rather than in reciprocal space. This happens
automatically once smeared ions are turned on. Solvation calculations
in ONETEP require no corrections due to periodicity (in contrast to the original
model of \cite{Scherlis}), as they are natively performed under open BCs.
On the other hand, solvation calculations using periodic or mixed boundary conditions
are currently not supported in ONETEP (but we're working on it).


\subsection{The model: self-consistently changing cavity}

Since the dielectric cavity is determined wholly by the electronic
density, it will change shape every time the electronic density changes.
From the physical point of view this is good, since it means the density
can respond self-consistently to the polarization of the dielectric
and vice versa. From the computational point of view this is rather inconvenient,
because it requires extra terms in the energy gradients (cf. \cite{Scherlis} eqs. 5 and 14). Because these terms vary rapidly over very localised regions of space, their accurate calculation requires unreasonably fine grids and becomes prohibitively difficult for larger molecules. On the other hand, neglecting these terms
while nevertheless changing the cavity shape in response to the changes
in the electronic density results in lack of convergence. Below we outline
three proposed solutions to this problem. 

\subsubsection{Fixed cavity}
A solution which is straightforward, but an approximation, consists in \textbf{fixing} the cavity and not allowing it to change
shape. This is realized by performing an \emph{in vacuo} calculation
first, then restarting a solvated calculation from a converged 
\emph{in vacuo} density.  In this way the
final \emph{in vacuo} density will be used to generate the cavity,
which will remain fixed for the duration of the solvated calculation.
This should yield solvation energies within several percent of the
accurate, self-consistent calculation, cf.~\cite{EPL}. As the cavity remains fixed,
the difficult extra terms no longer need to be calculated. The extra
error is small while the memory and CPU requirements are greatly
reduced (because the grid does not need to be made finer), thus this is the recommended solution. Note that \textbf{this is still
a self-consistent process} (the necessary terms are included in the
Hamiltonian), only the cavity is kept fixed.

The restarting can be performed in two fashions, either manually or automatically. For the manual approach, ensure you have \texttt{write\_denskern T}
and \texttt{write\_tightbox\_ngwfs T} in the vacuum calculation. Also
ensure you have \texttt{is\_smeared\_ion\_rep T}, as per \ref{sec:smeared}.
Once the calculation in vacuum is complete, restart in solvent by
adding \texttt{read\_denskern T}, \texttt{read\_tightbox\_ngwfs T}, and
\texttt{is\_implicit\_solvent T}. It's probably a good idea to run this on a copy, or else the restart files from the solvated calculation are going to overwrite the restart files from vacuum. You can also disable the writing out of restart files in solution.

A more elegant solution, is to use \texttt{is\_auto\_solvation T} alongside \texttt{is\_implicit\_solvent T}. This will automatically perform a vacuum calculation, save restart files, initiate a solvated calculation, read the restart files, generate the cavity and continue with a solvated calculation. In this case the restart files are given distinct extensions (e.g. \texttt{.dkn} vs. \texttt{.vacuum\_dkn}).

\subsubsection{Quasi-consistently-updated cavity}
The second solution, reducing the error by about a factor of two, but twice as awkward,
is to employ what we've termed quasi-self-consistent updating of the cavity. A fully converged
\emph{in vacuo} calculation is performed similarly to the first approach.
Again, the solvated calculation proceeds by means of a restart and
uses a fixed cavity, however it is terminated after several, say 3,
steps, writing out the new density. Another restart is then performed,
with the cavity being updated, as it is read from the saved files.
This is again terminated after, say 3, steps and the process is repeated
until convergence. In this way the cavity is updated in between restarts,
avoiding the need to calculate the difficult extra term. This, however,
means that the cavity is still fixed when calculating gradients, doing
line searches and so on, and the process is not strictly variational,
converging to an energy that is (expected to be) somewhere between
the approximation of approach one and the true self-consistent energy.
The awkwardness is ameliorated by the use of a script that automatically
does the restarts within one PBS job, so this does not require more
than one qsub. 

\subsubsection{Fully self-consistently-updated cavity}
The third solution is to perform calculations with
the cavity self-consistently responding to changes in density (as in \cite{Scherlis}), but as mentioned earlier, this is costly, because
it requires grids that are finer than default. This is achieved
by setting \texttt{is\_dielectric\_model SELF\_CONSISTENT} and
increasing \texttt{fine\_grid\_scale} to something like 3. You
might be able to get away with 2.5, you might need 3.5 or more.
The memory and CPU cost increase with the cube of this value (which is 2.0 by default). Ideally, one such calculation could
be performed to assess the magnitude of the error introduced by using
one of the two simpler approaches. This error in the free energy of
solvation is expected to be less than 3-4\% percent for charged species
and less than 1\% for neutral species, meaning calculations with a fixed
cavity are preferred.

\subsection{The model: cavitation energy}

The model in ONETEP includes the apolar cavitation term in the
solvent-accessible surface-area (SASA) approximation, thus assuming
the cavitation energy is proportional to the surface area of the cavity,
the constant of proportionality being the (actual physical) surface
tension of the solvent, $\gamma$, and the constant term being zero.
The cavitation energy term is calculated and added automatically,
unless \texttt{is\_include\_apolar~F} is explicitly stated. Surface tension
of the solvent has to be specified (otherwise the default for water
near room temperature will be used). This can be done using
\texttt{is\_solvent\_surf\_tension}. In contrast to previous versions,
the actual physical value of surface tension is now expected (e.g. about 
0.074 N/m for water).

\subsection{The model: dispersion-repulsion energy}

ONETEP's model allows for the solute-solvent dispersion-repulsion 
apolar energy term to be modeled approximately. This greatly improves
the quality of obtained solvation energies for uncharged molecules,
particularly so if they are large. This term is reasonably approximated
with the same SASA approach that cavitation uses, albeit with a smaller,
and negative, prefactor. In practice this is most easily achieved by 
simply scaling the cavitation term down by a constant factor. A good
scaling factor is 0.281705, which is what our model uses by default.
The keyword controlling this parameter is \texttt{is\_apolar\_scaling\_factor}
and its argument is a unitless value.

\section{The implementation}

The NPE is solved by means of a multigrid solver. Currently ONETEP
is interfaced to a solver called DL\_MG written by Lucian Anton.
The solver is distributed with ONETEP and is compiled in by default.
Solving the NPE is a memory- and time-consuming process, and you
should expect solvation calculations to take about 2-3 times longer
compared to standard ONETEP. The memory requirement of the solver grows cubically with the grid size along one dimension, so extra
vacuum/bulk solvent is not free anymore, as soon as smeared ions are
turned on!

The solver is only able to solve the NPE to second order. To ensure
the high-order accuracy necessary for solvation calculations, a defect-correction
technique is applied, which corrects the initial solution to a higher
order by invoking the solver several times. Consult \cite{lysozyme} for
more information.

Another limitation of the multigrid solver is the that grid sizes it 
uses are not created equal. Good grid sizes are divisible many times into
grids twice as small. For example a grid with 161 points (and so 160
grid-edges in between them) is an excellent choice, since it divides into two 
grids with 81 points (160 splits into two 80's), these divide into 
two grids with 41 points, which in turn divide into two grids with 21 points,
which divide into two grids with 11 points and so on. This lets the solver
use many multigrid levels, increasing efficiency. Now consider a grid with
174 points (and so 173 grid-edges). 173 is prime, and this grid cannot
be subdivided at all. 

Knowing about these limitations, ONETEP will sometimes slightly reduce
your fine grid dimensions when passing data to and from the multigrid solver.
This truncation always affects the right-hand side of the grid, and by default 
between 1 and 7 grid lengths will be truncated, to give the multigrid
enough flexibility. This is done automatically, and you will be informed
about the details like this:

\begin{verbatim}
ONETEP fine grid is 126 x 126 x 126 gridpoints, 29.0000 x 29.0000 x 29.0000 bohr.
FD multigrid is     121 x 121 x 121 gridpoints, 27.8492 x 27.8492 x 27.8492 bohr.
\end{verbatim}

Here ONETEP discarded just over 1 bohr from your system.

Even though this is done automatically, it is your responsibility to ensure that
nothing of significance (read: any charge density) is in the margin that is
thrown away. If your NGWFs extend into the margin, you're screwed. 

\subsection{Basic directives used in solvation calculations}
\begin{itemize}
\item \texttt{is\_implicit\_solvent T/F} \textendash{} turns on/off the
implicit solvent. Default is off.
\item \texttt{is\_include\_apolar T/F} \textendash{} turns on/off the
apolar energy terms. Default is on.
\item \texttt{is\_smeared\_ion\_rep T/F} \textendash{} turns on/off the
smeared-ion representation. Default is off, but if ONETEP detects you're
running a solvation calculation, it will turn it on for you and let you
off with a warning. When comparing results of two calculations (e.g.
results in vacuum and in solvent), always ensure this is set identically
in both calculations.
\item \texttt{is\_density\_threshold $x$} \textendash{} sets the solvation
parameter $\rho_{0}$ to $x$ (atomic units). The default is 0.00035, as per \cite{EPL}.
\item \texttt{is\_solvation\_beta $x$ }\textendash{} sets the solvation
parameter $\beta$ to $x$ (no unit). The default is 1.3, as per \cite{EPL}.
\item \texttt{is\_bulk\_permittivity $x$ }\textendash{} sets the solvation
parameter $\epsilon_{\infty}$ to $x$ (no unit). The default is 78.54
(suitable for water near room temperature and pressure and at low frequencies) 
if implicit solvent is on, and 1.0 is implicit solvent is off.
\item \texttt{is\_solvent\_surf\_tension $x$ }\textendash{} sets the
solvation parameter $\gamma$ to $x$ (unit must be supplied). The
default is 0.07415~N/m (which is suitable for water near room temperature). 
\item \texttt{is\_apolar\_scaling\_factor $x$ }\textendash{} controls
the scaling of the apolar term with the aim of taking solute-solvent
dispersion-repulsion into account. The default is 0.281075.
\item \texttt{is\_discretization\_order $x$ }\textendash{} sets the discretization
order used when solving the NPE to x (no unit). Available values are
2, 4, 6, 8, 10 and 12, the default is 8. With 2 no defect correction
is performed. Values of 4 and above employ defect correction. The
lowest values (2 and 4) are not recommended, because they offer poor
accuracy. Generally the largest value (12) will offer best accuracy,
but this has to be weighted against a likely drop in performance (higher
orders often take longer) and possibility of Gibbs-like phenomena
that may occur when high orders are used with steeply-changing dielectric
permittivity, as is the case for larger values of $\beta$. 8 or 10
is a good starting value. Results should not depend on the choice
of this parameter, but performance and multigrid convergence will.
See the troubleshooting section below for details. See \cite{lysozyme} for more details.
\item \texttt{is\_smearing\_width $x$} \textendash{} sets the width of
the smeared-ion Gaussians, $\sigma$, to $x$ (bohr). The default
is 0.8 and should be OK for most calculations. The results should
not depend on this parameter, but only if it's within rather narrow
limits of sensibility. Too high values (anything larger than 1.0,
roughly) are seriously unphysical, as they will lead to cores whose
Gaussian tails stick out of the electronic cloud, especially in hydrogen
atoms. This is very bad, since it does not change the energy \emph{in
vacuo} (the effect of the smearing, regardless of $\sigma$, is cancelled
by the correction terms to energy), but changes the energy in solution
(by polarising the solvent differently \textendash{} in reality the
cores are screened by the electons). Too low values (anything smaller
than 0.6, roughly), on the other hand, will lead to Gaussians so thin
and tall that they will become very difficult for the multigrid solver
to treat, requiring high orders and unreasonably fine grids to obtain
multigrid convergence. See \cite{lysozyme} for more details.
\item \texttt{is\_dielectric\_model FIX\_INITIAL/SELF\_CONSISTENT} \textendash{}
picks either the fixed cavity or the self-consistently changing cavity,
as described in \textquotedblleft{}The model: self-consistently changing
cavity\textquotedblright{}.
\item \texttt{fine\_grid\_scale $x$} \textendash{} makes the ONETEP fine grid $x$ (no unit) times as fine as the coarse grid, $x$ does not have to be an integer. The
solution of the NPE and associated finite-difference operations are
performed on (a subset of) the fine grid. Increasing \texttt{fine\_grid\_scale}
allows for making this grid finer without unnecessarily increasing
the kinetic energy cutoff of the calculation. The default is 2. Memory
and computational effort increase with the cube of $x$.
\item \texttt{is\_auto\_solvation $x$} \textendash{} automatically runs a vacuum calculation before any solvation calculation, thus relieving the user from the burden of manually restarting calculations. This attempts to automatically control the directives for restarting, running two calculations (vacuum and solvated) in succession. Using this directive is a must when doing implicit-solvent geometry optimisation, implicit-solvent molecular dynamics, implicit-solvent transition state search or implicit-solvent forcetest. Since v4.4 this directive is compatible with conduction calculations.

\end{itemize}

\subsection{Advanced directives used in solvation calculations}

The default settings usually work fine and the advanced settings should
only be changed if you know what you're doing.
\begin{itemize}
\item \texttt{is\_multigrid\_max\_iters $x$ }\textendash{} changes the
maximum number of multigrid iterations to $x$ (no unit). The default
is 100.
\item \texttt{is\_multigrid\_error\_tol $x$} \textendash{} changes the
error tolerance used for the termination condition for the multigrid
solver to $x$ (no unit). The default is 1E-5. Smaller values (like
1E-7) will add negligible increase in accuracy at a significant computational
cost. Larger values (like 1E-3) will incur significant loss of accuracy
at a significant reduction of computational cost.
\item \texttt{is\_multigrid\_defect\_error\_tol $x$ }\textendash{} changes
the error tolerance used for the termination condition for every defect
correction iteration in the multigrid solver to $x$ (no unit). The
default is 1E-2. Smaller values (like 1E-4) may be used to help in
corner cases where the solver does not converge, but should only be
used when necessary, since they increase the computational cost. Larger
values (like 1) might decrease computational cost, but can break convergence,
especially with higher orders.
\item \texttt{is\_bc\_coarseness $x$ }\textendash{} changes the size of
the blocks into which charge is coarsened when boundary conditions
are calculated. The default is 5. Smaller values may subtly increase
accuracy, but will incur a computational cost that grows as $x^{-3}$.
This can be perfectly acceptable for smaller molecules. For larger molecules
(1000 atoms and more) use 7 or more to reduce computational cost. For the effect of this parameter on accuracy, cf. \cite{lysozyme}. 
\item \texttt{is\_bc\_surface\_coarseness $x$ }\textendash{} changes the
size of the surface blocks onto which charge is interpolated when
boundary conditions are calculated. The default is 1 and is recommended. Larger values
will improve computational cost (that grows as $x^{-2}$),
but may decrease accuracy, especially for charged molecules. If possible,
it's better to use \texttt{is\_bc\_coarseness $x$} with a larger
$x$ to speed up the calculation.
\item \texttt{is\_separate\_restart\_files} (new in 3.5.3.2) \textendash{} allows a different set of restart files to be used to construct the dielectric cavity in solvent, from the set of restart files to be used to construct the density. This is useful if you need to restart a solvated calculation, but still want to construct the cavity from the converged vacuum density, and not the partially-converged solvated density.
\end{itemize}

\subsection{Expert directives used in solvation calculations}

These will only be listed here and not discussed. The last three directives are discussed in a separate document devoted to the real space local pseudopotential.
\begin{itemize}
\item \texttt{is\_surface\_thickness,}
\item \texttt{is\_bc\_threshold,}
\item \texttt{is\_core\_width,}
\item \texttt{is\_check\_solv\_energy\_grad,}
\item \texttt{openbc\_pspot\_finetune\_nptsx,}
\item \texttt{openbc\_pspot\_finetune\_f,}
\item \texttt{openbc\_pspot\_finetune\_alpha.}
\end{itemize}

\subsection{Can you do solvation forces yet? Can you do geometry optimisation?}
Yes! Since 3.5.8.0 the force terms due to implicit solvent should be automatically calculated any time you do standard force calculations. The formulas employed are exact (to numerical accuracy) when a fully-self-consistently-updated cavity is used. For the case of a fixed cavity, they are approximate. The approximation is very good, but initial tests suggest that you might not be able to converge the geometry to typical thresholds -- although the noise in the forces will be small, it might be enough close to equilibrium to throw off the geometry optimiser.

You should be able to do geometry optimisation and molecular dynamics without any problems with implicit solvent, provided that you use \texttt{is\_auto\_solvation T}. Note that restarting these might be tricky if they are interrupted during the in-solvent stage -- you will need to ensure the correct restart files (the vacuum restart files) are used to generate the solvent cavity upon restart.

Smeared-ion forces in vacuum are also implemented.

\subsection{Can you do implicit solvation under PBCs or mixed boundary conditions?}
Not yet, but we plan to.

\subsection{Can you do solvents other than water?}
Yes, provided you know the dielectric permittivity of the solvent and its surface tension. Accuracy has not been extensively tested, but it should work.

\section{Various hints for a successful start}
\begin{itemize}
\item Use one of the examples provided as a starting point.
\item Make sure both your vaccum and solvated calculations use smeared ions.
\item Make sure the parameters of both your vacuum and solvated calculations
are identical (box sizes, KE cutoffs, \texttt{k\_zero}, \texttt{is\_discretization\_order,
is\_smeared\_ion\_width, is\_bc\_coarseness, is\_bc\_surface\_coarseness}). Or just use \texttt{is\_auto\_solvation T}.
\item Choose \texttt{FIX\_INITIAL} over \texttt{SELF\_CONSISTENT} for \texttt{is\_dielectric\_model}.
\item Use an \texttt{is\_discretization\_order} of 8 and \texttt{is\_smearing\_width}
of 0.8. Specify them explicitly, as the defaults may change in the future.
\item Do not mess with expert directives.
\item Have at least about 10 bohr of vacuum/solvent around your molecule's
NGWFs (not atomic positions) on each side of the simulation cell.
Minimize the margin discussed in \textquotedblleft{}The implementation\textquotedblright{}.
\item Always start your calculation in solution as a restart from a fully
converged \emph{in vacuo} calculation. Or just use \texttt{is\_auto\_solvation T}.
\end{itemize}

\section{Troubleshooting: Problems, causes and solutions}
\begin{itemize}
\item \textbf{Problem}: ONETEP crashes (MPI gets killed) when evaluating
the boundary conditions or solving the NPE.\\
\textbf{Cause (1)}: You've run out of memory and the OOM killer
killed the calculation. Solving the NPE represents the peak memory
usage of the calculation.\\
\textbf{Solution (1)}: Increase memory or decrease box size or decrease grid fineness.\\
\textbf{Cause (2)}: You've run out of stack space. Solving the NPE represents the peak stack usage of the calculation.\\
\textbf{Solution (2)}: Increase stack size using \texttt{ulimit -s}. Make sure you do that on compute nodes, not the login node.
\item \textbf{Problem}: Multigrid calculation does not converge or converges
very slowly. Multigrid iterations are the ones denoted with \textquotedblleft{}MG\textquotedblright{}
\textendash{} they consist in defect-correcting the second-order solution
to the NPE. Normally the multigrid calculation should converge within
several iterations (in vacuum) and umpteen iterations (in solution).
If it takes more iterations, the multigrid struggles to converge.
In really bad cases it will diverge, which will stop the calculation
with an error message.\\
\textbf{Cause (1)}: Charge is not correctly localized (cell is
too small).\\
\textbf{Solution (1)}: Check and fix the cell size, paying attention
to the margin between the MG grid and fine grid.\\
\textbf{Cause (2)}: Dielectric permittivity too steeply changing
on the cavity boundary for the current grid size, finite differences
struggling to approximate the changes. This is the culprit if the
calculation ran fine \emph{in vacuo} but struggles in solvent.\\
\textbf{Solution (2)}: Preferable, but painful, solution is to
make the grid finer (\texttt{fine\_grid\_scale}). Otherwise an increase
or decrease of discretization order may help (make sure it stays consistent
across your calculations, though). A parametrization with lower \texttt{is\_solvation\_beta}
and \texttt{is\_density\_threshold} will usually help (make sure it
stays consistent across your calculations, though).\\
\textbf{Cause (3)}: The smearing width is too small, making the
smeared cores too thin and tall, which is difficult for the finite
differences. This is the culprit if the calculation also struggles
\emph{in vacuo}.\\
\textbf{Solution (3)}: Increasing \texttt{is\_smearing\_width} will help
(but mind the consequences), if it was too small in the first place.
Increasing the discretization order will help (especially if you've
been using less than 10), but might lead to a similar problem (Cause
(2)) in solution.\\
\textbf{Cause (4)}: Too lax setting for \texttt{is\_multigrid\_defect\_error\_tol}.\\
\textbf{Solution (4)}: Decrease \texttt{is\_multigrid\_defect\_error\_tol}
to 1E-3 or less.
\item \textbf{Problem}: Calculation struggles to converge LNV or NGWFs or
does not converge at all. RMS gradient stalls.\\
\textbf{Cause (1)}: If you're using \texttt{is\_dielectric\_model
SELF\_CONSISTENT}, then this is normal, unless your grid is ridiculously
fine (you will need \texttt{psinc\_spacing 0.5} and \texttt{fine\_grid\_scale 3} or
better, as a rule of thumb).\\
\textbf{Solution (1)}: Prefer \texttt{is\_dielectric\_model FIX\_INITIAL}.
If you definitely want \texttt{is\_dielectric\_model SELF\_CONSISTENT},
make the grid finer and have a lot of memory.\\
\textbf{Cause (2)}: Density kernel is not converged enough.\\
\textbf{Solution (2)}: \texttt{Try minit\_lnv 6} and \texttt{maxit\_lnv 6} (for smaller
molecules) or \texttt{minit\_lnv 10} and \texttt{maxit\_lnv 10} (for
large molecules). 
\end{itemize}

\section{What are the values for the model parameters?}

Two sets of values will be proposed here. The first one will be called
\textquotedblleft{}high-beta\textquotedblright{} parametrization.
It offers the best quality (in terms of r.m.s. error from experiment)
for both charged and neutral species. The drawback is that the high
value of $\beta$ means the multigrid convergence is poor and it often
takes a while to converge. Or it may not converge. This should be your 
first choice \textbf{only} if accuracy trumps anything else. The parameters are:

\texttt{is\_solvation\_beta 1.6}

\texttt{is\_density\_threshold 0.00055}

The second parametrization, called \textquotedblleft{}low-beta\textquotedblright{}
should pose no problems to the multigrid solver under any circumstances.
Quality should be only marginally worse for anions and neutrals and
comparable or better for cations. These are the default parameters, and
thy are:

\texttt{is\_solvation\_beta 1.3}

\texttt{is\_density\_threshold 0.00035}

Both parametrizations assume \texttt{is\_bulk\_permittivity 78.54},
which is suitable for water. It should be noted that the model is deficient in
its treatment of anions, consistently underestimating the magnitude
of the solvation effect by 10-25\%. Work is ongoing to fix this, until
then a different parametrization may be used if one is only interested
in anionic species.

\section{New in version 4.4}
ONETEP version 4.4 includes a number of changes to the DL\_MG solver
that should be transparent to end-users (apart from improvements
in performance). There are a few visible changes, outlined below.
\begin{itemize}
 \item \texttt{is\_multigrid\_error\_damping T/F }can be used to turn on 
error damping in the defect correction procedure. This is often necessary
when solving the full (non-linearised) Poisson-Boltzmann equation, but will
likely not do much for the linearised Poisson-Boltzmann equation or
for the Poisson equation (the latter being the only supported scenario
currently). Accordingly, the default depends on \texttt{is\_pbe }and is 
\texttt{F }for \texttt{is\_pbe NONE }and \texttt{is\_pbe LINEARISED},
and \texttt{T }for \texttt{is\_pbe FULL}.
 \item \texttt{is\_multigrid\_verbose T/F }can be used to output
cross-sections of quantities that are of interest during multigrid calculations
to text files. For instance it might be desirable to examine the permittivity
to verify whether a pocket in a molecule is solvent accessible or not.
The cross sections are always performed along the X direction,
for a given value of Y and Z. The following quantities are output:
\begin{itemize}
\item Total density (rho),
\item Boundary condtion for the potential (bound),
\item Dielectric permittivity at gridpoint (eps\_full),
\item Dielectric permittivity offset by half grid point along X (eps\_half\_x),
\item Dielectric permittivity offset by half grid point along Y (eps\_half\_y),
\item Dielectric permittivity offset by half grid point along Z (eps\_half\_z),
\item Steric potential (steric\_pot) -- only for \texttt{is\_pbe }different from \texttt{NONE}.
\item Steric weight a.k.a. accessibility (steric\_w) -- only for \texttt{is\_pbe }different from \texttt{NONE}. 
\end{itemize}
In addition the following quantities are output (to a separate file) for every defect correction iteration:
\begin{itemize}
\item Total defect (defect),
\item Sum of source and Poisson contributions to the defect (defect\_stpt),
\item Boltzmann contribution to the defect (defect\_bt).
\item The potential value at which the derivative of the Boltzmann term is computed (der\_pot) -- only for \texttt{is\_pbe }different from \texttt{NONE}.
\end{itemize}
\item \texttt{is\_multigrid\_verbose\_y $y$} \textendash{} used in combination with \texttt{is\_multigrid\_verbose}. Specifies the offset along the Y axis to be $y$ (make sure you provide units).
\item \texttt{is\_multigrid\_verbose\_z $z$} \textendash{} used in combination with \texttt{is\_multigrid\_verbose}. Specifies the offset along the Z axis to be $z$ (make sure you provide units).
\end{itemize}

The following keywords control the experimental Poisson-Boltzmann solver functionality, which allows performing calculations in implicit solvent containing Boltzmann ions. While this functionality works to the best of our knowledge, we do not provide support for it yet. Below is the bare list of keywords for controlling it. These will be described further once this functionality becomes supported.

\begin{itemize}
 \item \texttt{is\_pbe},
 \item \texttt{is\_pbe\_temperature},
 \item \texttt{is\_pbe\_exp\_cap},
 \item \texttt{is\_pbe\_use\_fas},
 \item \texttt{is\_pbe\_bc\_debye\_screening},
 \item \texttt{is\_steric\_write},
 \item \texttt{is\_hc\_steric\_cutoff},
 \item \texttt{is\_sc\_steric\_cutoff},
 \item \texttt{is\_sc\_steric\_magnitude},
 \item \texttt{is\_sc\_steric\_smoothing\_alpha},
 \item \texttt{sol\_ions}.
\end{itemize}


\section{Questions?}

Questions should be directed to Jacek Dziedzic, \texttt{jd12g09[-at-]soton.ac.uk.}
\begin{thebibliography}{3}
\bibitem{Scherlis}Scherlis, Fattebert, Gygi, Cococcioni and Marzari,
J. Chem. Phys. \textbf{124} (2006).

\bibitem{Floris}Floris, Tomasi and Ahuir, J. Comp. Chem. \textbf{12}
(1991).

\bibitem{EPL}Dziedzic, Helal, Skylaris, Mostofi and Payne, EPL \textbf{95} (2011).

\bibitem{lysozyme}Dziedzic, Fox, Fox, Tautermann and Skylaris, International Journal of Quantum Chemistry \textbf{113} issue 6 (2013).

\end{thebibliography}

\end{document}
