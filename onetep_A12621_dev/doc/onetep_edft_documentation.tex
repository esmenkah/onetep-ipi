\documentclass[dvipdfm, a4paper, 12pt]{article}

% packages in use
\usepackage[english]{babel}
\usepackage{color}
\usepackage{graphicx}
\usepackage{times}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{tensor}
\usepackage{braket}
\usepackage{notoccite}
\usepackage{subfigure}
\usepackage[T1]{fontenc}
\usepackage{t1enc}
\usepackage{anyfontsize}
\usepackage[a4paper, portrait, dvips, textwidth=15cm,
textheight=23cm, headheight=1cm, bindingoffset=0cm, bottom=3cm]{geometry}
\usepackage[sort&compress]{natbib}
\usepackage[nottoc]{tocbibind}
\usepackage{hypernat}
\usepackage{hyperref}

\hypersetup{
    colorlinks,
    citecolor=blue,
    filecolor=black,
    linkcolor=blue,
    urlcolor=black,
    linktocpage=true
}

% new commands
\newcommand{\onetep}{\textsc{Onetep}}
\newcommand{\KS}{Kohn-Sham}
\newcommand{\braces}[1]{\left\lbrace #1 \right\rbrace}
\newcommand{\mm}[2]{\tensor{M}{^#1_#2}}
\newcommand{\mmt}[2]{\tensor*{M}{^\dagger_#2^#1}}
\newcommand{\mmn}[3]{\tensor{M}{^#1_#2^{(#3)}}}
\newcommand{\mmtn}[3]{\tensor*{M}{^\dagger_#2^#1^{(#3)}}}
\newcommand{\temperature}{\mathcal{T}}
\newcommand{\boltzmann}{k_\textrm{B}}
\newcommand{\lapack}{\textsc{Lapack}}
\newcommand{\scalapack}{\textsc{ScaLapack}}

\newcommand{\bool}[2]{[Boolean, default \texttt{#1: #2}].}
\newcommand{\intg}[2]{[Integer, default \texttt{#1: #2}].}
\newcommand{\real}[2]{[Real, default \texttt{#1: #2}].}
\newcommand{\rphy}[3]{[Real physical, default \texttt{#1: #2 #3}].}

%opening
\title{Finite-temperature DFT calculations using the Ensemble-DFT method in
ONETEP}
\author{\'{A}lvaro Ruiz Serrano}
\date{\today}

\begin{document}

% \fontsize{13}{15.6}\selectfont
\maketitle


\tableofcontents % content

\section{Basic principles}

This manual describes how to run finite-temperature calculations using
{\onetep}. A recent implementation uses a direct minimisation technique based
on the Ensemble-DFT method \cite{MarzariVanderbiltPayne}. The Helmholtz free
energy functional is minimised in two nested loops. The inner loop performs a
line-search in the space of Hamiltonian matrices (in a similar fashion as
described in Ref. \cite{FreysoldtBoeckNeugebauer}), for a fixed set of NGWFs.
Then, the outer loop optimises the NGWFs psinc expansion coefficients for a
fixed density kernel. For a more detailed description and discussion of this
method in {\onetep}, see Ref. \cite{metals_onetep}.

\vspace{1\baselineskip}\vspace{-\parskip}

Using the NGWF representation, the Helmholtz free energy functional
becomes:

\begin{align}\label{eq:freeenergy2}
A_\temperature \left[\braces{H_{\alpha\beta}},\braces{\ket{\phi_\alpha}}\right]
= E\left[\braces{H_{\alpha\beta}},\braces{\ket{\phi_\alpha}}\right] -
\temperature S\left[\braces{f_i}\right].
\end{align}

\noindent where $\braces{H_{\alpha\beta}}$ is the NGWF representation
of the Hamiltonian matrix, $\braces{\ket{\phi_\alpha}}$ is the
current set of NGWFs, $\temperature$ is the electronic temperature,
$E\left[\braces{H_{\alpha\beta}},\braces{\ket{\phi_\alpha}}\right]$ is the
energy functional and $S\left[\braces{f_i}\right]$ is an entropy term. The
occupancies of the {\KS} states, $\braces{f_i}$ are calculated from the energy
levels, $\braces{\epsilon_i}$, using the Fermi-Dirac distribution:

\begin{equation}\label{eq:fermidirac}
 f_i\left(\epsilon_i\right) = \left( 1 + \exp\left[\dfrac{\epsilon_i -
 \mu}{\boltzmann \temperature }\right] \right)^{-1}.
\end{equation}

\noindent where $\mu$ is the Fermi level. 
To obtain the energy eigenvalues, $\braces{\epsilon_i}$, the Hamiltonian matrix
is diagonalised as:

\begin{equation}\label{eq:hamdiag}
 H_{\alpha\beta} \mm{\beta}{i} = S_{\alpha\beta} \mm{\beta}{i} \epsilon_i,
\end{equation}

\noindent where $\braces{S_{\alpha\beta}}$ are the elements of the NGWF
overlap matrix, and $\braces{\mm{\beta}{i}}$ are the expansion coefficients
of the {\KS} eigenstates in the NGWF basis set. At the moment, this is a
cubic-scaling operation that requires dealing dense matrices, which makes
it memory-demanding.


\section{Compilation}

By default, {\onetep} is linked against the {\lapack} library \cite{lapack_web}
for linear algebra. The {\lapack} eigensolver DSYGVX \cite{DSYGVX}, can only be
executed in one CPU at a time. Therefore, EDFT calculations with {\lapack} are
limited to small systems (a few tens of atoms). Calculations on large systems
are possible if, instead, {\onetep} is linked against {\scalapack} library
\cite{scalapack_web} during compilation time. The {\scalapack} eigensolver,
PDSYGVX, can be run in parallel using many CPUs simultaneously. Moreover,
{\scalapack} can distribute the storage of dense matrices across many CPUs, thus
allowing to increase the total memory allocated to a given calculation in a
systematic manner, simply by requesting more processors. For the compilation
against {\scalapack} to take effect, the flag \texttt{-DSCALAPACK} must be
specified during the compilation of {\onetep}. 

\section{Commands for the inner loop}

\subsection{Basic setup}

\begin{itemize}

\item \texttt{edft: T/F}\newline
\bool{edft}{F}\newline
If true, it enables Ensemble-DFT calculations.

\item \texttt{edft\_maxit: n}\newline
\intg{edft\_maxit}{10}\newline
Number of EDFT iterations in the {\onetep} inner loop.

\item \texttt{edft\_smearing\_width: x \textit{units}}.\newline
\rphy{edft\_smearing\_width}{0.1}{eV}\newline
Sets the value of the smearing width, $\boltzmann \temperature$, of the
Fermi-Dirac distribution. It takes units of energy (eV, Hartree) or
temperature. For example, \newline
\texttt{edft\_smearing\_width: 1500 K} \newline
will set $\temperature =$ 1500 degree Kelvin.

\end{itemize}

\subsection{Tolerance thresholds}

\begin{itemize}

\item \texttt{edft\_free\_energy\_thres: x \textit{units}}.\newline
\rphy{edft\_free\_energy\_thres}{1.0e-6}{Hartree}\newline
Maximum difference in the Helmholtz free energy functional between two
consecutive iterations. 

\item \texttt{edft\_energy\_thres: x \textit{units}}.\newline
\rphy{edft\_energy\_thres}{1.0e-6}{Hartree}\newline
Maximum difference in the energy functional between two consecutive iterations.

\item \texttt{edft\_entropy\_thres: x \textit{units}}.\newline
\rphy{edft\_entropy\_thres}{1.0e-6}{Hartree}\newline
Maximum difference in the entropy functional between two consecutive iterations.

\item \texttt{edft\_rms\_gradient\_thres: x}.\newline
\real{edft\_rms\_gradient\_thres}{1.0e-4}\newline
Maximum RMS gradient $\dfrac{d A_\temperature}{d f_i}$.

\item \texttt{edft\_commutator\_thres: x \textit{units}}.\newline
\rphy{edft\_commutator\_thres}{1.0e-5}{Hartree}\newline
Maximum value of the Hamiltonian-Kernel commutator.

\item \texttt{edft\_fermi\_thres: x \textit{units}}.\newline
\rphy{edft\_fermi\_thres}{1.0e-3}{Hartree}\newline
Maximum change in the Fermi energy between two consecutive iterations.

\end{itemize}

\subsection{Advanced setup}

\begin{itemize}
\item \texttt{edft\_extra\_bands: n}.\newline
\intg{edft\_extra\_bands}{-1}\newline
Number of extra energy bands. The total number of bands is equal to the number
of NGWFs plus \texttt{edft\_extra\_bands}. When set to a negative number, no
extra bands are added.

\item \texttt{edft\_round\_evals: n}.\newline
\intg{edft\_round\_evals}{-1}\newline
When set to a positive integer value, the occupancies that result from the
Fermi-Dirac distribution are rounded to \texttt{n} significant figures. This
feature can reduce some numerical errors arising from the grid-based
representation of the NGWFs.

\item \texttt{edft\_write\_occ: T/F}.\newline
\bool{edft\_write\_occ}{F}\newline
Save fractional occupancies in a file.

\item \texttt{edft\_max\_step: x}.\newline
\real{edft\_max\_step}{1.0}\newline
Maximum step during the EDFT line search. 

\end{itemize}

\section{Commands for the outer loop}

The standard {\onetep} commands for NGWF optimisation apply to the EDFT
calculations as well. The only flag that is different is:

\begin{itemize}
\item \texttt{ngwf\_cg\_rotate: T/F}.
\intg{ngwf\_cg\_rotate}{T}
This flag is always true in EDFT calculations. It ensures that the eigenvectors
$\mm{\beta}{i}$ are rotated to the new NGWF representation once these are
updated.
\end{itemize}


\section{Restarting an EDFT calculation}

\begin{itemize}

\item \texttt{write\_hamiltonian: T/F}.\newline
\bool{write\_hamiltonian}{F}\newline
Save the last Hamiltonian matrix on a file.

\item \texttt{read\_hamiltonian: T/F}.\newline
\bool{read\_hamiltonian}{F}\newline
Read the Hamiltonian matrix from a file, and continue the calculation from this
point.

\item \texttt{write\_tightbox\_ngwfs: T/F}.\newline
\bool{write\_tightbox\_ngwfs}{T}\newline
Save the last NGWFs on a file.

\item \texttt{read\_tightbox\_ngwfs: T/F}.\newline
\bool{read\_tightbox\_ngwfs}{F}\newline
Read the NGWFs from a file and continue the calculation from this point.

\vspace{1\baselineskip}\vspace{-\parskip}

\noindent \underline{\textbf{NOTE:}} if a calculation is intended to be
restarted at some point in the future, then run the calculation with
\\
\texttt{write\_tightbox\_ngwfs: T}\\
\texttt{write\_hamiltonian: T}\\
to save the Hamiltonian and the NGWFs on disk. Two new files will be created,
with extensions \texttt{.ham} and \texttt{.tightbox\_ngwfs},
respectively. Then, to restart the calculation, set\\
\texttt{read\_tightbox\_ngwfs: T}\\
\texttt{read\_hamiltonian: T}\\
to tell {\onetep} to read the files that were previously saved on disk. Remember
to keep a backup of the output of the first run before restarting the
calculation.

\vspace{1\baselineskip}\vspace{-\parskip}

\noindent \underline{\textbf{NOTE:}} the density kernel is not necessary to
restart an EDFT calculation. However, it is necessary to calculate the
electronic properties of the system, once the energy minimisation has completed.
To save the density kernel on a file, set:
\texttt{write\_denskern: T}\\
to generate a \texttt{.dkn} file containing the density kernel. To read in the
density kernel, set\\
\texttt{read\_denskern: T}\\


\end{itemize}

\section{Controlling the parallel eigensolver}

Currently, only the {\scalapack} PDSYGVX parallel eigensolver is available. A
complete manual to this routine can be found by following the link in Ref.
\cite{PDSYGVX}. If {\onetep} is interfaced to {\scalapack}, the following
directives can be used:

\begin{itemize}

\item \texttt{eigensolver\_orfac: x}.\newline
\real{eigensolver\_orfac}{1.0e-4}\newline
Precision to which the eigensolver will orthogonalise degenerate Hamiltonian
eigenvectors. Set to a negative number to avoid reorthogonalisation with the
{\scalapack} eigensolver.

\item \texttt{eigensolver\_abstol: x}.\newline
\real{eigensolver\_abstol}{1.0e-9}\newline
Precision to which the parallel eigensolver will calculate the eigenvalues. Set
to a negative number to use {\scalapack} defaults.

\end{itemize}

The abovementioned directives are useful in calculations where the
{\scalapack} eigensolver fails to orthonormalise the eigenvectors. In such
cases, the following error will be printed in the input file:

\vspace{1\baselineskip}\vspace{-\parskip}

\noindent \texttt{(P)DSYGVX in subroutine dense\_eigensolve returned info=   
2}.

\vspace{1\baselineskip}\vspace{-\parskip}

\noindent Many times (although not always) this error might cause the
calculation to fail. If this situation occurs, set\\
\texttt{eigensolver\_orfac: -1}\\
\texttt{eigensolver\_abstol: -1}\\
in the input file and restart the calculation. {\scalapack} will not
reorthonormalise the eigenvectors. Instead, an external L\"{o}wdin
orthonormalisation process \cite{lowdin:365} will be triggered. This is usually
more efficient for larger systems.

% bibliography
\pagebreak
\begin{thebibliography}{1}

\bibitem{MarzariVanderbiltPayne}
N.~Marzari, D.~Vanderbilt, and M.~C. Payne.
\newblock {Ensemble density-functional theory for ab initio molecular dynamics
  of metals and finite-temperature insulators}.
\newblock {\em {Phys. Rev. Lett.}}, {79}({7}):1337--1340, {1997}.

\bibitem{FreysoldtBoeckNeugebauer}
C.~Freysoldt, S.~Boeck, and J.~Neugebauer.
\newblock {Direct minimization technique for metals in density functional
  theory}.
\newblock {\em {Phys. Rev. B}}, {79}({24}):241103, {2009}.

\bibitem{metals_onetep}
{\'A}.~Ruiz-Serrano and C.-K. Skylaris.
\newblock A variational method for density functional theory calculations on
  metallic systems with thousands of atoms.
\newblock {\em \\J. Chem. Phys.}, 139(5):054107, 2013.

\bibitem{lapack_web}
Lapack.
\newblock http://www.netlib.org/lapack/.

\bibitem{DSYGVX}
{Lapack DSYGVX eigensolver}.
\newblock http://netlib.org/lapack/double/dsygvx.f.

\bibitem{scalapack_web}
{ScaLapack}.
\newblock http://www.netlib.org/scalapack/.

\bibitem{PDSYGVX}
{ScaLapack PDSYGVX eigensolver}.
\newblock http://www.netlib.org/scalapack/double/pdsygvx.f.

\bibitem{lowdin:365}
Per-Olov Lowdin.
\newblock On the non-orthogonality problem connected with the use of atomic
  wave functions in the theory of molecules and crystals.
\newblock {\em J. Chem. Phys.}, 18(3):365--375, 1950.

\end{thebibliography}


\end{document}
