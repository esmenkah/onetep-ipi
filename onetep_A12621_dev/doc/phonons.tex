\documentclass[10pt,a4paper,onecolumn]{article}

\usepackage{amsmath}
\usepackage{fancyvrb}

\begin{document}

\title{Phonon calculations in {\sc onetep}}
\author{Fabiano Corsetti}
\date{September 20, 2013 (v2)}

\maketitle

\section{Theory}

We make use of the harmonic approximation, in which the total energy of the system $E^\mathrm{tot}$ is expanded to quadratic order in the displacement of the ions about their equilibrium positions:
\begin{equation}
E^\mathrm{tot} = E^\mathrm{eq} + \frac{1}{2} \sum_{a,\alpha,\kappa,a',\alpha',\kappa'} u_{a,\alpha,\kappa} \phi_{\kappa,\kappa'}^{\alpha,\alpha'} \left ( a,a' \right ) u_{a',\alpha',\kappa'},
\end{equation}
where $E^\mathrm{eq}$ is the equilibrium energy and $u_{a,\alpha,\kappa}$ denotes a small displacement of ion $\alpha$ belonging to unit cell $a$ in the Cartesian coordinate direction $\kappa$ from its equilibrium position; $\boldsymbol{\phi} \left ( a,a' \right )$ is known as the force constants matrix, defined as
\begin{equation}
\phi_{\kappa,\kappa'}^{\alpha,\alpha'} \left ( a,a' \right ) = \frac{\partial^2 E}{\partial u_{a,\alpha,\kappa} \partial u_{a',\alpha',\kappa'}}.
\end{equation}
It can be shown that the phonon frequencies $\omega_{\mathbf{q},n}$ at wavevector $\mathbf{q}$ are the eigenvalues of the dynamical matrix $\mathbf{D} \left ( \mathbf{q} \right )$, which can be calculated from the Fourier transform of the force constants matrix:
\begin{equation} \label{eq:dynamical_mat}
D_{\kappa,\kappa'}^{\alpha,\alpha'} \left ( \mathbf{q} \right ) = \frac{1}{\sqrt{M_\alpha M_{\alpha'}}} \sum_a \phi_{\kappa,\kappa'}^{\alpha,\alpha'} \left ( a,0 \right ) \mathrm{e}^{-\mathrm{i} \mathbf{q} \cdot \mathbf{R}_a},
\end{equation}
where $M_\alpha$ is the mass of ion $\alpha$ and $\mathbf{R}_a$ is the lattice vector displacement for unit cell $a$. The vibrational free energy for the unit cell is then given by
\begin{equation} \label{eq:free_energy}
F \left ( T \right ) = \frac{1}{2} \sum_{\mathbf{q},n} \omega_{\mathbf{q},n} + k_\mathrm{B} T \sum_{\mathbf{q},n} \ln{\left ( 1-\mathrm{e}^{-\omega_{\mathbf{q},n}/k_\mathrm{B} T} \right )},
\end{equation}
where the first term is the zero-point energy of the system, and the second term is the temperature-dependent part of the free energy. In the limit of an infinite periodic system the sum over $\mathbf{q}$ should be replaced by an integral of the phonon dispersion curves over the first Brillouin zone.

The \texttt{phonon} module in {\sc onetep} uses the {\em finite-displacement} method to calculate the phonon frequencies of the system; for molecules (the default), only the $\Gamma$-point frequencies $\omega_{\mathbf{0},n}$ are calculated, while for supercells of bulk crystal, any arbitrary q point $\omega_{\mathbf{q},n}$ can be calculated. The elements of the force constants matrix are calculated by a central-difference formula, using either 2 (the default) or 4 displacements:
\begin{equation} \label{eq:fd}
\begin{cases}
\phi_{\kappa,\kappa'}^{\alpha,\alpha'} \approx \frac{F_{\alpha,\kappa}^+ - F_{\alpha,\kappa}^-}{2d} & \text{\texttt{phonon\_sampling 1} (2 disps.)} \\
\phi_{\kappa,\kappa'}^{\alpha,\alpha'} \approx \frac{- F_{\alpha,\kappa}^{2+} + 8 F_{\alpha,\kappa}^+ - 8 F_{\alpha,\kappa}^- + F_{\alpha,\kappa}^{2-}}{12d} & \text{\texttt{phonon\_sampling 2} (4 disps.)}
\end{cases},
\end{equation}
where $F_{\alpha,\kappa}^\pm$ is the force on ion $\alpha$ in direction $\kappa$ caused by a displacement $\pm d$ of ion $\alpha'$ in direction $\kappa'$, and $F_{\alpha,\kappa}^{2\pm}$ is the same for a displacement $\pm 2d$. Therefore, $6N$/$12N$ calculations are needed in total, where $N$ is the number of atoms in the system. However, each of these calculations is simply a small perturbation on the equilibrium configuration. Therefore, the converged set of NGWFs $\left \{ \xi_\beta \left ( \mathbf{r} \right ) \right \}$ and density kernel $\mathbf{K}$ that are obtained from a preliminary ground-state calculation on the equilibrium structure are used as the starting guess for each of the displacement calculations.

\section{Overview of the \texttt{phonon} module}

A phonon calculation in {\sc onetep} is divided into three stages:
\begin{enumerate}
\item A ground-state calculation is performed for the unperturbed configuration, as specified in the input file. The forces on the ions are then calculated, and the code checks that the magnitude of the force on every ion is smaller than the value specified by \texttt{phonon\_fmax}, as the starting configuration must correspond to a minimum in the energy landscape for the phonon calculation to be meaningful. If this requirement is not met, the calculation is interrupted.
\item Each ion is displaced in turn in the $+$ve and $-$ve x-, y-, and z-directions by a distance $d$ (and, optionally, $2d$). For each displacement a separate ground-state calculation is performed. The initial description of the electronic structure is read in each time from the converged files \texttt{<seedname>.dkn} and \texttt{<seedname>.tightbox\_ngwfs} for the unperturbed structure that have been obtained from Stage 1; the overwriting of these files is therefore disabled at the start of Stage 2. After each set of $+$ve/$-$ve displacements, one row of the force constants matrix is calculated and written to the file \texttt{<seedname>.force\_consts\_<i>}, where \texttt{<i>} is the number identifier of the row (going from 1 to $3N$ for an $N$-atom system). It is important to note that {\em not all rows are necessarily computed}, if some vibrational degrees of freedom are switched off (Sec.~\ref{sec:free}), and/or a supercell calculation of bulk crystal is being performed (Sec.~\ref{sec:supercell}); however, \texttt{<i>} retains the same value as it would have if all $3N$ rows were to be used.
\item The rows of the force constants matrix are read back in from the files \texttt{<seedname>.force\_consts\_<i>}, and the full force constants matrix is constructed. The dynamical matrix can then be calculated for the desired q points and diagonalized to find the phonon frequencies. First, the phonon frequencies are calculated on a regular grid of q points ($\Gamma$ only for a molecule); in either case, the $\Gamma$-point frequencies only are printed to standard output. Then, the following thermodynamic quantities are calculated on the full grid and printed to standard output: the zero-point energy, and the free energy, entropy, internal energy, and specific heat within a user-specified temperature range. The phonon DOS is also calculated on the full grid and written to the file \texttt{<seedname>.qdos}. Additionally, the user can specify a list of arbitrary q points, for which the phonon frequencies (and, optionally, the corresponding eigenvectors) are calculated and written to the file \texttt{<seedname>.phonon\_freqs}. Finally, a list of $\Gamma$-point modes \texttt{<j>} can be specified for which animation files \texttt{<seedname>.phonon\_<j>.xyz} are written.
\end{enumerate}
This division in stages is done so as to allow for a {\em task farming} approach (see Sec.~\ref{sec:farm} for details).

\section{Selecting degrees of freedom} \label{sec:free}

\begin{table}[b]
\begin{center}
\begin{tabular}{cccc}
\hline
\hline
\texttt{phonon\_vib\_free}  & x          & y          & z          \\
\hline
\texttt{0}                  & \texttt{F} & \texttt{F} & \texttt{F} \\
\texttt{1}                  & \texttt{T} & \texttt{F} & \texttt{F} \\
\texttt{2}                  & \texttt{F} & \texttt{T} & \texttt{F} \\
\texttt{3}                  & \texttt{T} & \texttt{T} & \texttt{F} \\
\texttt{4}                  & \texttt{F} & \texttt{F} & \texttt{T} \\
\texttt{5}                  & \texttt{T} & \texttt{F} & \texttt{T} \\
\texttt{6}                  & \texttt{F} & \texttt{T} & \texttt{T} \\
\texttt{7}                  & \texttt{T} & \texttt{T} & \texttt{T} \\
\hline
\hline
\end{tabular}
\caption{Allowed options for keyword \texttt{phonon\_vib\_free}.}
\label{table:free}
\end{center}
\end{table}

The input file allows the user to select only a subset of the complete vibrational degrees of freedom of the entire system, as well as to specify different finite-displacement options for each $\left ( \alpha, \kappa \right )$ pair. This is controlled through two keywords: \texttt{phonon\_vib\_free} and \texttt{phonon\_exception\_list}.

\texttt{phonon\_vib\_free} is an integer parameter controlling the global default of which Cartesian directions are `switched on' for all ions. The options are listed in Table~\ref{table:free}. The default option is \texttt{7}, corresponding to all three Cartesian directions being switched on (i.e., all vibrational degrees of freedom are allowed).

\texttt{phonon\_exception\_list} is a block in which the user can list specific $\left ( \alpha, \kappa \right )$ pairs with options differing from the global defaults defined by \texttt{phonon\_vib\_free}, \texttt{phonon\_sampling}, and \texttt{phonon\_finite\_disp}. An example of doing so is as follows:
\begin{Verbatim}[frame=single]
phonon_vib_free 3
phonon_sampling 1
phonon_finite_disp 1.4e-1 bohr

%block phonon_exception_list
10 3 1 2 0.9
15 1 0 1 1.0
36 2 0 1 1.0
%endblock phonon_exception_list
\end{Verbatim}
Here, we have first defined the global defaults; \texttt{phonon\_vib\_free 3} corresponds to only the x- and y-directions being selected for the calculation, and the z-direction being switched off. Then, in the \texttt{phonon\_exception\_list} block, we list three exceptions:
\begin{itemize}
\item displacement of ion \texttt{10} in the z-direction (\texttt{3}) is switched on (\texttt{1}), with a value of \texttt{phonon\_sampling} of \texttt{2}, and a value of \texttt{phonon\_finite\_disp} of \texttt{0.9} $\times$ the global value (i.e., \texttt{1.26e-1 bohr});
\item displacement of ion \texttt{15} in the x-direction (\texttt{1}) is switched off (\texttt{0}), with the last two parameters not being read;
\item displacement of ion \texttt{36} in the y-direction (\texttt{2}) is switched off (\texttt{0}), with the last two parameters not being read.
\end{itemize}

\section{Bulk crystal supercell calculations} \label{sec:supercell}

Phonon calculations for crystalline systems can be performed in {\sc onetep} using a supercell approach, with either a real-space truncation of the force constants matrix, or a Slater-Koster style interpolation. The size of the supercell is chosen by the user; obviously, larger supercells will produce more accurate results at arbitrary q points.

It is the responsability of the user to provide the correct supercell lattice vectors and atomic coordinates in the usual way. Additionally, the \texttt{supercell} block must be specified to inform the \texttt{phonon} module that the system is a supercell of bulk material; otherwise, it will be assumed to be a molecule. An example of doing so is as follows:
\begin{Verbatim}[frame=single]
%block lattice_cart
ang
5.3938105 5.3938105 0.0000000
5.3938105 0.0000000 5.3938105
0.0000000 5.3938105 5.3938105
%endblock lattice_cart

%block positions_abs
ang
Si 0.000000000 0.000000000 0.000000000
Si 0.000000000 2.696905250 2.696905250
Si 2.696905250 0.000000000 2.696905250
Si 2.696905250 2.696905250 5.393810500
Si 2.696905250 2.696905250 0.000000000
Si 2.696905250 5.393810500 2.696905250
Si 5.393810500 2.696905250 2.696905250
Si 5.393810500 5.393810500 5.393810500
Si 1.348452625 1.348452625 1.348452625
Si 1.348452625 4.045357875 4.045357875
Si 4.045357875 1.348452625 4.045357875
Si 4.045357875 4.045357875 6.742263125
Si 4.045357875 4.045357875 1.348452625
Si 4.045357875 6.742263125 4.045357875
Si 6.742263125 4.045357875 4.045357875
Si 6.742263125 6.742263125 6.742263125
%endblock positions_abs

%block supercell
2 2 2
1
9
%endblock supercell
\end{Verbatim}
Within the \texttt{supercell} block, the first line gives the shape of the supercell (\texttt{2}$\times$\texttt{2}$\times$\texttt{2}), and subsequent lines list the ions in the \texttt{positions\_abs} block that belong to the `base' unit cell (of course, this supercell is too small to give sensible results for a phonon calculation, and is probably too small to run in {\sc onetep} anyway; a 1000-atom cubic supercell of Si gives excellent results however!)

When a supercell calculations is specified, only the ions within the unit cell are displaced, although the forces on all ions in the system are used to calculate the elements of the dynamical matrix from Eq.~\ref{eq:dynamical_mat}. It is also possible to specify \texttt{phonon\_vib\_free} and \texttt{phonon\_exception\_list} in a supercell calculation, although only the ions listed in the \texttt{supercell} block can be included in the \texttt{phonon\_exception\_list} block.

\section{Task farming} \label{sec:farm}

The most efficient way of performing a phonon calculation is by task farming, as the full force constants matrix is built up from many perturbed-structure calculations, each of which is completely independent. This can be done with the following steps:
\begin{enumerate}
\item Run \texttt{phonon\_farming\_task 1} as a single job; this is essentially a standard single-point energy-and-force {\sc onetep} calculation. Find the line in the main output file which gives the \texttt{Number of force constants} needed for the phonon calculation you have specified (this will be between 1 and $3N$).
\item Divide the total number of force constants that need to be calculated between the desired number of jobs. Prepare the {\sc onetep} input file for each job specifying \texttt{phonon\_farming\_task 2} and a subset of the force constant calculations in the \texttt{phonon\_disp\_list} block. Make sure every job has access to the files \texttt{<seedname>.dkn} and \texttt{<seedname>.tightbox\_ngwfs} obtained from the unperturbed calculation in the previous step.
\item Collect all the \texttt{<seedname>.force\_consts\_<i>} files and place them in the same directory. Finally, run \texttt{phonon\_farming\_task 3} as a single job, to construct the full force constants matrix and perform the post-processing calculations.
\end{enumerate}

\section{Keywords}

The phonon calculation is selected by specifying \texttt{task phonon}. All other keywords related to the module are optional. They are:

\begin{itemize}
\item \texttt{phonon\_farming\_task} [Integer]\\Select which stage to perform (as described in Sec.~\ref{sec:farm}). Can be either \texttt{1}, \texttt{2}, \texttt{3} for a single stage, or \texttt{0} for all stages. Default is \texttt{0}.
\item \texttt{phonon\_sampling} [Integer]\\Finite-difference formula to use (see Eq.~\ref{eq:fd}). Default is \texttt{1}.
\item \texttt{phonon\_finite\_disp} [Physical]\\Ionic displacement distance $d$. Default is \texttt{1.0e-1 bohr}.
\item \texttt{phonon\_fmax} [Physical]\\Maximum ionic force allowed in the unperturbed system. Default is \texttt{5.0e-3 `ha/bohr'}.
\item \texttt{phonon\_energy\_check} [Logical]\\Perform a sanity check that the total energy doesn't decrease upon ionic displacement. Default is \texttt{F}.
\item \texttt{phonon\_vib\_free} [Integer]\\Default allowed vibrational degrees of freedom for all ions (see Sec.~\ref{sec:free} for details). Default is \texttt{7}.
\item \texttt{phonon\_exception\_list} [Block]\\List of exceptions to the global defaults defined by \texttt{phonon\_vib\_free}, \texttt{phonon\_sampling}, and \texttt{phonon\_finite\_disp} (see Sec.~\ref{sec:free} for details). Default is unspecified.
\item \texttt{supercell} [Block]\\Definition of the supercell used for crystalline material (see Sec.~\ref{sec:supercell} for details). Default is unspecified.
\item \texttt{phonon\_disp\_list} [Block]\\List of force constant calculations to perform for Stage 2. Note that the total number of force constant calculations is given in the main output file in the line \texttt{Number of force constants}; this will be less than or equal to $3N$. The numbers listed in the \texttt{phonon\_disp\_list} block should go from 1 to this number; {\em they can only be equated to the label} \texttt{<i>} {\em if all} $3N$ {\em force constants are calculated}. If unspecified, all displacements are performed. Default is unspecified. Example:
\begin{verbatim}
%block phonon_disp_list
1
3
5
%endblock phonon_disp_list
\end{verbatim}
\item \texttt{phonon\_grid} [Block]\\Definition of the regular grid of q points used for the computation of thermodynamic quantities and the phonon DOS. Default is \texttt{1 1 1} (i.e., $\Gamma$ point only). Example:
\begin{verbatim}
%block phonon_grid
10 10 10
%endblock phonon_grid
\end{verbatim}
\item \texttt{phonon\_SK} [Logical]\\Use a Slater-Koster style interpolation for q points instead of a real-space cutoff of the force constants matrix elements. Default is \texttt{F}.
\item \texttt{phonon\_tmin} [Physical]\\Lower bound of the temperature range for the computation of thermodynamic quantities, expressed as an energy ($k_\mathrm{B} T$). Default is \texttt{0.0 hartree}.
\item \texttt{phonon\_tmax} [Physical]\\Upper bound of the temperature range for the computation of thermodynamic quantities. Default is \texttt{2.0e-3 hartree} ($\simeq 632$ K).
\item \texttt{phonon\_deltat} [Physical]\\Temperature step for the computation of thermodynamic quantities. Default is \texttt{1.5e-5 hartree} ($\simeq 5$ K).
\item \texttt{phonon\_min\_freq} [Physical]\\Minimum phonon frequency for the computation of thermodynamic quantities, expressed as an energy ($\hbar \omega$); frequencies lower than this are discarded. Default is \texttt{3.6e-6 hartree} ($\simeq 5$ cm$^{-1}$).
\item \texttt{phonon\_DOS} [Logical]\\Calculate the phonon DOS and write to file. Default is \texttt{T}.
\item \texttt{phonon\_DOS\_min} [Real]\\Lower bound of the phonon DOS range (in cm$^{-1}$). Default is \texttt{0.0}.
\item \texttt{phonon\_DOS\_max} [Real]\\Upper bound of the phonon DOS range (in cm$^{-1}$). Default is \texttt{1000.0}.
\item \texttt{phonon\_DOS\_delta} [Real]\\Frequency step for the phonon DOS calculation (in cm$^{-1}$). Default is \texttt{10.0}.
\item \texttt{phonon\_qpoints} [Block]\\List of additional q points for which to calculate the phonon frequencies, in fractional coordinates of the reciprocal unit cell vectors. For non-supercell calculations only the $\Gamma$ point can be specified. Default is unspecified. Example:
\begin{verbatim}
%block phonon_qpoints
0.0 0.0 0.0
0.0 0.0 0.1
0.0 0.0 0.2
0.0 0.0 0.3
0.0 0.0 0.4
0.0 0.0 0.5
%endblock phonon_qpoints
\end{verbatim}
\item \texttt{phonon\_write\_eigenvecs} [Logical]\\Write the eigenvectors as well as the phonon frequencies to file for the additional q points. Default is \texttt{F}.
\item \texttt{phonon\_animate\_list} [Block]\\List of $\Gamma$-point modes (where \texttt{1} is the lowest) for which to write xyz animation files. Default is unspecified. Example:
\begin{verbatim}
%block phonon_animate_list
2
6
33
34
%endblock phonon_animate_list
\end{verbatim}
\item \texttt{phonon\_animate\_scale} [Real]\\Relative scale of the amplitude of the vibration in the xyz animation. Default is \texttt{1.0}.
\end{itemize}

\section{Additional notes}

Phonon calculations are quite sensitive to the accuracy of the ionic forces calculated for the perturbed structures. Therefore, it is advisable to make sure that the forces are well-converged with respect to the usual parameters: cut-off energy, number and radius of NGWFs, and spatial cut-off of the density kernel.

Furthermore, it is also important to make sure that for a given set of parameters the forces are properly converged at the end of the energy minimization procedure, and that the numerical noise is reduced to a minimum; the code will not check this automatically, and the forces generally converge slower than the total energy. To ensure an accurate result, therefore, the following values for the convergence threshold parameters are suggested:

\begin{itemize}
\item \texttt{ngwf\_threshold\_orig 1.0e-7}.
\item \texttt{lnv\_threshold\_orig 1.0e-11}.
\end{itemize}

\end{document}
