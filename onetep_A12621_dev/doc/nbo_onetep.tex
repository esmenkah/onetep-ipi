\documentclass[a4paper,10pt]{article}
\title{\textsc{onetep} to \textsc{gennbo} \texttt{FILE.47} Input Parameters}
\author{Louis Lee\\
  TCM Group, Cavendish Laboratory\\
  19 JJ Thomson Ave, Cambridge CB3 0HE, UK\\
  \texttt{lpl24@cam.ac.uk}}
\date{\today}

\usepackage{a4wide}
\usepackage{mathtools}
\usepackage{multirow}
\usepackage{graphicx, subfigure}
\usepackage{float}
\usepackage[numbers,super,comma,sort&compress]{natbib}
\usepackage{natmove}
\usepackage[T1]{fontenc}

\numberwithin{equation}{section}

\newcommand{\bra}[1]{\langle #1|}
\newcommand{\ket}[1]{|#1\rangle}
\newcommand{\braket}[2]{\langle #1|#2\rangle}

\begin{document}
\maketitle

\section{Standalone \textsc{nbo} 5 Program \textsc{gennbo}}
\label{sec:onetep_gennbo}
The standalone version of the NBO program (\textsc{\textsc{gennbo}})~\cite{nbo5.9} accepts parameters from an input 
ASCII free-format \texttt{FILE.47} containing atomic coordinates and matrix information, as printed by \textsc{onetep} if the Natural Population Analysis~\cite{reed:735} subroutine is called during a \texttt{PROPERTIES}
calculation, by specifying the keyword \texttt{write\_nbo: T}. The NBO
formalism allows one transform a converged 1-particle wavefunction in an atom-centred bases
into a set of highly-local `Natural Bond Orbitals', which are one and two (or three)-centred
`lone' and `bond' pairs recognizable as chemical bonds from a classical Lewis structure standpoint~\cite{doi:10.1021/cr00088a005}. Details of the NBO formalism are discussed elsewhere~\cite{reed:735,doi:10.1021/cr00088a005,nbo_encyl_comp_chem}.

\subsection{Compiling \textsc{gennbo}}
\label{sec:compile_nbo}
Compilation of the standalone \textsc{gennbo} does not involve \textsc{onetep} in any way.
As of writing, the latest \textsc{nbo} release is version 5.9~\cite{nbo5.9}. Compilation instructions 
are listed here for convenience, based on some trial-and-error when the arcane 
\texttt{g77} compiler listed in the \textsc{nbo} manual is unavailable.

To compile \textsc{gennbo}, first, compile the activator, \texttt{enable.f}:
\begin{quote}
\texttt{gfortran -o enable enable.f}
\end{quote}
then run the \texttt{enable} program. Complete the selections to 
generate the standalone \textsc{gennbo} source \texttt{gennbo.f}.

By default, \textsc{gennbo} limits the number of atoms and basis in the 
\texttt{FILE.47} input to 200 and 2000 respectively. 
This can be increased by replacing all instances of \texttt{MAXATM = 200} 
and \texttt{MAXBAS = 2000} to a user-specified value, up to a limit 
of 999 and 9999 respectively (higher values are possible, albeit accompanied by 
illegible output due to format overflow. In principle one could modify the code even 
further to remedy this issue.).

The following command should compile \textsc{gennbo} correctly on x64 architectures, 
when no modification is made to the \texttt{gennbo.f} source:

\begin{quote}
\texttt{gfortran -fdefault-integer-8 -fno-sign-zero -m64 -o gennbo gennbo.f}\\
\texttt{ifort -i8 -m64 -f77rtl -o gennbo gennbo.f}
\end{quote}

For the 32-bit version, integer length should be set to 4 bytes instead 
(e.g. \texttt{-i4} in \texttt{ifort}). 
If \texttt{MAXATM} and \texttt{MAXBAS} 
have been increased then the memory model should also be set to allow data $> 2$ GB, by
adding a \texttt{-mcmodel=medium} flag. For
\texttt{ifort}, an additional 
\texttt{-shared-intel} flag is most likely necessary.

Then, to run:
\begin{quote}
\texttt{gennbo < FILE.47 > output.out}
\end{quote}

\section{\textsc{onetep} NPA Generation Routine}
The Natural Population Analysis~\cite{reed:735} method of computing atomic charges
is implemented in \textsc{onetep}. The routine transforms the set of non-orthogonal,
optimized NGWFs into a set of orthogonal atom-centred `Natural Atomic Orbitals' (NAOs) 
via an `occupancy-weighted symmetric orthogonalization' procedure, which serves 
to maximise the resemblance of the final orthogonal orbitals to their
initial non-orthogonal parents (a la L\"{o}wdin orthogonalization), weighted according to
the parent orbital occupancies. Therefore, vacant, highly-diffuse orbitals are
free to distort to achieve orthogonality with their more highly-preserved occupied
counterpart. This ensures that the final NAO population (the `Natural Population')
remains stable with respect to basis set size.

Once in the NAO basis, further transformations such as pair-block density matrix
diagonalization produce the final set of NBOs -- these procedures are performed by
\textsc{nbo} 5 from the \texttt{FILE.47} output of \textsc{onetep},
which contains relevant matrices in the NAO basis. The NAO routine is performed
internally in \textsc{onetep} as \textsc{nbo} 5 requires pseudo-atomic orbitals (such
as Gaussian-type orbitals) with free-atom symmetries and orthogonality within each
atom, a property not rigorously satisfied by the optimized NGWFs.

The NPA module in \textsc{onetep} performs at its best for large systems when
compiles with the \textsc{ScaLapack} linear algebra package, as it takes advantage
of the distributed memory storage of dense global matrices, such as the inverse
square root of the overlap matrix that needs to be computed for the
`occupancy-weighted symmetric orthogonalization' step. This has the unfortunate
side effect of rendering the NAO transformation a cubic-scaling method. However,
this step occurs only once during the routine, and should be comparable to
the time needed to generate canonical molecular orbitals.

\section{List of Available Parameters}
\begin{table}[H]\footnotesize
\centering
\begin{tabular}{l l l l p{2.75 in}}
\hline
\multicolumn{5}{l}{NAO Generation (Default)} \\
\hline
Keyword                                            & Type & Default & Level & Description \\
\hline
\texttt{write\_nbo}           &  L   &    F    &   B   & Enables Natural Population Analysis (NPA) and writing of \textsc{gennbo} input file
                                                                              \texttt{<seedname>\_nao\_nbo.47} \\
\texttt{nbo\_init\_lclowdin}  &  L   &    T    &   E   & Performs atom-local L\"{o}wdin orthogonalisation on NGWFs as the first step before constructing NAOs \\
\texttt{nbo\_write\_lclowdin} &  L   &    F    &   E   & Writes full matrices (all atoms) in the atom-local L\"{o}wdin-orthogonalized basis to 
                                                                              \texttt{FILE.47} (For reference/testing/comparison purposes).
                                                                              Output will be \texttt{<seedname>\_lclowdin\_nbo.47} \\
\texttt{nbo\_write\_npacomp}  &  L   &    F    &   B   & Writes NAO charges for all orbitals to standard output \\
\texttt{nbo\_write\_dipole}   &  L   &    F    &   B   & Computes and writes dipole matix to \texttt{FILE.47} \\
\texttt{nbo\_scale\_dm}       &  L   &    T    &   E   & Scales partial density matrix output to \texttt{<seedname>\_nao\_nbo.47} in order to
                                                                              achieve charge integrality \\
\texttt{nbo\_scale\_spin}     &  L   &    T    &   E   & Scales $\alpha$ and $\beta$ spins independently to integral chrage when partial matrices are printed and
                                                                              \texttt{nbo\_scale\_dm = T}. Inevitably means spin density values from \textsc{gennbo} 
                                                                              are invalid and one should calculate them manually from the $\alpha$ and $\beta$ NPA populations. \\
\texttt{nbo\_write\_species}  &  B   &   N/A   &   B   & Block of lists of species to be included in the partial matrix output of
                                                                              \texttt{<seedname>\_nao\_nbo.47}. If not present all atoms will be included. E.g. \newline
                                                                              specified will default to AUTO. E.g.: \newline
                                                                              \texttt{\%block nbo\_write\_species} \newline
                                                                              \texttt{C1} \newline
                                                                              \texttt{H1} \newline
                                                                              \texttt{\%endblock nbo\_write\_species} \\
\texttt{nbo\_species\_ngwflabel} & B &  AUTO   &   I   & Optional user-defined (false) \textit{lm}-label for NGWFs according to \textsc{gennbo} convention. Species not
                                                                              specified will default to AUTO. E.g.: \newline
                                                                              \texttt{\%block nbo\_species\_ngwflabel} \newline 
                                                                              \texttt{C1 ``1N 151N 152N 153N''} \newline 
                                                                              \texttt{H1 ``AUTO''} \newline
                                                                              \texttt{\%endblock nbo\_species\_ngwflabel} \newline 
                                                                              -N suffix denotes NMB orbital. If 'SOLVE' orbitals are used, this block should be present as 'AUTO' initialisation 
                                                                              assumes orbitals were also initialised as 'AUTO'.\\
\texttt{nbo\_aopnao\_scheme}  &  T   & ORIGINAL &  E   & The AO to PNAO scheme to use. Affects the '\textit{lm}-averaging' and diagonalisation steps in the initial
                                                                              AO to PNAO, NRB \textit{lm}-averaging, and rediagonaliation transformations (the '\textbf{N}' transformations
                                                                              in \cite{reed:735}). For testing purposes only - so far none of the other schemes apart from 
                                                                              \texttt{ORIGINAL} works. Possbile values are: \newline
                                                                              \texttt{ORIGINAL} - default, as in \cite{reed:735} with \textit{lm}-averaging \newline
                                                                              \texttt{DIAGONALIZATION} - Diagonalises entire atom-centred sub-block w/o \textit{lm}-averaging
                                                                              or splitting between different angular channels. \newline
                                                                              \texttt{NONE} - Skips all '\textbf{N}' transformations. \\
\texttt{nbo\_pnao\_analysis}  &  L   &    F     &  E   & Perform s/p/d/f analysis on the PNAOs (analogous to \texttt{ngwf\_analysis}) \\
\hline
\end{tabular}
\label{tab:keywords}
\end{table}

\begin{table}[!h]\footnotesize
\centering
\begin{tabular}{l l l l p{2.75 in}}
\hline
\multicolumn{5}{l}{Orbital Plotting} \\
\hline
Keyword                                            & Type & Default & Level & Description \\
\hline
\texttt{plot\_nbo}            &  L   &    F    &   B   & Instructs \textsc{onetep} to read the relevant orbital transformation output from \textsc{gennbo}, determined by the
                                                                              flag \texttt{nbo\_plot\_orbtype} and plots the orbitals specified in 
                                                                              \texttt{\%block nbo\_list\_plotnbo}. \texttt{write\_nbo} 
                                                                              and \texttt{plot\_nbo} are mutually exclusive. Scalar field plotting must be
                                                                              enabled (e.g. \texttt{cube\_format = T}). \\
\texttt{nbo\_plot\_orbtype}   &  T   &   N/A   &   B   & The type of \textsc{gennbo}-generated orbitals to read and plot. Possible values and their associated \textsc{gennbo}
                                                                              transformation files must be present, as follows: \newline
                                                                              \texttt{NAO} - \texttt{<seedname>\_nao.33} \newline
                                                                              \texttt{NHO} - \texttt{<seedname>\_nao.35} \newline
                                                                              \texttt{NBO} - \texttt{<seedname>\_nao.37} \newline
                                                                              \texttt{NLMO} - \texttt{<seedname>\_nao.39} \newline
                                                                              NLMO is only defined for the full system i.e. partitioned  \texttt{FILE.47}
                                                                              will give meaningless NLMOs. Except for NLMO, adding a 'P' prefix e.g. 'PNAO' to the value of
                                                                              \texttt{nbo\_plot\_orbtype} causes the non-orthogonalised PNAOs to be used
                                                                              in plotting instead of NAOs. PNAOs are of the normal type, i.e. when \texttt{RPNAO = F}
                                                                              in \textsc{gennbo} (default). \\
\texttt{nbo\_list\_plotnbo}   &  B   &  N/A    &   B   & The list of \texttt{nbo\_plot\_orbtype} orbitals to be plotted, identified by their indices
                                                                              as in the \textsc{gennbo} output. Specify each index on a new line. \\
\hline
\end{tabular}

\begin{tabular}{l p{3.25 in}}
& \\
\hline
Output files: & \\
\hline
\texttt{<seedname>\_nao\_nbo.47}        & Always written. Contains partial matrices according to \texttt{\%block nbo\_write\_species}. \\
\texttt{<seedname>\_lclowdin\_nbo.47}   & Written if \texttt{nbo\_write\_lclowdin = T}. Always contains all atoms in the atom-local
                                                               L\"{o}wdin-orthogonalized basis. If \texttt{nbo\_init\_lclowdin = T} and all atoms are included
                                                               \texttt{<seedname>\_lclowdin\_nbo.47} = \texttt{<seedname>\_nao\_nbo.47}
                                                               except for ordering of atomic centers (will be fixed in newer releases). \\
\texttt{<seedname>\_nao\_atomindex.dat} & Contains mapping of atomic indices of the potentially subset of the full system
                                                               in \texttt{<seedname>\_nao\_nbo.47} to the real atomic index of the full system (since labels
                                                               have to be consecutive). Real atomic index refers to original input order in \textsc{onetep}. \\
\texttt{<seedname>\_inittr\_nao\_nbo.dat} & Raw NGWF to NAO transformation read for plotting (i.e. when \texttt{plot\_nbo = T}). \\
\texttt{<seedname>\_inittr\_pnao\_nbo.dat} & Raw NGWF to PNAO transformation read for plotting (i.e. when \texttt{plot\_nbo = T}). PNAOs are of the
                                                                  'normal' type, i.e when \texttt{RPNAO = F} in \textsc{gennbo}. \\
\texttt{<seedname>\_nbo\_DEBUG.dat}      & Contains various debugging info. Only written if compiled in debug mode. \\
\hline
\end{tabular}
\label{tab:keywords2}
\end{table}

\clearpage

\section{Notes}
\subsection{Orbital labelling with pseudoatomic solver}
\begin{itemize}
\item
\textsc{gennbo} labels in \texttt{\%block nbo\_species\_ngwflabel}
should always be explicitly given when  \texttt{SOLVE} is used to 
initialise the NGWFs. The label string is however limited to 80 characters in \textsc{onetep}, 
which should be fine up to $1s2sp3spd4sp$. This will be fixed later unless it is urgently required.
\item
Make sure the orbitals selected for plotting are valid. The NPA routine assumes 
that the appropriate transformation file from \textsc{gennbo} in the same directory is correct, and 
only complains if it encounters an EOF, but not if the wrong transformation file is given 
(e.g. from a different system with a larger basis).
\item
Do not rename the \textsc{gennbo}-generated transformation files. \textsc{onetep} expects them to have 
the name \texttt{<seedname>\_nao.xx}.
\end{itemize}

\subsection{Orbital plotting}
\begin{itemize}
\item
In order to plot the various orbitals, first run the output  \texttt{FILE.47}
through \textsc{gennbo} to obtain the relevant orbital vectors. Refer to the \textsc{nbo} 5 manual
for details on how to print these (e.g. to print NBOs in the input \texttt{FILE.47}
basis, set \texttt{AONBO=W} in the \texttt{\$NBO} block).

\item
For some reason, the \texttt{PLOT} keyword itself in \textsc{gennbo} doesn't work.
This might have something to do with the '\texttt{ORTHO} bug'.
\end{itemize}

\subsection{'\texttt{ORTHO} bug'}
The \textsc{nbo} 5 program up till circa April/May 2011 had a bug whereby specifying the 
\texttt{ORTHO} flag causes the program to crash. The \textsc{nbo 5} developers
seem to have fixed most of this and given me the an updated version, but residual bug could remain
(have they made the fix a general release yet?). This is of course fixable by 
running the \texttt{<seedname>\_lclowdin\_nbo.47} file through \textsc{gennbo}
instead, albeit this would mean one can't do DM partitioning.

\section{Example Usage}
\subsection{Obtaining $2^{\mathrm{nd}}$-order Perturbation Estimates of the $n\rightarrow\sigma^*$
Secondary Hyperconjugation in Water Dimer (Hydrogen Bond)}
The hydrogen bond stabilization in water dimer can be attributed to the non-classical 'charge transfer'
interaction between two water molecules due to delocalization of the electronic charge
from the oxygen lone pair $n$ of the donor monomer to the $\sigma^*$ O--H antibond of
the acceptor~\cite{doi:10.1021/cr00088a005}. The expansion of the variational space to 
included non-Lewis, formally vacant antibond NBOs leads to an energetic lowering compared to 
the ideal Lewis configuration (all Lewis NBO occupancy = 2 \textit{e}), which can be estimated via 
$2^{\mathrm{nd}}$-order perturbation theory as the 'charge transfer' energetic component of the
dimer interaction.

From a converged SCF calculation in \textsc{onetep} using the reference coordinates
below (given in Angstroms):

\begin{quote}\footnotesize

\begin{verbatim}
%block  positions_abs
"ang"
 O  10.6080354926368 12.5000150953008 12.5705695516353
 H  10.4341376488693 12.5000119731552 13.5119746410112
 H1 11.5729802892758 12.5000098564464 12.5000098564464
 H  13.9638274701977 13.2691512541917 12.2000071259853
 O1 13.4760438789916 12.5000098564464 12.5000098564464
 H  13.9638258826660 11.7308667653340 12.2000083960106
%endblock  positions_abs
\end{verbatim}

\end{quote}

\noindent with the pseudoatomic solver employing a minimal NGWF basis (1 NGWF on H, 4 on O)
with a 10.0 a$_0$ NGWF radius cutoff, PBE exchange-correlation functional, 
norm-conserving pseudopotential with pseudized
$1s$ core for O, and a 1200 eV psinc cutoff in a 25.0 a$_0$ cubic simulation cell, 
one should run a \texttt{PROPERTIES} calculation with the 
additional keywords as such:

\begin{quote}\footnotesize

\begin{verbatim} 
write_nbo: T
%block species_ngwflabel
 H  "1N"
 O  "1N 152N 153N 151N"
 H1 "1N"
 O1 "1N 152N 153N 151N"
%endblock species_ngwflabel
\end{verbatim}

\end{quote}

\noindent where the \texttt{species\_ngwflabel} block tells the NPA
routine in \textsc{onetep} how to label each NGWF. The order of $m$ for each $l$
in the $Y(l,m)$ isn't straightforward, and follows the pattern of e.g.
``152 153 151'' i.e. $m=\{-1,0,1\}$ for $l=1$, and ``251 253 255 252 254'' for
$l=2$. I've yet to look at how others are arranged, though this is not
very important unless one is interested in 'NHO Directionality and Bond Bending'
analysis, as in the NBO scheme, all $m$ of the same $l$ are treated equally.
The order of each $Y(l,m)$ should follow that of the pseudoatomic solver, which
does them in principal quantum number ($n$) increments 
(with multiple-$\zeta$ basis, the split-valence set of $Y(l,m)$ probably 
comes first i.e. $Y^{\zeta1}(l,m)$ then $Y^{\zeta2}(l,m)$ before the next $n$.
The ``N'' suffix denotes valence orbital in the ground state, which in the 
case of H, ``1N'' is the $1s$ orbital. Make sure the correct orbitals are 
marked as valence as they would appear in the ground state (even if
the pseudoatomic solver basis was initialized in an excited configuration).
In this example, the pseudoatomic solver block would have explicitly been:

\begin{quote}\footnotesize

\begin{verbatim}
%block species_atomic_set
 H  "SOLVE conf=1s1"
 O  "SOLVE conf=1sX 2s2 2p4"
%endblock species_atomic_set
\end{verbatim}

\end{quote}

\textsc{onetep} should run and produce an NPA output listing the
NPA charges on each atom, and print a 
\texttt{<seedname>\_nao\_nbo.47} file. This
\texttt{.47} file serves as the input for
\textsc{gennbo}.

If we wanted to generate NBOs and visualize them, insert the
keyword \texttt{AONBO=W} in the
\texttt{\$NBO} block of the 
\texttt{.47} file before running it
through \textsc{gennbo}. \textsc{gennbo} will output a
report containing NBO information, including the $2^{\mathrm{nd}}$-order
perturbation estimates, and a \texttt{.37} file
containing the NBO vectors in terms of the 
\texttt{.37} input basis (don't change any
of the \texttt{.47}, \texttt{.37}
etc. filenames).

First, we can see that the $2^{\mathrm{nd}}$-order perturbation report
shows one prominenet interaction, namely one between the occupied lone pair
of oxygen from one H$_2$O unit 
(\texttt{LP ( 2) O 5}) to the O--H antibond of the other
(\texttt{BD*( 2) O 1- H 3}) with an estimate of 15.32
kcal/mol, corresponding to the hydrogen bond in water dimer:

\pagebreak

\begin{quote}\footnotesize

\begin{verbatim}
 SECOND ORDER PERTURBATION THEORY ANALYSIS OF FOCK MATRIX IN NBO BASIS

     Threshold for printing:   0.50 kcal/mol
    (Intermolecular threshold: 0.05 kcal/mol)
                                                          E(2)  E(j)-E(i) F(i,j)
      Donor NBO (i)              Acceptor NBO (j)       kcal/mol   a.u.    a.u. 
 ===============================================================================

 within unit  1
       None above threshold

 from unit  1 to unit  2
   2. BD ( 1) O 1- H 3       11. BD*( 1) H 4- O 5         0.08    0.67    0.007
   2. BD ( 1) O 1- H 3       12. BD*( 1) O 5- H 6         0.08    0.67    0.007

 from unit  2 to unit  1
   3. BD ( 1) H 4- O 5       10. BD*( 1) O 1- H 3         0.10    0.83    0.008
   4. BD ( 1) O 5- H 6       10. BD*( 1) O 1- H 3         0.10    0.83    0.008
   7. LP ( 1) O 5            10. BD*( 1) O 1- H 3         0.18    0.49    0.008
   8. LP ( 2) O 5            10. BD*( 1) O 1- H 3        15.32    0.60    0.085

 within unit  2
       None above threshold
\end{verbatim}

\end{quote}

Noting down the orbital numbers, we can then proceed to plot them by
running another properties calculation in \textsc{onetep} with the
following block:

\begin{quote}\footnotesize

\begin{verbatim}
write_nbo   : F
plot_nbo    : T
cube_format : T
nbo_plot_orbtype : NBO
%block nbo_list_plotnbo
  8
 10
%endblock nbo_list_plotnbo
\end{verbatim}

\end{quote}

where \texttt{write\_nbo} needs to be set to 
\texttt{F}. \textsc{onetep} will then read
the \texttt{<seedname>\_inittr\_nao\_nbo.dat}
file printed during the first run and the 
\texttt{.37} file to plot the orbitals specified
in the \texttt{nbo\_list\_plotnbo} block into
Gaussian cube files.

An example result is displayed in a figure available
in the published paper.

%\begin{figure}[!htb]\footnotesize
%\centering
%\caption{Example plots of the $n\rightarrow\sigma^*$ hyperconjugation in
%water dimer. Oxygen lone pair $n$ in red/blue (+/-), O--H antibond
%$\sigma^*$ in cyan/yellow (+/-). Isosurface contour value at 0.075 a.u.}
%\label{fig:plots}
%\includegraphics[width=0.4\textwidth]{wdimer_hbond.eps}
%\end{figure}

\pagebreak

\subsection{Notes on Selectively Passing sub-region sub-matrices into \textsc{gennbo}}
To circumvent the limitations on system size in \textsc{gennbo}, and for
convenience, we could output only matrix elements corresponding to atoms
within a selected sub-region of a large system. To do so, during an NPA analysis
run (not plotting) within a properties run in \textsc{onetep}, the following
should be specified:

\begin{quote}\footnotesize
\begin{verbatim}
%block nbo_write_species
 O1
 H1
 C1
 ...
%endblock nbo_write_species
\end{verbatim}

\end{quote}

\textsc{onetep} would then print only matrix elements belongning to species
specified by the labels in the \texttt{\%block nbo\_write\_species}
block to \texttt{<seedname>\_nao\_nbo.47}. Due to
\textsc{gennbo} insisting on integral charges, the density matrix in the
\texttt{.47} file is re-scaled downwards to the nearest
lowest integral number, to avoid the possibility of orbitals having occupancies
$> 2$ \textit{e}, which also annoys \textsc{gennbo}. To minimize the impact
of this technical re-scaling to the NBO results, a sufficiently-sized partition
should be chosen in \texttt{\%block nbo\_write\_species}
so that $1/N_e << 1$, where $N_e$ is the number of electrons in the partition.

The final results fron NBO analysis that depend on the density matrix will
then need to be de-scaled to arrive at the correct value (e.g. NPA charges,
NBO occupancies, $2^{\mathrm{nd}}$-order perturbation estimates, while orbital
energies don't require de-scaling).

Note that the region included in 
\texttt{\%block nbo\_write\_species} should have buffer
atoms, which minimally should include the next-nearest neighbour atom bonded 
to the last atom in the selection -- that way, the severing of a bond would
only affect NBOs centred on the buffer atom, and not anywhere else.

As a final note, there is a possibility that during an NBO search, slightly
different NBO pictures are obtained when passing only part of the matrix
as compared to analyzing the full system -- this can be caused by the fact
that during an NBO search, the \textsc{nbo} 5 program iterates through
different occupancy thresholds ($n_{min}$) for deciding upon whether an orbital is 
a lone pair/NBO. If one is padentic about this, then $n_{min}$ can be fixed
by specifying the \texttt{THRESH = }$n_{min}$ keyword
manually in the \texttt{\$NBO} block in
the \texttt{.47} file, where $n_{min}$ is defined by the user.

\bibliographystyle{bibfmt}

\providecommand{\refin}[1]{\\ \textbf{Referenced in:} #1}
\begin{thebibliography}{1}

\bibitem{nbo5.9}
E. D. Glendening, J. K. Badenhoop, A. E. Reed, J. E. Carpenter, J. A. Bohmann,
  C. M. Morales, F. Weinhold; {NBO} 5.9 (http://www.chem.wisc.edu/\~{}nbo5) \&
  the {NBO} 5.9 Manual, Theoretical Chemistry Institute, University of
  Wisconsin, Madison, WI.

\bibitem{reed:735}
A.~E. Reed,\ R.~B. Weinstock,\ F.~Weinhold \textit{J. Chem. Phys.}
  \textbf{1985,} \textsl{83,} 735-746.

\bibitem{doi:10.1021/cr00088a005}
A.~E. Reed,\ L.~A. Curtiss,\ F.~Weinhold \textit{Chem. Rev.} \textbf{1988,}
  \textsl{88,} 899-926.

\bibitem{nbo_encyl_comp_chem}
A. D. {MacKerell, Jr.}, B. Brooks, C. L. Brooks III, L. Nilsson, B. Roux, Y.
  Won, M. Karplus, in Encyclopedia of Computational Chemistry; R. Schleyer et
  al. Eds.; John Wiley \& Sons, Chichester, \textbf{1998}; Vol. 3, Chapter
  `Natural Bond Orbital Methods', pp 1792-1811.

\end{thebibliography}


\end{document}
