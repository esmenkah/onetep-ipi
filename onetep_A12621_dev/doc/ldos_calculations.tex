%% LyX 1.6.7 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[11pt,english]{article}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage[a4paper]{geometry}
\geometry{verbose,lmargin=3cm,rmargin=3cm}
\usepackage{amsmath}
\usepackage{amssymb}

\makeatletter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.


\usepackage{latexsym}\usepackage{amsthm}\usepackage{bm}

\makeatother

\usepackage{babel}

\begin{document}

\title{Calculating the Local/Partial Density of States in ONETEP}


\author{Nicholas D.M. Hine \\
 Imperial College London}


\date{April 2012}

\maketitle

\section*{What is being calculated?}

The local density of states provides a description encompassing both
the spatial and energetic distribution of the single-particle eigenstates
simultaneously. It can thus be one of the most valuable sources of
information for understanding and interpreting electronic structure
calculations. In the local-orbital framework of ONETEP \cite{ref:skylaris:084119},
an LDOS decomposition is achieved by performing a diagonalisation
of the Hamiltonian matrix in the NGWF basis. This is a post-processing
step performed at the end of the calculation once NGWF and density
kernel convergence have been achieved. While this comes with a $O(N^{3})$
computational cost, the prefactor is low because the NGWF basis is
generally quite small. Therefore, such a diagonalisation remains fast
up to quite large system sizes, particularly if a parallel eigensolver
such as ScaLAPACK is used.

The generalised eigenproblem that needs to be solved to provide the
eigenvalues and eigenvectors is:\begin{equation}
\sum_{\beta}H_{\alpha\beta}M_{\phantom{\beta}n}^{\beta}=\epsilon_{n}\sum_{\beta}S_{\alpha\beta}M_{\phantom{\beta}n}^{\beta}\label{eq:gen_eig_prob}\end{equation}
The matrix $M_{\phantom{\beta}n}^{\beta}$ describes the eigenvectors,
which take the form $|\psi_{n}\rangle=\sum_{\beta}M_{\phantom{\beta}n}^{\beta}|\phi_{\beta}\rangle$.
In ONETEP, Eq.~\ref{eq:gen_eig_prob} can be solved using either
the LAPACK routine DSYGVX or (preferably) the ScaLAPACK routine PDSYGVX,
depending on whether -DSCALAPACK has been specified at compile time,
and ScaLAPACK libraries have been provided. 

The result is the eigenvalues $\{\epsilon_{n}\}$ and eigenvectors
$M_{\phantom{\beta}n}^{\beta}$. From these, the total density of
states can be obtained as \begin{equation}
D(\epsilon)=\sum_{n}\delta(\epsilon-\epsilon_{n})\;.\label{eq:DOS}\end{equation}
In practice the delta function is replaced with a Gaussian broadening,
typically of the order of $0.1\,\mathrm{eV}$. 

The local density of states in a given region $I$ is calculated by
projecting each eigenstate onto the local orbitals of region $I$,
as

\begin{equation}
D_{I}(\epsilon)=\sum_{n}\delta(\epsilon-\epsilon_{n})\;\langle\psi_{n}|\sum_{\alpha\in I}\left(|\phi^{\alpha}\rangle\langle\phi_{\alpha}|\right)|\psi_{n}\rangle.\label{eq:LDOS}\end{equation}
Here, the non-orthogonality of the NGWFs when used as projectors means
that the ket must be contravariant. We are therefore implicitly using
the contravariant dual of the NGWF (as in DFT+U \cite{ref:PRB_ORegan_2011,ref:PRB_ORegan_2012}).
Fortunately, the functions $|\phi^{\alpha}\rangle$ need not be explicitly
constructed in real space: the relationship $\langle\phi_{\alpha}|\phi^{\beta}\rangle=\delta_{\alpha\beta}$
implies that we can re-write Eq.~\ref{eq:LDOS} as:

\begin{eqnarray}
D_{I}(\epsilon) & = & \sum_{n}\!\delta(\epsilon-\epsilon_{n})\!\!\!\!\!\!\sum_{\beta,\gamma,\alpha\in I}\!\!\!\!(M^{\dagger})_{n}^{\phantom{n}\gamma}\langle\phi_{\gamma}|\left(|\phi^{\alpha}\rangle\langle\phi_{\alpha}|\right)|\phi_{\beta}\rangle M_{\phantom{\beta}n}^{\beta}\nonumber \\
 & = & \sum_{n}\delta(\epsilon-\epsilon_{n})\sum_{\alpha\in I}(M^{\dagger})_{n}^{\phantom{n}\alpha}(\sum_{\beta}S_{\alpha\beta}M_{\phantom{\beta}n}^{\beta})\label{eq:LDOS2}\end{eqnarray}
What is therefore obtained is a series of functions $D_{I}(\epsilon)$
for each of the chosen regions $I$, which may be the NGWFs of a single
atom, or those of a group of atom types.


\section*{Performing an LDOS Calculation}

An LDOS calculation is performed as part of the optional post-processing
activated using {}``\texttt{do\_properties: T}'' or using {}``\texttt{task: PROPERTIES}''.
To activate LDOS we then need to specify the Gaussian broadening,
such as {}``\texttt{dos\_smear : 0.1 eV}''. The default value of
{}``\texttt{dos\_smear : -0.1 eV}'' disables LDOS.

Then, we need to specify the groups of atom types. This is done via
a block, with each line listing a group of atoms. For example, in
a benzene ring, we might use the following to find the contributions
of the carbon and hydrogen atoms respectively:

\texttt{}%
\begin{minipage}[t]{1\columnwidth}%
\texttt{\vskip0.0cm}

\texttt{\%block species\_ldos\_groups}

C

H

\texttt{\%endblock species\_ldos\_groups}

\texttt{\vskip0.2cm}%
\end{minipage}A more complex example would be for a GaAs nanorod with hydrogen termination
on the faces. If we wished to see the LDOS varying over 5 layers,
labelled 1-5, we could use:

\texttt{}%
\begin{minipage}[t]{1\columnwidth}%
\texttt{\vskip0.0cm}

\texttt{\%block species\_ldos\_groups}

\texttt{Ga1 As1 H1}

\texttt{Ga2 As2 H2}

\texttt{Ga3 As3 H3}

\texttt{Ga4 As4 H4}

\texttt{Ga5 As5 H5}

\texttt{\%endblock species\_ldos\_groups}

\texttt{\vskip0.2cm}%
\end{minipage}

Examples of the use of LDOS analysis, including example plots, can
be found in several recent papers employing ONETEP \cite{ref:PhysRevB.83.241402,ref:PhysRevB.85.115404,ref:JPhysConfSer}.
\begin{thebibliography}{6}
\bibitem{ref:skylaris:084119}C.-K.~Skylaris, P.~D.~Haynes, A.~A.~Mostofi,
and M.~C.~Payne, J. Chem. Phys. \textbf{122}, 084119 (2005).

\bibitem{ref:PRB_ORegan_2011}D.\ D. O'Regan, M.\ C. Payne and A.\ A.
Mostofi, Phys. Rev. B \textbf{83}, 245124 (2011).

\bibitem{ref:PRB_ORegan_2012}D.\ D. O'Regan, N.\ D.\ M. Hine,
M.\ C. Payne and A.\ A. Mostofi, Phys. Rev. B \textbf{85}, 085107
(2012).

\bibitem{ref:PhysRevB.83.241402}P.~W.~Avraam, N.~D.~M.~Hine,
P.~Tangney, and P.~D.~Haynes, Phys. Rev. B \textbf{83}, 241402(R)
(2011).

\bibitem{ref:PhysRevB.85.115404}P.~W.~Avraam, N.~D.~M.~Hine,
P.~Tangney, and P.~D.~Haynes, Phys. Rev. B \textbf{85}, 115404
(2012).

\bibitem{ref:JPhysConfSer}N.~D.~M.~Hine, P.~W.~Avraam, P.~Tangney,
and P.~D.~Haynes, J. Phys. Conf. Ser. (2012).
\end{thebibliography}

\end{document}
