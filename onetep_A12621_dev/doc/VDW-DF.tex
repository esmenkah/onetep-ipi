\documentclass[11pt,english]{article}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage[a4paper]{geometry}
\geometry{verbose,lmargin=3cm,rmargin=3cm}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{esint}
\makeatletter
\usepackage{latexsym}
\usepackage{amsthm}
\usepackage{bm}
\makeatother
\usepackage{babel}

\newcommand{\pd}[2]{\frac{\partial{#1}}{\partial{#2}}}
\newcommand{\ee}{\varepsilon}
\newcommand{\mgd}{|\nabla{\rho}|}
\newcommand{\rr}{\mathbf{r}}
\begin{document}

\title{Using van der Waals Density Functionals in ONETEP}

\author{Lampros Andrinopoulos \\
 Imperial College London}

\date{November 2012}

\maketitle

\section*{Activating vdW-DF}

The van der Waals energy is calculated in ONETEP using the van der Waals Density
Functional method, developed by Dion \textit{et al} \cite{ref:DION}.

The only input variable needed to activate the vdW-DF functional is to set
\texttt{xc\_functional VDWDF}. If a \texttt{vdW\_df\_kernel} file is not
present in the working directory, it will be automatically generated.

\section*{Theory}

The form for the exchange-correlation functional proposed by Dion \textit{et al}
is:

\begin{equation}\label{E_xc}
E_{xc} = E_{x}^{\rm{revPBE}} + E_c^{\rm{PW92}} + E_c^{\rm{nl}}
\end{equation}
where the non-local exchange-correlation energy is given by:
\begin{equation}\label{E_nl_dion}
E_{c}^{\mathrm{nl}} = \frac{1}{2}\int\int d\rr d\rr '\rho(\rr)\phi(\rr,\rr ')\rho(\rr ')
\end{equation}
where $\rho(\rr)$ is the electron density at $\rr$ and $\phi(\rr,\rr')$ is the
nonlocal exchange correlation kernel whose form is explained in \cite{ref:DION}.

\subsection*{Non-local correlation energy}

The direct calculation of the integral in the form of Eq. \ref{E_nl_dion} is very
computationally expensive, as it involves a six-dimensional spatial integral.

The algorithm proposed later by Roman-Perez and Soler \cite{ref:SOLER} improves the
efficiency of the calculation. They observed that with the form used by Dion
\textit{et al} for $\phi$, the above expression can be re-written as:
\begin{equation}
E_{c}^{\mathrm{nl}} = \frac{1}{2}\int\int d\rr d\rr '\rho(\rr)\phi(q,q',r)\rho(\rr ')
\end{equation}
where $r=|\rr-\rr '|$, and $q$ and $q'$ are the values of a universal function
$q_0[\rho(\rr),|\nabla \rho(\rr)|]$ at $\rr$ and $\rr'$.
They thus proposed a way to expand the kernel $\phi$ using interpolating polynomials
$p_\alpha(q)$ for chosen values $q_\alpha$ of $q$, and tabulated functions
$\phi_{\alpha\beta}(r)$ for the kernel corresponding to each pair of interpolating
polynomials. The interpolating polynomials $p_{\alpha}$ are cubic splines that
evaluate to a Kronecker delta on each respective interpolating point.
A mesh of 20 interpolation points is used in Soler's implementation.
The Soler form of the nonlocal energy can be written as:
\begin{equation}\label{kernel}
\phi(q_1,q_2,r) = \sum_{\alpha\beta}\phi_{\alpha\beta}(r) p_{\alpha}(q_1) p_{\beta}(q_2)
\end{equation}

The universal function $q_0(\rr)$ is in practice given by:
\begin{equation}\label{q0}
q_0(\rr) = \Bigg(1 + \frac{\epsilon_c^{\rm{PW92}}}{\epsilon_x^{\rm{LDA}}} + 
\frac{0.8491}{9}\Big(\frac{\nabla\rho}{2\rho k_F}\Big)^2\Bigg) k_F
\end{equation}
with $k_F=(3\pi^2\rho)^{1/3}$. The quantity $q_0$ is first "saturated" to limit its
maximum value, according to:
\begin{equation}
    q_0^{\text{sat}}(\rho,\mgd) = q_c \Bigg(1-\exp\Big(-\sum_{m=1}^{m_c}\frac{(q/q_c)^m}{m}\Big)\Bigg)
\end{equation}
where $q_c$ is the maximum value of the mesh of $q_{\alpha}$.

To evaluate this, we first define a quantity 
$\theta_{\alpha}(\rr) = \rho(\rr) p_{\alpha}(q(\rho(\rr),\nabla\rho(\rr))$
in real space. In terms of this, Eq.\ref{E_nl_dion} can be written as:
\begin{equation}\label{E_nl_real}
E_c^{\mathrm{nl}} = \frac{1}{2} \sum_{\alpha\beta} \int \int d\rr d\rr '
\theta_{\alpha}(\rr) \theta_{\beta}(\rr ') \phi_{\alpha\beta}(r)
\end{equation}

It can be shown that this can be written as a reciprocal space integral:
\begin{equation}\label{E_nl}
    E_c^{\mathrm{nl}} = \frac{1}{2} \sum_{\alpha\beta}\int d\mathbf{k} 
    \theta^{*}_{\alpha}(\mathbf{k})\theta_{\beta}(\mathbf{k})\phi_{\alpha\beta}(k)
\end{equation}

Since the kernel is radially dependent in real space, it is only dependent on the
magnitude of the G-vectors, hence the kernel need only be evaluated as a 1-dimensional
function $\phi_{\alpha\beta}(k)$ for each $\alpha$, $\beta$.

The kernel $\phi$ and its second derivatives are tabulated for a specific set of radial
points and transformed to reciprocal space. These values are then used to interpolate
the kernel at every point $\mathbf{k}$ in reciprocal space required to calculate
Eq. \ref{E_nl}.

\subsection*{Kernel}

This section details the evaluation of the NLXC kernel. The kernel $\phi(\rr,\rr ')$ as
specified by Dion \textit{et al} \cite{ref:DION} is given by (in atomic units):

\begin{equation}
    \phi(\rr,\rr ') = \frac{1}{\pi^2}\int_{0}^{\infty}a^2da 
    \int_0^{\infty}b^2db W(a,b) T(\nu(a),\nu(b),\nu'(a),\nu'(b))
\end{equation}
where 
\begin{equation}
    T(w,x,y,z) = \frac{1}{2}\Big[\frac{1}{w+x} + \frac{1}{y+z}\Big]\Big[\frac{1}{(w+y)(x+z)}+\frac{1}{(w+z)(y+x)}\Big],
\end{equation}
and
\begin{align}
    W(a,b) = 2\Big[ & (3-a^2)b\cos b \sin a \\
                    + & (3-b^2)a\cos a \sin b   \\
                    + & (a^2+b^2-3)\sin a\sin b \\
                    - & 3ab\cos a \cos b \Big]/(a^3b^3),
\end{align}
and
\begin{equation}
\nu(y) = 1- e^{-\gamma y^2/d^2}; \quad \nu'(y) = 1- e^{-\gamma y^2/d'^2};
\end{equation}
where $d=|\rr-\rr '|q_0(\rr)$, $d'=|\rr-\rr '|q_0(\mathbf{r'})$


Following this chain of logic, it is clear that this the kernel can in fact be considered as
a function only of $|\rr-\rr'|$, $q_0(\rr)$ and $q_0(\rr')$, since all other variables
are dummy variables which are integrated over. The kernel can therefore be written as
\begin{equation}\label{phi_tab}
\phi(r,q_0(\rr),q_0(\rr '))
\end{equation}
This makes it possible to evaluate the integrals above so as to tabulate the kernel
values numerically for a pre-chosen set of radial points and $q_0$ values. 

\subsection*{Non-local potential}

Starting from Eq. \ref{E_nl}, one can evaluate the potential $v^{\mathrm{nl}}(\rr)$
corresponding to this energy, by evaluating all terms in $\partial E_{\mathrm{nl}} /
\partial n(\rr)$. The non-local potential $v_i^{\mathrm{nl}}$ at point $\rr_i$ on
the grid is thus written explicitly in terms of the derivatives of the
$\theta_{\alpha}$ quantities with respect to the values $\rho_j$ at all other points
on the grid:
\begin{equation}\label{v_nl}
v_i^{\mathrm{nl}} = \sum_{\alpha}(u_{\alpha i}\pd{\theta_{\alpha i}}{\rho_i}+\sum_j u_{\alpha j}
\pd{\theta_{\alpha j}}{\nabla\rho_j}\pd{\nabla\rho_j}{\rho_i})
\end{equation}
This makes use of the quantities
$u_\alpha(\rr)= \sum_{\beta}\mathcal{F}(\theta_{\beta}(\mathbf{k})\phi_{\alpha\beta}(k))$:
which are already calculated in the evaluation of the energy.

Using the White and Bird \cite{ref:WnB} approach, Eq. \ref{v_nl} can be written as:
\begin{equation}\label{v_nl_wnb}
v_{\mathrm{nl}}(\rr) = \sum_{\alpha} \Big( 
  u_{\alpha}(\rr)\pd{\theta_{\alpha}(\rr)}{\rho(\rr)} 
  - \int\int i\mathbf{G}\cdot \frac{\nabla\rho(\rr ')}{|\nabla\rho(\rr ')|}
  \pd{\theta_{\alpha}(\rr ')}{|\nabla\rho(\rr ')|}e^{i\mathbf{G}\cdot (\rr-\rr ')} d\rr d\mathbf{G}
  \Big)
\end{equation}

For this we need to calculate $\pd{\theta}{\rho}$ and $\pd{\theta}{\mgd}$:

\begin{align}\label{dtheta_drho}
    \pd{\theta_\alpha}{\rho} &=p_\alpha + \rho \pd{p_\alpha}{\rho} \nonumber \\
                                               &=p_\alpha + \rho \pd{p_\alpha}{q}\pd{q}{\rho} \nonumber \\
                                               &=p_\alpha + \rho \pd{p_\alpha}{q} \frac{q}{k_F} \pd{k_F}{\rho} + \rho \pd{p_\alpha}{q} k_F (\pd{\ee_c}{\rho}\ee_x^{-1}-\ee_c\ee_x^{-2}\pd{\ee_x}{\rho} - \frac{8}{3(3\pi^2)^{2/3}}\frac{Z}{4}(\nabla\rho)^2 \rho^{-11/3}) \nonumber \\
                                               &=p_\alpha + q/3\pd{p_\alpha}{q} + k_F\rho \pd{p_\alpha}{q} (\pd{\ee_c}{\rho}\ee_x^{-1}-\ee_c\ee_x^{-2}\pd{\ee_x}{\rho}- \frac{2Z}{3(3\pi^2)^{2/3}} \mgd^2\rho^{-11/3})
\end{align}

\begin{equation}\label{dtheta_dgradrho}
    \pd{\theta_\alpha}{\mgd} = \rho \pd{p_\alpha}{q} \pd{q}{\mgd} = \frac{Z}{2\rho k_F} \rho \pd{p_\alpha}{q}\mgd
\end{equation}

Combining Eqs.~\ref{v_nl_wnb}, \ref{dtheta_drho} and \ref{dtheta_dgradrho} gives us
the final expression for the nonlocal potential.

\section*{Overview of computational algorithm}

\subsection*{Module \texttt{nlxc\_mod}}

The main module for the calculation of the non-local energy and potential is
\texttt{nlxc\_mod}. The tabulation of the kernel $\phi$ is performed only
if a kernel file is not found, by \texttt{vdwdf\_kernel}.

The input required to calculate the non-local energy and potential is essentially
just the density and its gradient on the fine grid. The calculation of $q$
and the Fourier transformed $\theta_\alpha$ from Eq. \ref{E_nl} is performed
first, in the routine \texttt{nlxc\_q0\_theta}.
The derivatives of the $\theta_\alpha$s with respect to the
density and the module of its gradient are calculated on-the-fly
in the real-space loop for the calculation of the non-local potential $v_nl$
in Eq. \ref{v_nl}. This is to avoid storing unnecessary arrays.
From Eq. \ref{v_nl_wnb} two transforms are required per $\alpha$ value,
a forward FFT, followed by a backward FFT for calculating the non-local potential. 

Subroutines to interpolate the polynomials as well as the kernel using cubic
splines are used (\texttt{spline\_interp} and \texttt{interpolate}). The
interpolating polynomials $p_\alpha$ used are Kronecker deltas, so they take
the value 1 on the interpolating point and are zero at the other points.

\subsection*{Module \texttt{vdwdf\_kernel}}

The kernel $\phi_{\alpha\beta}(k)$ is tabulated for 1024 radial reciprocal space 
points and 20 $q_0$ points.
Gaussian quadrature is used to calculate Eq. \ref{phi_tab} and then the result 
is Fourier transformed. The second derivatives of the kernel are calculated by
interpolation, and also tabulated. The default name of the file is
\texttt{vdw\_df\_kernel}. The program will first check if this file exists: if
it does, it will be loaded in and need not be calculated. If it does not, it
will be generated from scratch (which only takes a few minutes) and then it
is written out for future re-use in the current working directory.

The format of the \texttt{vdw\_df\_kernel} file is:\\
\texttt{}%
\begin{minipage}[t]{1\columnwidth}%
\texttt{N\_alpha} \texttt{N\_radial} \\
\texttt{max\_radius} \\
\texttt{q\_points(:)} \\
\texttt{kernel(0:N\_radial,alpha,beta)} \\
\texttt{kernel2(0:N\_radial,alpha,beta)}
\texttt{\vskip0.0cm}
\end{minipage}
where \texttt{kernel2} is the array of second derivatives of the kernel): 

\begin{thebibliography}{6}
\bibitem{ref:DION} M. Dion, H. Rydberg, E. Schr{\"o}der, D. C. Langreth, 
and B. I. Lundqvist, Phys. Rev. Lett. \textbf{92}, 246401 (2004).

\bibitem{ref:SOLER} G. Rom{\'a}n-P{\'e}rez and J. M. Soler, Phys. Rev.
Lett. \textbf{103}, 096102 (2009).

\bibitem{ref:WnB} J. A. White and D. M. Bird, Phys. Rev. B \textbf{50},
4954 (1994).

\end{thebibliography}

\end{document}
