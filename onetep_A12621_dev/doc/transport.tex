
\documentclass[12pt]{article}

\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{verbatim}
\usepackage{xspace}
\usepackage[table]{xcolor}
\usepackage{longtable}
\usepackage{times}
\usepackage{soul}

\def\onetep{{\sc onetep}\xspace}
\def\castep{{\sc castep}\xspace}
\def\unit#1{\ensuremath{\, \mathrm{#1}}}
\def\fig#1{Fig.~\ref{#1}}

\renewcommand\Re{\operatorname{Re}}
\renewcommand\Im{\operatorname{Im}}

\def\bfmatrix#1{\mathbf{#1}}
\def\bfvector#1{\boldsymbol{#1}}
\def\sfmatrix#1{\mathsf{#1}}
\def\zero{\cdot}
\def\hc{^{\dagger}}

% parameter: #1=name #2=type #3=options #4=default #5=description
\newcommand{\parameter}[5]{
  \begin{tabular}{p{\textwidth}}
  \hline
  {\tt #1: #3}\cellcolor{black!10}\\
  \begin{tabular}{p{0.95\textwidth}}
  \rule{0pt}{6mm}{[}#2, default {\tt #1: #4}{]}\\
  \rule{0pt}{6mm}#5 \\
  \end{tabular} \\
  \end{tabular}
  \hspace{8mm}
  \\
}
 
\newcommand{\block}[3]{
  \begin{tabular}{p{\textwidth}}
  \hline
  {\tt #1: {[}block{]}}\cellcolor{black!10}\\
  \begin{tabular}{p{0.95\textwidth}}
  \rule{0pt}{6mm}{\tt \begin{tabular}[c]{@{}l@{}}#2\end{tabular}} \\
  \rule{0pt}{6mm}#3
  \end{tabular} \\
  \end{tabular}
  \hspace{8mm}
  \\
}
 

\begin{document}

\title{Electronic transport calculations in \onetep}

\date{\today}
\author{Robert~A.~Bell\\
   \\
   Theory of Condensed Matter Group\\
   Cavendish Laboratory\\
   University of Cambridge\\
   \texttt{rab207@cam.ac.uk}
   }

\maketitle

\section{Introduction}
This document describes the use of the electronic transport functionality that is implemented in the \onetep code\cite{ONETEPtransport}.
The implementation computes the ballistic Landauer-B\"{u}ttiker conductance at zero bias through a device and associated properties using electronic structure derived from density functional theory (DFT).
The calculation proceeds as a post-processing step after a ground-state single-point calculation.

This document focuses on the practical aspects of setting up calculations; a detailed technical explanation of the method can be found in \cite{ONETEPtransport}.
For a detailed discussion of the Landauer-B\"{u}ttiker formalism and conductance derived from DFT see \cite{DiVentra2008,Datta1995}.

I first briefly outline what is being calculated, I then give an example calculation and suitable input parameters, and then finally a full input parameter listing is given.


\section{What is being calculated?}
The transport calculation determines the ballistic conductance through a device.
Current can flow between two leads via the connection made by the central scattering region.
We will refer to this geometry as the \emph{device} geometry.

Under the Landauer formalism, the contribution to the conductance between two leads (indexed $i$ and $j$) from electrons with energy $E$ is given by
\begin{equation}
   G_{ij}(E) = \frac{2e^2}{h} T_{ij}(E)
\end{equation}
where $T_{ij}(E)$ is the transmission function.
The transmission function is the central quantity to the Landauer-B\"{u}ttiker conductance and is calculated using a Green's function technique:
\begin{align}
   T_{ij}(E) &= \mathrm{tr}\Big[\sfmatrix{\Gamma}_i(E)\;\sfmatrix{G}_{\rm d}(E)\;\sfmatrix{\Gamma}_j(E)\;\sfmatrix{G}_{\rm d}^{\dagger}(E)\Big], \label{eq:caroli} \\
   \sfmatrix{G}_{\rm d}(E) &= \big[ (E+i\eta)\sfmatrix{S}_{\rm d} - \sfmatrix{H}_{\rm d} - \sfmatrix{\Sigma}(E)\big]^{-1},
   \label{eq:greenf}
\end{align}
where $\sfmatrix{H}_{\rm d}$, $\sfmatrix{S}_{\rm d}$ the Hamiltonian and overlap matrices for the device, and $\eta$ is an infinitessimal positive energy that selects the retarded response of the Green's function $\sfmatrix{G}_{\rm d}$.
The interaction with the semi-infinite leads accounted for through the lead self-energies $\sfmatrix{\Sigma}_j$, with $\sfmatrix{\Sigma} = \sum_j \sfmatrix{\Sigma}_j$. 
The coupling matrices are defined as $\sfmatrix{\Gamma}_j = i(\sfmatrix{\Sigma}_j - \sfmatrix{\Sigma}_j^{\dagger})$. 
 
From the Green's function, one may also calculate the density of states within the device region as
\begin{equation}
   \mathcal{N}(E) = -\frac{1}{\pi}\Im\mathrm{tr}\big[\sfmatrix{G}_{\rm d}(E) \sfmatrix{S}_{\rm d}\big].
\end{equation}
Further post processing steps may be performed to determine the eigenchannels.\cite{ONETEPtransport,Paulsson2007}

\section{Building the device}
The principle quantities required in Eqns.~\ref{eq:caroli} and \ref{eq:greenf} are the device Hamiltonian and overlap matrices, and the lead self energies.
The approach adopted in this implementation is to extract these matrix elements from the ground-state electronic structure obtained after a standard \onetep single-point calculation.
This approach corresponds to a non-self consistent calculation of the transmission spectrum and device electronic structure.
Note that this procedure differs from self consistent approaches, such as e.g. \cite{Brandbyge2002}, where the open boundary conditions of the leads are treated explicitly.
At zero bias, however, both methods produce equivalent results yet the non-self consistent approach is often far less computationally expensive.


A detailed description of the matrix element extraction procedure, as implemented in the \onetep code, is given in \cite{ONETEPtransport} which should be referred to in the first instance.
Summarising briefly, to calculate the transmission through any device geometry the user must construct another \emph{auxilliary simulation} structure that contains this device.
The \emph{only} electronic structure that is calculated is that of the auxilliary simulation geometry.

The structure of the auxilliary simulation geometry must take a specific form, consisting of the central region that connects the leads and a bulk-like region for each lead.
Each lead region must have a local electronic structure that is bulk-like, and be large enough that it contains at least two principle layers of the bulk lead structure.\footnote{A lead principle layer is defined as the minimum number of primitive unit cells for that lead such that Hamiltonian and overlap matrix elements between atoms in non-adjacent principle layers are exactly zero. This is guaranteed to be true if the periodic length of the principle layer is larger than twice the maximum NGWF radius.}
The device geometry is then defined as all the atoms contained within the central region and the two lead principle layers for each lead.

For a device connecting two leads, the device Hamiltonian then looks like
\begin{equation}
   \sfmatrix{H}_{\rm d} = 
   \begin{pmatrix}
      \bfmatrix{h}_{00,L}    & \bfmatrix{h}_{01,L}  & \zero                & \zero                  & \zero              \\
      \bfmatrix{h}\hc_{01,L} & \bfmatrix{h}_{00,L}  & \bfmatrix{h}_{LC}    & \zero                  & \zero              \\
      \zero                  & \bfmatrix{h}\hc_{LC} & \bfmatrix{h}_{C}     & \bfmatrix{h}_{CR}      & \zero              \\
      \zero                  & \zero                & \bfmatrix{h}\hc_{CR} & \bfmatrix{h}_{00,R}    & \bfmatrix{h}_{01,R}\\
      \zero                  & \zero                & \zero                & \bfmatrix{h}\hc_{01,R} & \bfmatrix{h}_{00,R}
   \end{pmatrix}
\end{equation}
where $\bfmatrix{h}_{00,L}$ is the on-site Hamiltonian block for the left lead principle layer, $\bfmatrix{h}_{01,L}$ is the coupling Hamiltonian between principle layers, $\bfmatrix{h}_{LC}$ is the coupling block between the left lead principle layer and the central region, and $\bfmatrix{h}_{C}$ is the on-site block for the central region.
As a result of the localisation of the NGWFs the device Hamiltonian takes the block tri-diagonal form, with zero matrix blocks denoted by a dot.
An equivalent form is found for the device overlap matrix.

The lead self energies are computed using the $\bfmatrix{h}_{00,L}, \bfmatrix{h}_{01,L}$ blocks via the method of Ref.~\cite{Lopez-Sancho1985}. Note that these blocks are contained within the device Hamiltonian $\sfmatrix{H}_{\rm d}$.

The device matrix elements are extracted from the electronic structure of the auxilliary simulation geometry.
This procedure is entirely automated, however, and the user need only supply a list of atoms that define the device, and the subsets of these atoms that form the two principle layers for each lead.
\emph{i.e.} for an $n$-lead device, the user must define $2n+1$ sets of atoms.

A single limitation of the implementation is that the two principle layers of any lead must be the exact same structure \emph{with atoms in the same relative ordering}. This ensures that the lead self energies are computed correctly.
The implementation will not do this automatically, however a check is performed to ensure that it is true before the calculation is started.

For a more detailed discussion of the procedure for extracting matrix elements see \cite{ONETEPtransport}.

\section{Setting up a calculation}
The best way to explain how to set up an input is through an example, and here I give an explanation of the parameters required to calculation the transmission between two semi-infinite organic wires.
Specifically, I will explain the setup for calculating the tunnelling current between a  polyacetylene wire and polyyne wire where each wire is semi-infinite and terminated with a hydrogen atom.
I will, however, assume that the user knows how to successfully converge the standard \onetep single-point calculation.

A suitable auxiliary simulation geometry is shown in schematic form in \fig{fig:example}, consisting of molecular fragments of the polyacetylene and polyyne wires located in vacuum.
This entire geometry contains 52 atoms. I will assume that these atoms are ordered in the input file by their position from left to right.

Suppose that I wish to calculate the transmission spectrum in a $\pm 2\unit{eV}$ window about the Fermi energy with a resolution of $0.01\unit{eV}$. The transport specific input parameters are as follows:
{
\ttfamily
\begin{verbatim}
   
   etrans_lcr               : T
   etrans_bulk              : T
   etrans_emin              : -2.0 eV
   etrans_emax              :  2.0 eV 
   etrans_enum              :  401
   etrans_calculate_lead_mu : T
   
   %block etrans_setup
     10  47
   %endblock etrans_setup

   %block etrans_leads
     10  17  18  25  unit_cells=2
     44  47  40  43  unit_cells=2
   %endblock etrans_leads
   
   ! rest of input file...

\end{verbatim}
}

The first six lines indicate that we wish to calculate the transmission between the two leads (the LCR transmision, see \cite{ONETEPtransport}), and the maximum transmission that could be injected by each lead (the bulk transmission), and the energy range the calculation will be performed over.
I am also indicating that I want to calculate the lead band structures.

The first block after these six lines is the {\ttfamily etrans\_setup} block which states that all atoms between atom $10$ and $47$ (inclusive, and in the order found in the input file) are defined as the device geometry.
This region is given by the long-dashed box in \fig{fig:example}.
Atoms outside this set will not be included in the transmission calculation, but will be used when calculating the ground-state electronic structure.

The final block is the {\ttfamily etrans\_leads} block which gives the subset of these device atoms that define the two principle layers for each lead.
Each lead is given on a separate line, with the first pair of indices defining the principle layer furthest from the central region, and the second pair the principle layer closest to the central region.
Note that this ordering of the two principle layers is important.
Finally, I have indicated that each principle layer is in fact two primitive unit cells of the lead structure by using {\ttfamily unit\_cells=2}.
This tells \onetep to symmetrise the lead matrix elements to reflect this periodicity.
The atoms contained within each principle layer are indicated in \fig{fig:example} by dotted boxes.

This choice of auxilliary simulation geometry tries to ensure that the lead principle layers are sufficiently far from the ends of the molecular fragments that the local electronic structure within the lead is bulk-like.
This has required the use of buffer atoms (atoms 1--9 and 48--52) that are not included in the transport calculation.
Some buffer has also been used between the principle layers and the central tunnelling gap (atoms 26--34 and 35--39) for the same reason.
In practice, the size of this buffer is probably too small and the lead principle layers will not be well converged to the bulk.
This can be tested by comparing the band structures of each lead to the corresponding bulk band structure for that lead, which can be calculated separately.
If the lead band structure is not converged, the size of the buffer region should be increased.



  \noindent
  \begin{figure}
  \scriptsize
  \setlength{\unitlength}{\textwidth}
  \begin{picture}(1,0.30)
    \put( 0.51127977,0.17){\makebox(0,0)[lb]{\smash{HC}}}
    \put( 0.55569943,0.17){\makebox(0,0)[lb]{\smash{C}}}
    \put( 0.58609980,0.17){\makebox(0,0)[lb]{\smash{C}}}
    \put( 0.61555545,0.17){\makebox(0,0)[lb]{\smash{C}}}
    \put( 0.64595583,0.17){\makebox(0,0)[lb]{\smash{C}}}
    \put( 0.67541148,0.17){\makebox(0,0)[lb]{\smash{C}}}
    \put( 0.70581186,0.17){\makebox(0,0)[lb]{\smash{C}}}
    \put( 0.73526751,0.17){\makebox(0,0)[lb]{\smash{C}}}
    \put( 0.76566788,0.17){\makebox(0,0)[lb]{\smash{C}}}
    \put( 0.79512353,0.17){\makebox(0,0)[lb]{\smash{C}}}
    \put( 0.82552391,0.17){\makebox(0,0)[lb]{\smash{C}}}
    \put( 0.85497956,0.17){\makebox(0,0)[lb]{\smash{C}}}
    \put( 0.88537999,0.17){\makebox(0,0)[lb]{\smash{C}}}
    \put( 0.91483559,0.17){\makebox(0,0)[lb]{\smash{C}}}
    \put( 0.94523602,0.17){\makebox(0,0)[lb]{\smash{C}}}
    \put( 0.97469161,0.17){\makebox(0,0)[lb]{\smash{CH}}}
    
    \put(  0.54124377,0.1787){\line(1,0){0.012000}}
    \put(  0.54124377,0.1760){\line(1,0){0.012000}}
    \put(  0.54124377,0.1733){\line(1,0){0.012000}}
    \put(  0.57069943,0.1760){\line(1,0){0.012000}}
    \put(  0.60109980,0.1787){\line(1,0){0.012000}}
    \put(  0.60109980,0.1760){\line(1,0){0.012000}}
    \put(  0.60109980,0.1733){\line(1,0){0.012000}}
    \put(  0.63055545,0.1760){\line(1,0){0.012000}}
    \put(  0.66095583,0.1787){\line(1,0){0.012000}}
    \put(  0.66095583,0.1760){\line(1,0){0.012000}}
    \put(  0.66095583,0.1733){\line(1,0){0.012000}}
    \put(  0.69041148,0.1760){\line(1,0){0.012000}}
    \put(  0.72081186,0.1787){\line(1,0){0.012000}}
    \put(  0.72081186,0.1760){\line(1,0){0.012000}}
    \put(  0.72081186,0.1733){\line(1,0){0.012000}}
    \put(  0.75026751,0.1760){\line(1,0){0.012000}}
    \put(  0.78066788,0.1787){\line(1,0){0.012000}}
    \put(  0.78066788,0.1760){\line(1,0){0.012000}}
    \put(  0.78066788,0.1733){\line(1,0){0.012000}}
    \put(  0.81012353,0.1760){\line(1,0){0.012000}}
    \put(  0.84052391,0.1787){\line(1,0){0.012000}}
    \put(  0.84052391,0.1760){\line(1,0){0.012000}}
    \put(  0.84052391,0.1733){\line(1,0){0.012000}}
    \put(  0.86997956,0.1760){\line(1,0){0.012000}}
    \put(  0.90037999,0.1787){\line(1,0){0.012000}}
    \put(  0.90037999,0.1760){\line(1,0){0.012000}}
    \put(  0.90037999,0.1733){\line(1,0){0.012000}}
    \put(  0.92983559,0.1760){\line(1,0){0.012000}}
    \put(  0.96023602,0.1787){\line(1,0){0.012000}}
    \put(  0.96023602,0.1760){\line(1,0){0.012000}}
    \put(  0.96023602,0.1733){\line(1,0){0.012000}}
    
    \put( 0.00500000,0.13478070){\makebox(0,0){\smash{H$_2$C}}}
    \put( 0.04799969,0.20165110){\makebox(0,0){\smash{H}}}
    \put( 0.04799969,0.18481659){\makebox(0,0){\smash{C}}}
    \put( 0.07592359,0.13478070){\makebox(0,0){\smash{C}}}
    \put( 0.07592359,0.1179462){\makebox(0,0){\smash{H}}}
    \put( 0.10411472,0.20165110){\makebox(0,0){\smash{H}}}
    \put( 0.10411472,0.18481659){\makebox(0,0){\smash{C}}}
    \put( 0.13203862,0.13478070){\makebox(0,0){\smash{C}}}
    \put( 0.13203862,0.1179462){\makebox(0,0){\smash{H}}}
    \put( 0.16022974,0.20165110){\makebox(0,0){\smash{H}}}
    \put( 0.16022974,0.18481659){\makebox(0,0){\smash{C}}}
    \put( 0.18815365,0.13478070){\makebox(0,0){\smash{C}}}
    \put( 0.18815365,0.1179462){\makebox(0,0){\smash{H}}}
    \put( 0.21634477,0.20165110){\makebox(0,0){\smash{H}}}
    \put( 0.21634477,0.18481659){\makebox(0,0){\smash{C}}}
    \put( 0.24426867,0.13478070){\makebox(0,0){\smash{C}}}
    \put( 0.24426867,0.1179462){\makebox(0,0){\smash{H}}}
    \put( 0.27245979,0.20165110){\makebox(0,0){\smash{H}}}
    \put( 0.27245979,0.18481659){\makebox(0,0){\smash{C}}}
    \put( 0.30038369,0.13478070){\makebox(0,0){\smash{C}}}
    \put( 0.30038369,0.1179462){\makebox(0,0){\smash{H}}}
    \put( 0.32857481,0.20165110){\makebox(0,0){\smash{H}}}
    \put( 0.32857481,0.18481659){\makebox(0,0){\smash{C}}}
    \put( 0.35649872,0.13478070){\makebox(0,0){\smash{C}}}
    \put( 0.35649872,0.1179462){\makebox(0,0){\smash{H}}}
    \put( 0.38468984,0.20165110){\makebox(0,0){\smash{H}}}
    \put( 0.38468984,0.18481659){\makebox(0,0){\smash{C}}}
    \put( 0.41261374,0.13478070){\makebox(0,0){\smash{C}}}
    \put( 0.41261374,0.1179462){\makebox(0,0){\smash{H}}}
    \put( 0.45348074,0.18497107){\makebox(0,0){\smash{CH$_2$}}}

    \put(0.0450,0.1758){\line(-5,-6){0.021000}}
    \put(0.0522,0.1768){\line( 5,-6){0.021000}}
    \put(0.0498,0.1749){\line( 5,-6){0.021000}}
    \put(0.1011,0.1758){\line(-5,-6){0.021000}}
    \put(0.1083,0.1768){\line( 5,-6){0.021000}}
    \put(0.1060,0.1749){\line( 5,-6){0.021000}}
    \put(0.1572,0.1758){\line(-5,-6){0.021000}}
    \put(0.1644,0.1768){\line( 5,-6){0.021000}}
    \put(0.1621,0.1749){\line( 5,-6){0.021000}}
    \put(0.2133,0.1758){\line(-5,-6){0.021000}}
    \put(0.2205,0.1768){\line( 5,-6){0.021000}}
    \put(0.2182,0.1749){\line( 5,-6){0.021000}}
    \put(0.2695,0.1758){\line(-5,-6){0.021000}}
    \put(0.2766,0.1768){\line( 5,-6){0.021000}}
    \put(0.2743,0.1749){\line( 5,-6){0.021000}}
    \put(0.3256,0.1758){\line(-5,-6){0.021000}}
    \put(0.3327,0.1768){\line( 5,-6){0.021000}}
    \put(0.3304,0.1749){\line( 5,-6){0.021000}}
    \put(0.3817,0.1758){\line(-5,-6){0.021000}}
    \put(0.3888,0.1768){\line( 5,-6){0.021000}}
    \put(0.3865,0.1749){\line( 5,-6){0.021000}}
    \put(0.4415,0.1758){\line(-5,-6){0.021000}}

    \put( 0.113,0.08){\dashbox{0.01}(0.765,0.2){}}
    \put( 0.75,0.05){\makebox(0,0){\smash{\footnotesize Device geometry: atoms 10--47}}}
    
    
    \put( 0.117,0.110){\dashbox{0.002}(0.110,0.12){}}
    \put( 0.231,0.110){\dashbox{0.002}(0.110,0.12){}}
    \put( 0.172,0.245){\makebox(0,0){\smash{10--17}}}
    \put( 0.286,0.245){\makebox(0,0){\smash{18--25}}}
    
    \put(  0.64,0.125){\dashbox{0.002}(0.114,0.1){}}
    \put(  0.76,0.125){\dashbox{0.002}(0.114,0.1){}}
    \put( 0.697,0.245){\makebox(0,0){\smash{40--43}}}
    \put( 0.817,0.245){\makebox(0,0){\smash{44--47}}}
    
    
    
  \end{picture}
  \caption{A possible auxilliary simulation geometry for calculating the tunnelling transmission between a polyacetylene wire and a polyyne wire. The atoms appear ordered in the input file by their position from left to right.}
  \label{fig:example}
\end{figure}

\section{Computational scaling}
The dominant computationally-intensive task in the transport routines is the computing of the device Green's function. This calculation is performed using an efficient block tri-diagonal Gaussian-elimination algorithm\cite{Petersen}. This algorithm results in memory usage and operation counts that scale linearly in the number of atoms. The pre-factor to this scaling depends on the precise geometry of the device: the more one-dimensional a device (i.e. the fewer NGWFs that overlap), the lower this pre-factor.

\section{Information on parallelisation}

The calculation of transmission coefficients is parallelised over the energy points, with each MPI process performing the calculation in serial. No internal communications are necessary making the routines scale perfectly with the number of MPI processes. However, as all matrices must be replicated on each MPI process, the memory requirements can be large. On entering the transport routines but prior to starting the calculation, an estimate of the additional memory required by the transport routines is printed. Note that this estimate does not include the memory required by the rest of the \onetep routines.

Parallelisation using OpenMP threading is not currently available. However, if the code is linked against Intel's Math Kernel Library (MKL), the linear algebra routines can make use of multi-threading within this library. To do this, the flag {\tt -DMKLOMP} must be included during compilation, and the number of threads can be set from the input file using {\tt threads\_num\_mkl}.

The calculation of the eigenchannels can only be parallelised if linking against the ScaLAPACK library to make use of parallel dense algebra. To enable this, the flag {\tt -DSCALAPACK} must be included at compilation.




\section{Calculations using the joint NGWF basis} 
Transmission calculations using the joint (valence + conduction) NGWF basis set have often been found to be numerically unstable and produce qualitively incorrect results when compared to calculations using the valence NGWF basis alone. This is a result of ill-conditioning of the joint basis when the valence and conduction NGWFs are very similar in character. This is a known issue, however there is currently no fix.

For some systems it has been found that the valence NGWFs alone are capable of describing low-energy conduction states with good accuracy, and therefore the joint basis is not needed. It is advised that the valence basis is used in the first instance.

If you are certain that the joint basis is required, then proceed with caution and always compare the results generated with the joint basis to those generated using the valence basis. For energies below the Fermi energy, the two calculations should coincide. If they do not, or if errors (e.g. complaining about computing the transfer matrix/self energy, or lead band structures) are found then it may not be possible to use the joint basis.

If using the joint NGWF basis, all output files have ~{\tt .joint} prepended to the extension.


\section{Warnings and fixing errors}
A useful check is to ensure that the lead band structure is as expected, and that the lead occupancy is correct. It is also useful to check the lead/device {\tt .xyz} files to ensure that the leads have the expected geometry.

The following are the main warning messages that may be encountered, and how they may be tackled.

\begin{description}
 \item[{\tt Inversion of (eS-H)\_lcr failed}] \hfill \\
   {\bf Cause}: Failed to calculate the Green's function as it is singular at this energy. This can happen if {\tt etrans\_ecmplx} is too small, or if localised states are present near this energy.\\
   {\bf Severity}: Low\\
   {\bf Fix}: It is safe to ignore this warning; the transmission coefficients and associated values are not written to file. If it occurs over a wide energy range, or in an energy range of interest, try increasing {\tt etrans\_ecmplx}.

 \item[{\tt Warning in compute\_transfer: Failed to compute transfer matrix.}] \hfill \\
   {\bf Cause}: Failed to compute the lead self energy. This can happen if {\tt etrans\_ecmplx} is too small, or if localised states are present near this energy. \\
   {\bf Severity}: Low\\
   {\bf Fix}: It is safe to ignore this warning; the transmission coefficients and associated values are not written to file. If it occurs over a wide energy range, or in an energy range of interest, try increasing {\tt etrans\_ecmplx}. If using the joint NGWF basis, this may indicate that the basis is ill-conditioned and that the calculation is numerically unstable.
 
 \item[{\tt lead electronic occupancy is significantly different ...}] \hfill \\
   {\bf Cause}: Large discrepancy between the ionic and electronic charge in the lead. Likely due to under-converged buffer region between this lead and the scattering region. \\
   {\bf Severity}: high \\
   {\bf Fix}: Check that the lead band structure is what is expected. Increase buffer region between the lead and the scattering region. Check the ground state DFT calculation is converged and that the density kernel occupancies are $0$ or $1$ (if using the default LNV algorithm).
   
 \item[{\tt Unable to determine lead potential for lead ...}] \hfill \\
   {\bf Cause}: lead potential calculation for this lead failed: could not determine the lead band structure. This may indicate that the leads have been defined incorrectly. Check the lead structure. \\
   {\bf Severity}: possibly high \\
   {\bf Fix}: Check the lead structure. Check the matrix elements contained in the lead {\tt .hsm} output file are resonable, that the main weight is along the matrix diagonal and that the values are sensible. If using joint NGWF basis, try using the valence basis only ({\tt task = properties} not {\tt properties\_cond}). The calculation will attempt to continue, but check carefully the results are as expected.
   
\end{description}


\pagebreak
\section{Full input parameter description}
\label{sec:full_parameters}

\subsection{Main parameters}

\begin{longtable}{p{\textwidth}}
 \parameter{etrans\_lcr}{Logical}{T/F}{F}{
   Calculate the transmission spectrum between all pairs of leads in the system.}
 
 \parameter{etrans\_bulk}{Logical}{T/F}{F}{
   Calculate the bulk transmission spectrum for the leads.}
 
 \block{etrans\_setup}{\%block etrans\_setup \\
      \-\hspace{0.5cm} start end \\
   \%endblock etrans\_setup}{
   The block defining the atoms to be used in the transport calculation. {\tt start/end} are integers giving the indices (from the input file) of the first/last atoms to be included as part of the system. All atoms between {\tt start} and {\tt end} are used in the calculation; atoms outside this range are buffer regions and ignored. The atoms constituting the leads must be contained within this region.}
 
 \block{etrans\_leads}{\%block etrans\_leads \\
      \-\hspace{0.5cm} start0 end0 start1 end1 \phantom{unit\_cells=<$n$> file=<lead2.hsm>}\\
      \-\hspace{0.5cm} start0 end0 start1 end1 unit\_cells=<$n$> file=<lead2.hsm>\\
      \-\hspace{0.5cm} \ldots \\
   \%endblock etrans\_leads}{
   The block defining the atoms to be used as the leads. Each line defines a new lead. 
   
   {\tt start0/end0} are integers giving the indices (from the input file) of the first/last atoms of the lead principle layer farthest from the central region; {\tt start1/end1} are integers giving the indices of the first/last atom of the principle layer closest to the central region. 
   
   The first principle layer geometry must be a periodic repeat of the lead geometry (i.e. same number of atoms, in the same relative ordering in the input file).
   
   For each lead, two optional tags can be defined. The option {\tt unit\_cells=<$n$>} forces the translational symmetry of the principle layer matrix elements, where $n$ is the number of primitive unit cells in the principle layer.
   The option {\tt file=<lead1.hsm>} allows for the lead matrix elements to be read in from a {\tt .hsm} file. Reading in matrix elements is generally unnecessary, however it may be used effectively by taking the {\tt .hsm} of a separate pristine/bulk transport calculation to ensure that the matrix elements are exactly at their bulk values. The positions and ordering of atoms in the lead principle layers in both calculation must be identical: this is not checked in the calculation.}
 
 \parameter{etrans\_enum}{Integer}{$n$}{$50$}{
 Number of transmission energy points calculated for. Energies are distributed uniformly.}

 \parameter{etrans\_emax}{Physical}{$E_{\mathrm{max}}$}{$-0.2 \unit{Hartree}$}{
 The maximum energy above the reference energy transmission is calculated for.}

 \parameter{etrans\_emin}{Physical}{$E_{\mathrm{min}}$}{$+0.2 \unit{Hartree}$}{
 The minimum energy below the reference energy transmission is calculated for.}

 \parameter{etrans\_eref\_method}{String}{{\tt LEADS|REFERENCE|DIAG}}{{\tt LEADS}}{
 The method used to determine the reference energy $E_{\mathrm{ref}}$ for the transmission calculation. 
 
 If {\tt{etrans\_eref\_method: LEADS}}, the reference energy is set as the average lead chemical potential.
 If {\tt{etrans\_eref\_method: DIAG}}, the reference energy is set as the Fermi energy of the original DFT system.
 If {\tt{etrans\_eref\_method: REFERENCE}}, the reference energy is set with {\tt etrans\_eref}.
 
 If unset, this will be determined by calculating the average chemical potential of the leads, or if that calculation fails, the Fermi energy of the original DFT system will be used.
}

 \parameter{etrans\_eref}{Physical}{$E_{\mathrm{ref}}$}{Determined automatically}{
 Reference energy around which the transmission will be calculated. Energies are distributed uniformly in range $E_{\mathrm{ref}}+E_{\mathrm{min}} \leq E \leq E_{\mathrm{ref}}+E_{\mathrm{max}}$.
   
   This value is only used if {\tt{etrans\_eref\_method: REFERENCE}}, otherwise it is determined automatically by the method given by {\tt etrans\_eref\_method}.}

 \parameter{etrans\_num\_eigchan}{Integer}{$n_{\mathrm{cham}}$}{$0$}{
 The number of eigenchannel transmissions calculated. The default does not decompose into eigenchannel transmissions.}

 \parameter{etrans\_plot\_eigchan}{Logical}{T/F}{F}{
   Whether to plot the transmission eigenchannels of the LCR system.}

 \block{etrans\_plot\_eigchan\_energies}{
   \%block etrans\_plot\_eigchan\_energies \\
      \-\hspace{0.5cm} \{Ha|eV\} \hspace{0.5cm}! optional energy unit\\
      \-\hspace{0.5cm} $E_1$\\
      \-\hspace{0.5cm} $E_2$\\
      \-\hspace{0.5cm} \ldots \\
   \%endblock etrans\_plot\_eigchan\_energies}{
   The energies at which the eigenchannels are calculated and plotted. The first line may, optionally, define the energy unit; if undefined, the energy unit is Hartree.
   
   Plotting the eigenchannels uses an algorithm that scales with the cube of the number of atoms in the LCR system. Compiling with the ScaLAPACK library is strongly recommended to reduce memory requirements.}
   
 \parameter{etrans\_write\_xyz}{Logical}{T/F}{T}{
   Whether to write, separately, the lead and LCR geometries to file in the .xyz file format. This is useful for ensuring that the correct atoms have been chosen for the leads/LCR region.}

\end{longtable}
\pagebreak
\subsection{Tweaking/optional parameters}
\begin{longtable}{p{\textwidth}}
 \parameter{etrans\_ecmplx}{Physical}{$\eta$}{$10^{-6}\unit{Hartree}$}{
 The small complex energy added to calculate the retarded Green's function.
   
 Ideally this should be infinitesimally small, however too small values create numerical instabilities. Large values improve numerical stability, but creates artificial scattering, reducing transmission.}
 
 \parameter{etrans\_calculate\_lead\_mu}{Logical}{T/F}{T}{
   Determine the chemical potential of the leads using a tight-binding approach. If {\tt etrans\_eref} is unset, the average lead chemical potential will be used as the reference energy. The band structure for each lead is saved to file {\tt basename\_lead$nn$.bands}.}
   
 \parameter{etrans\_num\_lead\_kpoints}{Integer}{$n_k$}{32}{
 The number of lead band structure $k$-points sampled to determine the lead chemical potential. The $k$-points are distributed uniformly between $\Gamma$ and $X$.}
 
 \parameter{etrans\_same\_leads}{Logical}{T/F}{F}{
 Reuse the self energy from one lead for all leads. If all leads are identical, this may lead to a small computational saving. The saving is typically negligible however.}
 
  \parameter{etrans\_write\_hs}{Logical}{T/F}{F}{
 Write the lead Hamiltonian and overlap matrices to disk.}
 
 \parameter{etrans\_lead\_disp\_tol}{Physical}{$\Delta r$}{$1.0a_0$}{
 The geometries of each lead and corresponding first principle layer should be identical; this parameter determines how strictly this is enforced. This should only be done if you are clear of the consequences.
 
 This may be useful in the case that the lead unit cell is much larger than the NGWF radius. If the distance between the lead and the first principle layer furthest from the lead is much larger than 2 NGWF radii, these atoms do not directly influence the non-zero matrix elements between the principle layer and the lead, and the coupling matrix $h_L$ is (approximately) independent of the structure of these atoms. Therefore, it may be possible to use a much smaller buffer region between the lead and the first principle layer by including part of the first principle layer in that buffer. The first principle layer and lead geometry need not be identical, however both regions must contain the same number of atoms and orbitals. 
   
 For each lead, the translation vector between the $i^{th}$ atoms of the lead and first principle layer is calculated: $\bfvector{R}_{i} = \bfvector{r}_{i\mathrm{,lead}} - \bfvector{r}_{i\mathrm{,PL}}$. If the geometries of the lead and first principle layer are identical, all of these translation vectors are identical. The maximum allowed difference between these displacement vectors is $\Delta r > |\bfvector{R}_{i} - \bfvector{R}_{j}|$.
 
 If the lead crosses the supercell boundary, this check will fail. This can be overridden by increasing $\Delta r$ to some very large value.}
 
 \parameter{etrans\_lead\_size\_check}{Logical}{T/F}{T}{
 If true, a check is performed to ensure that each lead forms complete principle layer. The run will abort if the periodic length of the lead unit cells, as defined in the input file, are smaller than twice the maximum NGWF radius of the species in that lead.
 
 Turning off this check is not advised as the lead band structure and self energies can be inaccurate. This can also be a problem in situations like in Fig.~\ref{fig:semi_infinite_cnts}, where the left lead can interact with the principle layer of the right lead, allowing current to flow between the leads in the wrong direction, bypassing the central scattering region.}

 \parameter{etrans\_seed\_lead}{Integer}{}{1}{
 The lead used to seed the block tri-diagonal partitioning. This should only be relevant for devices with more than two leads where one lead is much larger than the other leads.}

 \parameter{threads\_num\_mkl}{Integer}{}{1}{
 If the code is linked against Intel's Math Kernel Library (MKL), this defines the number of threads used in the linear algebra routines. The flag {\tt -DMKLOMP} must be set during compilation to enable this parameter, otherwise it is ignored.}
 
\end{longtable}

\section{Output file description}

Output files contain self-explanatory headers for each column. For spin polarised calculations, a separate file is outputted for each spin channel.

\begin{description}

 \item[{\tt basename\_LCR.TRC}] \hfill \\
   The LCR transmission coefficients between different pairs of leads of the system.
   
 \item[{\tt basename\_LCR\_channels\_lead$nn$.TRC}] \hfill \\
   The LCR transmission coefficients decomposed into eigenchannels. The lead number defines which lead acts as the source.

 \item[{\tt basename\_LCR\_E$nn$\_lead$mm$\_chan$ll$\_\{real|imag\}.cube}] \hfill \\
   The plotted LCR eigenchannel. $nn$ is the the energy index from \\{\tt \%block etrans\_plot\_eigchan\_energies}; $mm$ is the lead that acts as the source; $ll$ is the eigenchannel number. The real and imaginary part are printed separately.
 
 
 \item[{\tt basename\_BULK.TRC}] \hfill \\
   The bulk transmission coefficients for each lead of the system.
   
 \item[{\tt basename\_LCR.DOS}] \hfill \\
   The density of states for the LCR system.
   
 \item[{\tt basename\_BULK.DOS}] \hfill \\
   The density of states for each lead of the system.
 
 \item[{\tt basename\_lead$nn$.bands}] \hfill \\
   Bandstructure of the lead in the \castep~{\tt .bands} format. The number of $k$-points is set with {\tt etrans\_num\_lead\_kpoints}.
   
 \item[{\tt basename\_device.xyz ~ basename\_lead$nn$.xyz}] \hfill \\
   The geometries of the device and leads in .xyz file format.
   
 \item[{\tt basename\_lead$nn$.hsm}] \hfill \\
   The lead matrix elements in Fortran (unformatted) binary format. Note that all energies are in Hartree.
   It is planned, but currently not possible, to be able access the device matrix elements. Please contact the \onetep developers if you are interested in using this functionality.
   The format of this file is
   {
   \ttfamily
   \begin{verbatim}
 character(len=*) :: block_type  ! always lead
 character(len=*) :: ham_type    ! valence or joint
 integer          :: nspin       ! number of spin channels
 integer          :: orbs(4)     ! NGWF indices corresponding
                                 ! to the atoms defined
                                 ! in block_etrans_leads
 integer          :: norb        ! matrix sizes
 real(kind=8)     :: eref        ! the lead chemical potential
 
 real(kind=8)   :: h00(norb,norb,nspin)
 real(kind=8)   :: h01(norb,norb,nspin)
 real(kind=8)   :: s00(norb,norb)
 real(kind=8)   :: s01(norb,norb)
    \end{verbatim}
    }
 
\end{description}


\begin{thebibliography}{9}
 
 \bibitem{ONETEPtransport}
   R.A. Bell, S.M.-M. Dubois, M.C. Payne, A. A. Mostofi, \emph{in preparation} (2014)
 
 \bibitem{DiVentra2008}
   M.~Di~Ventra, \emph{Electrical Transport in Nanoscale Systems}, (Cambridge University Press, Cambridge 2008)
 
 \bibitem{Datta1995}
   S. Datta, \emph{Electronic Transport in Mesoscopic System}, 2nd ed. (Cambridge University Press, Cambridge 1995)
   
 \bibitem{Paulsson2007}
   M. Paulsson, M. Brandbyge, Phys. Rev. B. {\bf 76} 115117 (2007)
 
 \bibitem{Lopez-Sancho1985}
   M.P. Lopez-Sancho, J.M. Lopez-Sancho, J. Rubio, \emph{J. Phys. F: Met. Phys.} {\bf 15}, 851 (1985)

 \bibitem{Petersen}
   D.E. Petersen \emph{et al.}, \emph{J. Comp. Phys} {\bf 227}, 3174 (2008)
\end{thebibliography}

\end{document}
