\documentclass[11pt,english]{article}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage[a4paper]{geometry}
\geometry{verbose,lmargin=3cm,rmargin=3cm}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{esint}
\makeatletter
\usepackage{latexsym}
\usepackage{amsthm}
\usepackage{bm}
\makeatother
\usepackage{babel}

\newcommand{\pd}[2]{\frac{\partial{#1}}{\partial{#2}}}
\newcommand{\ee}{\varepsilon}
\newcommand{\mgd}{|\nabla{\rho}|}
\newcommand{\rr}{\mathbf{r}}
\begin{document}

\title{Empirical Dispersion Correction Within ONETEP}

\author{Max Phipps \\
 School of Chemistry, University of Southampton}

\date{October 2013}

\maketitle

\section*{Theory}

The modification to the total DFT energy when correcting for dispersion is given by,
\begin{equation}\label{disp_corr}
  E_{DFT-D} = E_{KS-DFT} + E_{disp}
\end{equation}
where the dispersion energy correction is given by:
\begin{equation}\label{E_disp}
  E_{disp} = -s_6 \sum^{N_{at}}_{i=1} \sum^{N_{at}}_{j=1,j\neq i} \dfrac{C_{6,ij}}{R^6_{ij}} f_{damp}( | \overrightarrow{R_i} - \overrightarrow{R_j} | )
\end{equation}
where $s_6$ is a DF-dependent global scaling factor, $C_{6,ij}$ is a dispersion coefficient for the atom pair $ij$, 
$R_{ij}$ is the atom pair distance, $f_{damp}(| \overrightarrow{R_i} - \overrightarrow{R_j} |)$ is a damping function that is unity at large
distances $| \overrightarrow{R_i} - \overrightarrow{R_j} |$ and 0 at small distances~\cite{ref:Hill09,ref:Grimme06}.
These coefficients and the form of the damping function are dependent upon the empirical vdW correction model adopted.
The damping functions for the different models are given below:
\begin{center}
    \renewcommand{\arraystretch}{1.4}
    \begin{tabular}{|c|p{6cm}|p{7cm}|}   
    \hline 
     Keyword  & Description & Damping Function $f_{damp}( | \overrightarrow{R_i} - \overrightarrow{R_j} | )$ Form \\ \hline 
    \verb ELSTNER  & Damping function of Elstner~\cite{ref:Elstner01}.               &  
\begin{equation} (1-\exp^{-c_{damp}((| \overrightarrow{R_i} - \overrightarrow{R_j} |)/R_{0,ij})^7})^4 \nonumber \end{equation}  \\ 
    \verb WUYANG1  & First damping function of Wu and Yang~\cite{ref:Wu02}.          &  
\begin{equation} (1-\exp^{-c_{damp}((| \overrightarrow{R_i} - \overrightarrow{R_j} |)/R_{0,ij})^3})^2 \nonumber \end{equation}  \\ 
    \verb WUYANG2  & Second damping function of Wu and Yang~\cite{ref:Wu02}.         &  
\begin{equation} \frac{1}{1+\exp^{-c_{damp}((| \overrightarrow{R_i} - \overrightarrow{R_j} |)/R_{0,ij}-1)}} \nonumber \end{equation}  \\ 
    \verb GRIMMED2 & Damping function of D2 correction of Grimme~\cite{ref:Grimme06}.&  
\begin{equation} \frac{1}{1+\exp^{-c_{damp}((| \overrightarrow{R_i} - \overrightarrow{R_j} |)/R_{0,ij}-1)}} \nonumber \end{equation}  \\ \hline 
    \end{tabular}
\end{center}
where $c_{damp}$ is a damping constant (referred to as $d$ within the literature of Grimme~\cite{ref:Grimme06} and $c_{damp}$ by Hill~\cite{ref:Hill09})
and $R_{0,ij}$ is determined by the vdW radii of the atomic pair $i$ and $j$.

\section*{Activating the dispersion corrections}

Four vdW correction options have been implemented within ONETEP.
Activation of the vdW corrections within ONETEP is achieved using the \verb DISPERSION  keyword followed by a dispersion correction setting.
eg.  For the D2 correction of Grimme~\cite{ref:Grimme06},  \begin{verbatim} DISPERSION GRIMMED2 \end{verbatim}
The exchange-correlation functionals available with optimized parameters for the dispersion models are given below:
\begin{center}
    \begin{tabular}{|c|p{8cm}|}   
    \hline 
     Keyword  & Available XC Functionals \\ \hline 
    \verb ELSTNER  & BLYP, PBE, PW91, REVPBE, RPBE, XLYP. \\ 
    \verb WUYANG1  & BLYP, PBE, PW91, REVPBE, RPBE, XLYP. \\ 
    \verb WUYANG2  & BLYP, PBE, PW91, REVPBE, RPBE, XLYP. \\ 
    \verb GRIMMED2 & BLYP, PBE, B3LYP. \\ \hline 
    \end{tabular}
\end{center}

The $C_{6,ij}$ coefficients and the coefficients of the damping function of dispersion corrections 1--3 are optimized for each xc functional
to minimize the root mean square difference of the interaction energy from the literature values for a selection of dispersion-dominant
biological complexes from the JSCH-2005 and S22 sets~\cite{ref:Jurecka06} and the database of Morgado~\cite{ref:Morgado07}.
For the D2 correction of Grimme the parameters are optimized as described in the literature~\cite{ref:Grimme06}.

The $s_6$ parameters are unity for dispersion corrections 1--3 and are fitted by least squares optimization of interaction energy error for 40 noncovalently bound complexes
for the D2 correction of Grimme.

In the case of using an unoptimized functional not given within the list, default unoptimized $c_{damp}$, $C_{6,ij}$) and $R_{0,i}$ values are adopted for the damping functions of Elstner or Wu and Yang as described by Hill~\cite{ref:Hill09} and a default $s_6$ value of 1.00 is adopted for Grimme's D2 correction model.

\section*{Overriding dispersion correction parameters}
It is possible to override the default parameters of the dispersion damping functions.
This option allows the user to specify parameters for elements and functionals for which values are not given.
The atom-dependent variables $C_{6,i}$ (used to calculate $C_{6,ij}$), $R_{0,i}$ (related to the atomic vdW radius of an atom $i$), and $n_{eff}$ (used in the calculation of $C_{6,ij}$ for all damping functions excluding the D2 correction of Grimme) are modified using the \verb vdw_params  block.
This override block applies the parameter changes to atoms by their atomic number (nzatom).
eg. To override the dispersion parameters associated with nitrogen,  
\begin{verbatim}
%block vdw_params
! nzatom, c6coeff, radzero, neff
  7       21.1200  2.6200   2.5100
%endblock vdw_params
\end{verbatim}


To override the damping constant $c_{damp}$ associated with a damping function, the keyword \verb vdw_dcoeff  followed by the modified damping constant parameter is used.
eg. \begin{verbatim} vdw_dcoeff 11.0 \end{verbatim} 


\begin{thebibliography}{6}

\bibitem{ref:Hill09} Q. Hill and C-K Skylaris, \textit{Proc. R. Soc. A} 465(2103):669-683, 2009

\bibitem{ref:Grimme06} S. Grimme, \textit{J. Comput. Chem.} 27(15):1787-1799, 2006

\bibitem{ref:Elstner01} M. Elstner, P. Hobza, T. Frauenheim, S. Suhai and E. Kaxiras, \textit{J. Chem. Phys.} 114(12):5149-5155, 2001

\bibitem{ref:Wu02} Q. Wu and W. Yang, \textit{J. Chem. Phys.}, 116(2):515-524, 2002

\bibitem{ref:Jurecka06} P. Jure\v{c}ka, J. \v{S}poner, J. \v{C}ern\'{y}a and P. Hobza, \textit{Phys. Chem. Chem. Phys.} 8:1985-1993, 2006

\bibitem{ref:Morgado07} C. A. Morgado, J. P. McNamara, I. H. Hillier, N. A. Burton and M. A. Vincent \textit{J. Chem. Theory Comput.} 3(5):1656-1664, 2007



\end{thebibliography}

\end{document}
