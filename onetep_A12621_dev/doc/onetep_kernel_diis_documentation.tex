\documentclass[dvipdfm, a4paper, 12pt]{article}

% packages in use
\usepackage[english]{babel}
\usepackage{color}
\usepackage{graphicx}
\usepackage{times}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{tensor}
\usepackage{braket}
\usepackage{notoccite}
\usepackage{subfigure}
\usepackage[T1]{fontenc}
\usepackage{t1enc}
\usepackage{anyfontsize}
\usepackage[a4paper, portrait, dvips, textwidth=15cm,
textheight=23cm, headheight=1cm, bindingoffset=0cm, bottom=3cm]{geometry}
\usepackage[sort&compress]{natbib}
\usepackage[nottoc]{tocbibind}
\usepackage{hypernat}
\usepackage{hyperref}

\hypersetup{
    colorlinks,
    citecolor=blue,
    filecolor=black,
    linkcolor=blue,
    urlcolor=black,
    linktocpage=true
}

% new commands
\newcommand{\onetep}{\textsc{Onetep}}
\newcommand{\KS}{Kohn-Sham}
\newcommand{\braces}[1]{\left\lbrace #1 \right\rbrace}
\newcommand{\mm}[2]{\tensor{M}{^#1_#2}}
\newcommand{\mmt}[2]{\tensor*{M}{^\dagger_#2^#1}}
\newcommand{\mmn}[3]{\tensor{M}{^#1_#2^{(#3)}}}
\newcommand{\mmtn}[3]{\tensor*{M}{^\dagger_#2^#1^{(#3)}}}
\newcommand{\temperature}{\mathcal{T}}
\newcommand{\boltzmann}{k_\textrm{B}}
\newcommand{\lapack}{\textsc{Lapack}}
\newcommand{\scalapack}{\textsc{ScaLapack}}
\newcommand{\ie}{i.e.}

\newcommand{\character}[2]{[String, default \texttt{#1: #2}].}
\newcommand{\bool}[2]{[Boolean, default \texttt{#1: #2}].}
\newcommand{\intg}[2]{[Integer, default \texttt{#1: #2}].}
\newcommand{\real}[2]{[Real, default \texttt{#1: #2}].}
\newcommand{\rphy}[3]{[Real physical, default \texttt{#1: #2 #3}].}

%opening
\title{Density mixing (Kernel-DIIS) in {\onetep}}
\author{\'{A}lvaro Ruiz Serrano}
\date{\today}

\begin{document}

\maketitle

\tableofcontents % content

\section{Basic principles}

This manual describes how to run density mixing (kernel DIIS) calculations in
{\onetep}. Currently, kernel DIIS is an alternative to the LNV density kernel
optimisation in the {\onetep} inner loop (where the NGWFs are fixed). Please
note that:

\begin{itemize}
 \item The current code is experimental and might be unstable.
 \item Kernel DIIS currently can only perform calculations on insulators, where
the constraint of idempotency of the density matrix holds ({\ie}, the {\KS}
occupancies are integers, 1 for the valence states and 0 for the conductions
states.)
 \item Kernel DIIS is cubic-scaling, always, even if the density matrix is
truncated and localised.
 \item Use only in the rare occasions where LNV optimisation might not be
optimal.
\end{itemize}

Density mixing is a type of self-consistent cycle used to minimise the total
energy of the system. Currently, there are two types of mixing implemented in
{\onetep}: density kernel mixing or Hamiltonian mixing. In both cases,
Hamiltonian diagonalisation is required, which makes the method cubic-scaling.
The current implementation includes the linear mixing, ODA mixing
\cite{ODA, ODA_fractional}, Pulay mixing \cite{PulayDIIS}, LiSTi \cite{LISTi}
and LiSTb \cite{listb} mixing schemes. Pulay, LiSTi and LiSTb offer the best
performance of all, but they must be used in conjunction with linear mixing in
order to obtain numerical stability.

\section{Compilation}

By default, {\onetep} is linked against the {\lapack} library \cite{lapack_web}
for linear algebra. The {\lapack} eigensolver DSYGVX \cite{DSYGVX}, can only be
executed in one CPU at a time. Therefore, kernel DIIS calculations with
{\lapack} are limited to small systems (a few tens of atoms). Calculations on
large systems are possible if, instead, {\onetep} is linked against {\scalapack}
library \cite{scalapack_web} during compilation time. The {\scalapack}
eigensolver, PDSYGVX \cite{PDSYGVX}, can be run in parallel using many CPUs
simultaneously. Moreover, {\scalapack} can distribute the storage of dense
matrices across many CPUs, thus allowing to increase the total memory allocated
to a given calculation in a systematic manner, simply by requesting more
processors. For the compilation against {\scalapack} to take effect, the flag
\texttt{-DSCALAPACK} must be specified during the compilation of {\onetep}. 

\section{Commands for the inner loop}

\subsection{Mixing schemes}

The keyword\\

\noindent \texttt{kernel\_diis\_scheme: \textit{string}}\newline
\character{kernel\_diis\_scheme}{NONE}\newline

\noindent is used to select the mixing method during the kernel-DIIS loop. By
default, this keyword takes the value ``\texttt{NONE}'', which disables kernel
DIIS and tells the program to proceed with the LNV optimisation. The following
options are available:

\begin{itemize}

 \item \texttt{kernel\_diis\_scheme: DKN\_LINEAR}\\ linear mixing of density
kernels. The new input density kernel is built from the \textit{in} and
\textit{out} density kernels of the current iteration as
$K_in^{n+1} = (1-\lambda) K_{in}^{n} + \lambda K_{out}^{n}$. 

 \item \texttt{kernel\_diis\_scheme: HAM\_LINEAR}\\ linear mixing of
Hamiltonians. The new input Hamiltonian is built from the \text{in} and
\textit{out} Hamiltonians of the current iteration as
$H_in^{n+1} = (1-\lambda) H_{in}^{n} + \lambda H_{out}^{n}$.

 \item \texttt{kernel\_diis\_scheme: DKN\_PULAY}\\ Pulay mixing of density
kernels (see Ref. \cite{PulayDIIS}). The new input density kernel is built as a
linear combination of the \textit{output} density kernels of the $N_{mix}$
previous iterations as $K_in^{n+1} = \sum_{m=n-N_mix}^{n} \lambda_m
K_{out}^{m}$.
Pulay mixing requires the storage of $N_{mix}$ matrices.

 \item \texttt{kernel\_diis\_scheme: HAM\_PULAY}\\ Pulay mixing of Hamiltonians
(see Ref. \cite{PulayDIIS}). The new input Hamiltonian is built as a linear
combination of the \textit{output} Hamiltonians of the $N_{mix}$ previous
iterations as $H_in^{n+1} = \sum_{m=n-N_mix}^{n} \lambda_m H_{out}^{m}$.
Pulay mixing requires the storage of $N_{mix}$ matrices.

 \item \texttt{kernel\_diis\_scheme: DKN\_LISTI}\\ LiSTi mixing of density
kernels
(see Ref. \cite{LISTi}). The new input density kernel is built as a linear
combination of the \textit{output} density kernels of the $N_{mix}$ previous
iterations as $K_in^{n+1} = \sum_{m=n-N_mix}^{n} \lambda_m K_{out}^{m}$. LiSTi
mixing requires the storage of $4\times N_{mix}$ matrices.

 \item \texttt{kernel\_diis\_scheme: HAM\_LISTI}\\ LiSTi mixing of Hamiltonians
(see Ref. \cite{LISTi}). The new input Hamiltonian is built as a linear
combination of the \textit{output} Hamiltonians of the $N_{mix}$ previous
iterations as $H_in^{n+1} = \sum_{m=n-N_mix}^{n} \lambda_m H_{out}^{m}$.
LiSTi requires the storage of $4\times N_{mix}$ matrices.

 \item \texttt{kernel\_diis\_scheme: DKN\_LISTB}\\ LiSTb mixing of density
kernels (see Ref. \cite{listb}). The new input density kernel is built as a
linear
combination of the \textit{output} density kernels of the $N_{mix}$ previous
iterations as $K_in^{n+1} = \sum_{m=n-N_mix}^{n} \lambda_m K_{out}^{m}$. LiSTb
mixing requires the storage of $4\times N_{mix}$ matrices.

 \item \texttt{kernel\_diis\_scheme: HAM\_LISTB}\\ LiSTb mixing of Hamiltonians
(see Ref. \cite{listb}). The new input Hamiltonian is built as a linear
combination of the \textit{output} Hamiltonians of the $N_{mix}$ previous
iterations as $H_in^{n+1} = \sum_{m=n-N_mix}^{n} \lambda_m H_{out}^{m}$.
LiSTb mixing requires the storage of $4\times N_{mix}$ matrices.

 \item \texttt{kernel\_diis\_scheme: DIAG}\\ Hamiltonian diagonalisation only -
no mixing takes place. Strongly NOT recommended.

\end{itemize}

\textbf{\underline{** NOTE}}: linear mixing can be used simultaneously with
Pulay, LiSTi or LiSTb mixing to create a history of density kernels/Hamiltonians
with optimal numerical properties. See the keywords
\texttt{kernel\_diis\_coeff} and \texttt{kernel\_diis\_linear\_iter} in Sec.
\ref{sec:basic} for more information.

\subsection{Basic setup: controlling the mix}\label{sec:basic}

\begin{itemize}

\item \texttt{kernel\_diis\_coeff: x}\newline
\real{kernel\_diis\_coeff}{0.1}\newline
Linear-mixing $\lambda$ coefficient. Must be in the range $\left[0,1\right]$.
If set to a negative value, the ODA method (see Refs.
\cite{ODA,ODA_fractional}) will automatically calculate the optimal value of
$\lambda$. The value of \texttt{kernel\_diis\_coeff} can be made arbitrarily
small in order to attain numerical stability. However, this can make convergence
slow. A small value can be used in order to create a history of matrices before
Pulay, LiSTi or LiSTb mixing. A value close to 1 will make the calculation
potentially unstable.

\item \texttt{kernel\_diis\_linear\_iter: n}\newline
\intg{kernel\_diis\_linear\_iter}{5}\newline
Number of linear mixing iterations before Pulay, LiSTi or LiSTb mixing. This
keyword will create and store a history of \texttt{n} previous matrices
generated by linear mixing before Pulay, LiSTi or LiSTb mixing begin. Required
for large systems in order to achieve stability.

\item \texttt{kernel\_diis\_size: n}\newline
\intg{kernel\_diis\_size}{10}\newline
Number of matrices ($N_{mix}$) to be mixed with the Pulay, LiSTi or LiSTb
schemes.

\item \texttt{kernel\_diis\_maxit: n}\newline
\intg{kernel\_diis\_maxit}{25}\newline
Maximum number of iterations during the density mixing inner loop.

\end{itemize}


\subsection{Tolerance thresholds}

\begin{itemize}

\item \texttt{kernel\_diis\_threshold: x}.\newline
\real{kernel\_diis\_threshold}{1.0e-9}\newline
Numerical convergence threshold for the DIIS inner loop. Use in conjunction
with \texttt{kernel\_diis\_conv\_criteria}.

\item \texttt{kernel\_diis\_conv\_criteria: \textit{string}}.\newline
\character{kernel\_diis\_conv\_criteria}{1000}
Select the convergence criteria for the DIIS inner
loop.\texttt{kernel\_diis\_conv\_criteria} takes a string value 4 characters
long. Each position acts as a logical switch, and can only take the values
``\texttt{1}'' (on) or ``\texttt{0}'' (off). The order is the following:

\begin{itemize}
 \item Position 1: residual $|K_{out} - K_{in}|$, if density kernel mixing, or
$|H_{out}-H_{in}|$, if Hamiltonian mixing.
 \item Position 2: Hamiltonian-density kernel commutator.
 \item Position 3: band-gap variation between two consecutive iterations (in
Hartree).
 \item Position 4: total energy variation between two consecutive iterations
(in Hartree).
\end{itemize} 

For example, \texttt{kernel\_diis\_conv\_thres: 1101} will enable criteria 1,2
and 4 and disable criterion 3.


\end{itemize}

\subsection{Advanced setup: level shifter}

Extra stability can sometimes be achieved if the conduction energy values are
artificially increased. This technique is known as level-shifting. See Ref.
\cite{SaundersDIISlevelshifting} for further details.

\begin{itemize}
\item \texttt{kernel\_diis\_lshift: x \textit{units}}.\newline
\rphy{kernel\_diis\_lshift}{1.0}{Hartree}\newline
Energy shift of the conduction bands.

\item \texttt{kernel\_diis\_ls\_iter: n}.\newline
\intg{kernel\_diis\_ls\_iter}{0}\newline
Total number of DIIS iterations with level-shifting enabled.

\end{itemize}

\section{Commands for the outer loop}

The standard {\onetep} commands for NGWF optimisation apply.

\section{Restarting a kernel DIIS calculation}

\begin{itemize}

\item \texttt{write\_denskern: T/F}.\newline
\bool{write\_denskern}{F}\newline
Save the last density matrix on a file.

\item \texttt{read\_denskern: T/F}.\newline
\bool{read\_denskern}{F}\newline
Read the density kernel matrix from a file, and continue the calculation from
this point.

\item \texttt{write\_tightbox\_ngwfs: T/F}.\newline
\bool{write\_tightbox\_ngwfs}{T}\newline
Save the last NGWFs on a file.

\item \texttt{read\_tightbox\_ngwfs: T/F}.\newline
\bool{read\_tightbox\_ngwfs}{F}\newline
Read the NGWFs from a file and continue the calculation from this point.

\vspace{1\baselineskip}\vspace{-\parskip}

\noindent \underline{\textbf{NOTE:}} if a calculation is intended to be
restarted at some point in the future, then run the calculation with
\\
\texttt{write\_tightbox\_ngwfs: T}\\
\texttt{write\_denskern: T}\\
to save the density kernel and the NGWFs on disk. Two new files will be created,
with extensions \texttt{.dkn} and \texttt{.tightbox\_ngwfs},
respectively. Then, to restart the calculation, set\\
\texttt{read\_tightbox\_ngwfs: T}\\
\texttt{read\_denskern: T}\\
to tell {\onetep} to read the files that were previously saved on disk. Remember
to keep a backup of the output of the first run before restarting the
calculation.

\end{itemize}

\section{Controlling the parallel eigensolver}

Currently, only the {\scalapack} PDSYGVX parallel eigensolver is available. A
complete manual to this routine can be found by following the link in Ref.
\cite{PDSYGVX}. If {\onetep} is interfaced to {\scalapack}, the following
directives can be used:

\begin{itemize}

\item \texttt{eigensolver\_orfac: x}.\newline
\real{eigensolver\_orfac}{1.0e-4}\newline
Precision to which the eigensolver will orthogonalise degenerate Hamiltonian
eigenvectors. Set to a negative number to avoid reorthogonalisation with the
{\scalapack} eigensolver.

\item \texttt{eigensolver\_abstol: x}.\newline
\real{eigensolver\_abstol}{1.0e-9}\newline
Precision to which the parallel eigensolver will calculate the eigenvalues. Set
to a negative number to use {\scalapack} defaults.

\end{itemize}

The abovementioned directives are useful in calculations where the
{\scalapack} eigensolver fails to orthonormalise the eigenvectors. In such
cases, the following error will be printed in the input file:

\vspace{1\baselineskip}\vspace{-\parskip}

\noindent \texttt{(P)DSYGVX in subroutine dense\_eigensolve returned info=   
2}.

\vspace{1\baselineskip}\vspace{-\parskip}

\noindent Many times (although not always) this error might cause the
calculation to fail.

% bibliography
% \bibliographystyle{unsrt}
% \bibliography{references}

\begin{thebibliography}{10}

\bibitem{ODA}
E.~Canc{\`e}s and C.~{Le Bris}.
\newblock Can we outperform the diis approach for electronic structure
  calculations?
\newblock {\em Int. J. Quantum Chem.}, 79(2):82, 2000.

\bibitem{ODA_fractional}
E.~Cances.
\newblock {Self-consistent field algorithms for Kohn--Sham models with
  fractional occupation numbers}.
\newblock {\em J. Chem. Phys.}, 114(24):10616, 2001.

\bibitem{PulayDIIS}
P.~Pulay.
\newblock Convergence acceleration of iterative sequences - the case of {SCF}
  iteration.
\newblock {\em Chem. Phys. Lett.}, {73}({2}):393, {1980}.

\bibitem{LISTi}
Y.~A. Wang, C.~Y. Yam, Y.~K. Chen, and G.~Chen.
\newblock Linear-expansion shooting techniques for accelerating self-consistent
  field convergence.
\newblock {\em J. Chem. Phys.}, 134(24):241103, 2011.

\bibitem{listb}
Y.~K. Chen and Y.~A. Wang.
\newblock {LISTb: a better direct approach to LIST}.
\newblock {\em J. Chem. Theory Comput.}, 7(10):3045, 2011.

\bibitem{lapack_web}
Lapack.
\newblock http://www.netlib.org/lapack/.

\bibitem{DSYGVX}
{Lapack DSYGVX eigensolver}.
\newblock http://netlib.org/lapack/double/dsygvx.f.

\bibitem{scalapack_web}
{ScaLapack}.
\newblock http://www.netlib.org/scalapack/.

\bibitem{PDSYGVX}
{ScaLapack PDSYGVX eigensolver}.
\newblock http://www.netlib.org/scalapack/double/pdsygvx.f.

\bibitem{SaundersDIISlevelshifting}
V.~R. Saunders and I.~H. Hillier.
\newblock Level-shifting method for converging closed shell {Hartree-Fock}
  wave-functions.
\newblock {\em Int. J. Quantum Chem.}, {7}({4}):699, {1973}.

\end{thebibliography}



\end{document}
