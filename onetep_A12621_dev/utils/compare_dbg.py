# horribly hacky!!
# this script compares the output from the debug_greenf subroutine in greenf_mod
# assumes this script is in a directory structure ./pgi/
# and that there is a similar directory structure ./ifort/
# where both directories have the output from a onetep calculation, with the
# .dbg file outputs.
# Iteratively compares the contents of 

def different_floats(float_i, float_p):
  for fi, fp in zip(float_i, float_p):
    if not_close(fi,fp): return True
  return False

def not_close(f1,f2):
   REL_DIFF = 1.0e-3
   ABS_DIFF = 1.0e-3
   abs_diff = abs(f1-f2)
   abs_max = max(abs(f1),abs(f2),1.0e-16)
   if abs_diff/abs_max > REL_DIFF and abs_diff > ABS_DIFF:
      return True
   return False

class dbg_data(object):
  def __init__(self,file):
    with open(file) as f:
      data = [line.strip() for line in f.readlines()]
      self.len = len(data)
      self.data=iter(data)
  
  def getline(self):
    return next(self.data)

  def __len__(self):
    return self.len

class Comparer(object):
  def __init__(self,index):
    self.error = False
    self.warning = False
    self.ifort_data = {}
    self.pgi_data = {}
    self.records = []

    self.ifort = dbg_data('../ifort/gf_debug_file_%4.4i_mpi_0001.dbg'%index)
    self.pgi   = dbg_data('../pgi/gf_debug_file_%4.4i_mpi_0001.dbg'%index)
    
  def log_error(self,message, line_i, line_p):
    self.error = True
    print "="*40
    print message
    print "="*40
    print "ifort: %s"%line_i
    print "pgi:   %s"%line_p
    print "="*40

  def log_warning(self,message, float_i, float_p):
    if self.warning: return
    self.warning = True
    self.warning_message = message + \
        " ".join("(%8.1e/%8.1e)"%(abs(fi/fp), abs(fi-fp)) \
        for fi, fp in zip(float_i,float_p))
      
    
  def get_record_details(self,line_i, line_p):
    assert line_i == line_p, "Different line data: %s %s" % (line_i, line_p)
    split_i = line_i.split()
    name_i = split_i[3]
    type_i = split_i[1].rstrip('>')
    len_i = int(split_i[2])
    return name_i, type_i, len_i
  
  
   
  def parse_dbg(self):

    assert len(self.ifort) == len(self.pgi), ("Debug data output not same length")
    
    #for line_i, line_p in zip(self.ifort.data, self.pgi.data):
    while True:
      try:
        line_i, line_p = self.ifort.getline(), self.pgi.getline()
      except StopIteration:
        return
      
      # record tags should be identical
      if line_i != line_p:
        self.log_error("Records are different", line_i, line_p)
        return
      
      record_name, record_type, record_len = self.get_record_details(line_i,line_p)
      self.records.append(record_name)
      
      self.ifort_data[record_name] = [line_i]
      self.pgi_data[record_name] = [line_p]
      for ii in xrange(record_len):
        try:
          line_i, line_p = self.ifort.getline(), self.pgi.getline()
        except StopIteration:
          print "End of iterator: %s %s" % (line_i, line_p)
          raise
        
        if record_type == "EXACT":
          if line_i != line_p:
            self.log_error("Records are different", line_i, line_p)
            return
        
        elif record_type == "FLOAT":
          float_i = map(float,line_i.split())
          float_p = map(float,line_p.split())
          self.ifort_data[record_name].append(float_i)
          self.pgi_data[record_name].append(float_p)
          if different_floats(float_i, float_p):
            self.log_warning("%s: Different floats"%record_name, float_i, float_p)
            return
          
from itertools import count
for dbg in count(1):
   try:
     ret = Comparer(dbg)
   except IOError:
     break
   ret.parse_dbg()
   if ret.error or ret.warning:
      print
      print "*"*72
      print "*"*72
      print "Error after dbg=%i"%dbg
      print "Comments = %s" % ret.ifort_data["comment"][0]
      print "record stack:"
      for c in enumerate(ret.records):
         print "\t%i: %s"%tuple(c)
      print
      if ret.warning:
        print "Warning message:\n%s"%ret.warning_message
      print "Comments = %s" % ret.ifort_data["comment"][0]
      print "*"*72
      print "*"*72
      break
   
