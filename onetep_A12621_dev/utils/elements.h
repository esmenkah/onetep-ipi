


atom element[110];


/* cks initialise elements  */

element[1].symbol="H";
element[2].symbol="He";
element[3].symbol="Li";
element[4].symbol="Be";
element[5].symbol="B";
element[6].symbol="C";
element[7].symbol="N";
element[8].symbol="O";
element[9].symbol="F";
element[10].symbol="Ne";

element[11].symbol="Na";
element[12].symbol="Mg";
element[13].symbol="Al";
element[14].symbol="Si";
element[15].symbol="P";
element[16].symbol="S";
element[17].symbol="Cl";
element[18].symbol="Ar";
element[19].symbol="K";
element[20].symbol="Ca";


element[21].symbol="Sc";
element[22].symbol="Ti";
element[23].symbol="V";
element[24].symbol="Cr";
element[25].symbol="Mn";
element[26].symbol="Fe";
element[27].symbol="Co";
element[28].symbol="Ni";
element[29].symbol="Cu";
element[30].symbol="Zn";

element[31].symbol="Ga";
element[32].symbol="Ge";
element[33].symbol="As";
element[34].symbol="Se";
element[35].symbol="Br";
element[36].symbol="Kr";
element[37].symbol="Rb";
element[38].symbol="Sr";
element[39].symbol="Y";
element[40].symbol="Zr";
element[41].symbol="Nb";
element[42].symbol="Mo";
element[43].symbol="Tc";
element[44].symbol="Ru";
element[45].symbol="Rh";
element[46].symbol="Pd";
element[47].symbol="Ag";
element[48].symbol="Cd";
element[49].symbol="In";
element[50].symbol="Sn";
element[51].symbol="Sb";
element[52].symbol="Te";
element[53].symbol="I";
element[54].symbol="Xe";


element[55].symbol="Cs";
element[56].symbol="Ba";
element[57].symbol="La";
element[58].symbol="Ce";
element[59].symbol="Pr";
element[60].symbol="Nd";
element[61].symbol="Pm";
element[62].symbol="Sm";
element[63].symbol="Eu";
element[64].symbol="Gd";
element[65].symbol="Tb";
element[66].symbol="Dy";
element[67].symbol="Ho";
element[68].symbol="Er";
element[69].symbol="Tm";
element[70].symbol="Yb";
element[71].symbol="Lu";
element[72].symbol="Hf";
element[73].symbol="Ta";
element[74].symbol="W";
element[75].symbol="Re";
element[76].symbol="Os";
element[77].symbol="Ir";
element[78].symbol="Pt";
element[79].symbol="Au";
element[80].symbol="Hg";

element[81].symbol="Tl";
element[82].symbol="Pb";
element[83].symbol="Bi";
element[84].symbol="Po";
element[85].symbol="At";
element[86].symbol="Rn";
element[87].symbol="Fr";
element[88].symbol="Ra";
element[89].symbol="Ac";
element[90].symbol="Th";
element[91].symbol="Pa";
element[92].symbol="U";
element[93].symbol="Np";
element[94].symbol="Pu";



