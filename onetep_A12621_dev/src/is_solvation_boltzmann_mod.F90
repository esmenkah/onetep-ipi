!=============================================================================!
!                     S O L V A T I O N _ B O L T Z M A N N                   !
!=============================================================================!
! This module collects the Boltzmann (ion-concentration-dependent) facet of   !
! implicit solvent. It mostly encompasses variables, datatypes and subroutines!
! related to the Boltzmann ions and the steric potential.                     !
!-----------------------------------------------------------------------------!
! Written by Jacek Dziedzic, with help from Lucian Anton, 10-11/2014.         !
!-----------------------------------------------------------------------------!

module is_solvation_boltzmann

  use constants, only: DP
#ifdef HAVE_DL_MG
  use dl_mg !! External dependency
  use dl_mg_nonlinear_model  !! External dependency
#endif

  implicit none

  private

  !---------------------------------------------------------------------------!
  !                       P u b l i c   r o u t i n e s                       !
  !                              a n d   t y p e s                            !
  !---------------------------------------------------------------------------!

  ! The call sequence is as follows:
  !   1) rundat_blocks calls init_ions
  !   2) energy_and_force_calculate calls init
  !   3) multigrid_methods calls compute many times
  !   4) energy_and_force_calculate calls exit
  !   2..4 are called multiple times if ions move.
  !   5) energy_and_force_exit_cell calls exit_ions

  public :: implicit_solvent_boltzmann_init_ions
  public :: implicit_solvent_boltzmann_init
  public :: implicit_solvent_boltzmann_defect
  public :: implicit_solvent_boltzmann_osmotic_pressure
  public :: implicit_solvent_boltzmann_mobile_energy
  public :: implicit_solvent_boltzmann_accessibility_correction
  public :: implicit_solvent_boltzmann_ion_atmosphere_entropy
  public :: implicit_solvent_boltzmann_exit
  public :: implicit_solvent_boltzmann_exit_ions

  !---------------------------------------------------------------------------!
  !                      P u b l i c   v a r i a b l e s                      !
  !---------------------------------------------------------------------------!
  ! jd: The maximum number of (kinds of) mean-field ions in implicit solvent
  !     Needed in rundat_blocks.
  integer, parameter, public   :: implicit_solvent_boltzmann_max_n_ions = 1000

  ! jd: The calculated Debye length.
  !     Needed in multigrid_methods.
  real(kind=DP), save, public  :: implicit_solvent_boltzmann_debye_length

  ! ---------------------------------------------------------------------------
  ! -------------------------- P R I V A T E ----------------------------------
  ! ---------------------------------------------------------------------------

  ! ******************** Boltzmann ions ********************

  ! jd: Describes a mean-field ion in implicit solvent
  type BOLTZMANN_ION
     character(len=10) :: name
     real(kind=DP)     :: charge          ! jd: in |e|
     real(kind=DP)     :: concentration   ! jd: atomic units
  end type BOLTZMANN_ION

  ! jd: All Boltzmann ion kinds in solvent. A copy is held on all nodes.
  type(BOLTZMANN_ION), allocatable, save    :: boltzmann_ions(:)

  ! jd: Number of Boltzmann ions
  integer, save   :: n_boltzmann_ions
  logical, save   :: boltzmann_ions_reported = .false.

  ! ******************** Steric potential and weight ********************
  ! jd: The arrays are not private to allow verbose dumps in multigrid_methods.

  ! jd: Steric potential (slab-distributed on MG grid)
  real(kind=DP), allocatable, save, protected, public   :: steric_pot(:,:,:)

  ! jd: Steric weight (slab-distributed on MG grid)
  real(kind=DP), allocatable, save, protected, public   :: steric_weight(:,:,:)

  character, save :: steric_pot_type ! 'H': hard-core, 'S': soft-core, 'X': none

#ifndef HAVE_DL_MG
  ! jd: Dummy value, only so the code compiles with no DL_MG
  real(kind=DP)   :: max_expcap = -1.0_DP
#endif


contains

  !---------------------------------------------------------------------------!
  !                      P u b l i c   s u b r o u t i n e s                  !
  !---------------------------------------------------------------------------!

  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  subroutine implicit_solvent_boltzmann_init_ions(cbuf, dbuf, &
       runbl_n_sol_ions)
    !=========================================================================!
    ! Initializes the sol_ions structure based on raw data passed from        !
    ! rundat_blocks. The structure is allocated here, to be deallocated in    !
    ! implicit_solvent_exit.
    ! ----------------------------------------------------------------------- !
    ! Arguments:                                                              !
    !   cbuf, dbuf (in): See description in rundat_blocks.                    !
    !   runbl_n_sol_ions (in): The number of solvent ions from rundat_blocks. !
    ! ----------------------------------------------------------------------- !
    ! Written by Jacek Dziedzic in 07/2014.                                   !
    !=========================================================================!

    use constants, only: CONC_MOL_PER_L_TO_AU
    use rundat, only: pub_is_pbe
    use utils, only: utils_alloc_check, utils_assert

    implicit none

    ! jd: Arguments
    character(len=10), intent(in) :: cbuf(implicit_solvent_boltzmann_max_n_ions)
    real(kind=DP), intent(in)   :: dbuf(implicit_solvent_boltzmann_max_n_ions*2)
    integer                     :: runbl_n_sol_ions

    ! jd: Local variables
    integer :: ierr
    integer :: i
    character(len=*), parameter   :: myself = &
         'implicit_solvent_boltzmann_init_ions'

    ! -------------------------------------------------------------------------

    call utils_assert(pub_is_pbe /= 'NONE', trim(myself)//&
         ' must only be called when pub_is_pbe is not ''none''.')

    n_boltzmann_ions = runbl_n_sol_ions

    allocate(boltzmann_ions(n_boltzmann_ions), stat=ierr)
    call utils_alloc_check(myself,'boltzmann_ions',ierr)

    do i=1, n_boltzmann_ions
       boltzmann_ions(i)%name = cbuf(i)(1:10)
       boltzmann_ions(i)%charge = dbuf(i*2-1)
       boltzmann_ions(i)%concentration = CONC_MOL_PER_L_TO_AU * dbuf(i*2)
    end do

  end subroutine implicit_solvent_boltzmann_init_ions
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  subroutine implicit_solvent_boltzmann_init(elements, grid, cell)
    !=========================================================================!
    ! Initialises Boltzmann solvation in ONETEP.                              !
    ! - Reports on Boltzmann ions (these are initialised earlier from         !
    !   rundat_blocks -> sol_ions_report), but it's neater to display the     !
    !   report only now.                                                      !
    ! - Calculates steric potential and steric weight, allocating them, if    !
    !   necessary.                                                            !
    ! - Calls the appropriate dl_mg nonlinear initialization routine, to pass !
    !   relevant data on Boltzmann ions, Boltzmann beta and steric potential. !
    ! ----------------------------------------------------------------------- !
    ! Notes:                                                                  !
    !   This can (and needs to) be called each time ions move, as the steric  !
    !   potential must be updated and dl_mg made aware of that.               !
    ! ----------------------------------------------------------------------- !
    ! Arguments:                                                              !
    !   elements (input): The usual elements array.                           !
    !   grid (input): Pass the fine grid.                                     !
    ! ----------------------------------------------------------------------- !
    ! Written by Jacek Dziedzic in 07/2014.                                   !
    ! Updated by Jacek Dziedzic in 11/2014.                                   !
    !=========================================================================!

    use cell_grid, only: GRID_INFO
    use comms, only: pub_on_root
    use constants, only : DP, PI, stdout, VERBOSE, CRLF
    use ion, only: ELEMENT
    use parallel_strategy, only: par=>pub_par
    use rundat, only: pub_is_pbe, pub_is_pbe_use_fas, pub_is_pbe_exp_cap, &
         pub_output_detail, pub_is_pbe_temperature, pub_is_bulk_permittivity
    use simulation_cell, only: CELL_INFO
    use utils, only: utils_abort, utils_feature_not_supported

    implicit none

    ! jd: Arguments
    type(ELEMENT), intent(in)      :: elements(par%nat)
    type(GRID_INFO), intent(in)    :: grid
    type(CELL_INFO), intent(in)    :: cell

    ! jd: Local variables
    real(kind=DP) :: exp_cap
    logical       :: want_linearised
    character(len=*), parameter :: myself = 'implicit_solvent_boltzmann_init'

    !------------------------------------------------------------------------

#ifdef HAVE_DL_MG

    if(pub_is_pbe /= 'NONE') then

       if(pub_on_root .and. pub_output_detail == VERBOSE) then
          write(stdout,'(a)') &
               'Initialising Boltzmann terms in implicit solvation...'
       end if

       ! jd: Report on Boltzmann ions
       call boltzmann_ions_report

       ! jd: Initialise steric potential
       call init_steric_pot(elements, grid, cell)

       if(pub_is_pbe == 'LINEARISED') then
          want_linearised = .true.
       else
          want_linearised = .false.
       end if

       if(pub_is_pbe_exp_cap == 0D0) then
          exp_cap = max_expcap
       else
          exp_cap = pub_is_pbe_exp_cap
       end if

       call dl_mg_init_nonlin(-4.0_DP*PI, pub_is_pbe_temperature, &       ! in
            n_boltzmann_ions, &                                           ! in
            boltzmann_ions(1:n_boltzmann_ions)%concentration, &           ! in
            boltzmann_ions(1:n_boltzmann_ions)%charge, &                  ! in
            implicit_solvent_boltzmann_debye_length, &                    ! out
            steric_weight, &                                              ! in
            want_linearised, pub_is_pbe_use_fas, exp_cap)                 ! in

       ! jd: DL_MG does not know about bulk permittivity. We assume here that
       !     the *entire* system is filled with bulk solvent.
       implicit_solvent_boltzmann_debye_length = &
            implicit_solvent_boltzmann_debye_length * &
            sqrt(pub_is_bulk_permittivity)

       if(pub_on_root .and. pub_output_detail == VERBOSE) then
          write(stdout,'(a,f7.2,a,a,a,l,a,f7.2,a,a,a)') 'Debye length is ', &
               implicit_solvent_boltzmann_debye_length, &
               ' bohr. PB equation variant: ', trim(pub_is_pbe), &
               '. Use_fas: ', pub_is_pbe_use_fas, '.'//CRLF//'Exp_cap: ', &
               exp_cap, ' Steric potential: ', steric_pot_type,'.'
       end if

    else
       call utils_abort(trim(myself)//' must only be called &
            &when pub_is_pbe is not ''none''.')
    end if
#else
    call utils_feature_not_supported('DL_MG multigrid solver')
#endif

  end subroutine implicit_solvent_boltzmann_init
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  subroutine implicit_solvent_boltzmann_exit
    !=========================================================================!
    ! Cleans up after Boltzmann implicit solvent.                             !
    !=========================================================================!

    use rundat, only: pub_is_pbe
    use utils, only: utils_dealloc_check, utils_assert

    implicit none

    integer                     :: ierr
    character(len=*), parameter :: myself = 'implicit_solvent_boltzmann_exit'

    ! ------------------------------------------------------------------------

    call utils_assert(pub_is_pbe /= 'NONE',trim(myself)//': Unexpected call')

    call utils_assert(allocated(steric_weight), &
         'Internal error in '//trim(myself)//': steric_weight not allocated')
    call utils_assert(allocated(steric_pot), &
         'Internal error in '//trim(myself)//': steric_pot not allocated')
    deallocate(steric_weight,stat=ierr)
    call utils_dealloc_check(myself,'steric_weight',ierr)
    deallocate(steric_pot,stat=ierr)
    call utils_dealloc_check(myself,'steric_pot',ierr)

  end subroutine implicit_solvent_boltzmann_exit
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  subroutine implicit_solvent_boltzmann_exit_ions
    !=========================================================================!
    ! Cleans up after Boltzmann implicit solvent (global state).              !
    !=========================================================================!

    use rundat, only: pub_is_pbe
    use utils, only: utils_dealloc_check, utils_assert

    implicit none

    integer                     :: ierr
    character(len=*), parameter :: myself = &
         'implicit_solvent_boltzmann_exit_ions'

    ! ------------------------------------------------------------------------

    call utils_assert(pub_is_pbe /= 'NONE',trim(myself)//': Unexpected call')

    call utils_assert(allocated(boltzmann_ions), &
         'Internal error in '//trim(myself)//': sol_ions not allocated')
    deallocate(boltzmann_ions, stat=ierr)
    call utils_dealloc_check(myself,'boltzmann_ions',ierr)

  end subroutine implicit_solvent_boltzmann_exit_ions
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  subroutine implicit_solvent_boltzmann_defect(bt, pot, n1, n2, n3)
    !=========================================================================!
    ! Computes the Boltzmann term to defect correction.                       !
    !=========================================================================!

    use constants, only : DP, PI, k_B
    use rundat, only: pub_is_pbe, pub_is_pbe_temperature, pub_is_pbe_exp_cap
    use utils, only: utils_abort

    implicit none

    ! jd: Arguments
    real(kind=DP), intent(out) :: bt(:,:,:)
    real(kind=DP), intent(in)  :: pot(:,:,:)
    integer, intent(in)        :: n1, n2, n3

    ! jd: Internal variables
    integer                    :: i
    real(kind=DP)              :: q, beta, kappa2
    real(kind=DP), parameter   :: FOURPI = 4.0_DP * PI
    real(kind=DP)              :: exp_cap

    ! -----------------------------------------------------------------------

    beta = 1.0_DP/(k_B*pub_is_pbe_temperature)

    if(pub_is_pbe_exp_cap == 0D0) then
       exp_cap = max_expcap
    else
       exp_cap = pub_is_pbe_exp_cap
    end if

    bt(:, :, :) = 0.0_DP
    if (pub_is_pbe == "FULL") then
       do i = 1, size(boltzmann_ions)
          q = boltzmann_ions(i)%charge
          bt(1:n1, 1:n2, 1:n3) = bt(1:n1, 1:n2, 1:n3) + &
               FOURPI*boltzmann_ions(i)%concentration * q * &
               steric_weight(1:n1, 1:n2, 1:n3) * &
               exp(min(exp_cap, -q * beta * pot(1:n1, 1:n2, 1:n3)))
       enddo
    elseif (pub_is_pbe == "LINEARISED") then
       kappa2 = FOURPI/implicit_solvent_boltzmann_debye_length**2
       bt(1:n1, 1:n2, 1:n3) = - kappa2 * &
            steric_weight(1:n1, 1:n2, 1:n3) * pot(1:n1, 1:n2, 1:n3)
    else
       call utils_abort("Internal error in implicit_solvent_boltzmann_defect")
    end if

  end subroutine implicit_solvent_boltzmann_defect
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  real(kind=DP) function implicit_solvent_boltzmann_osmotic_pressure(pot, &
       mg_grid)
    !=========================================================================!
    !=========================================================================!

    use comms, only: comms_reduce
    use constants, only: k_B
    use finite_differences, only: FD_GRID_INFO
    use rundat, only: pub_is_pbe_temperature, pub_is_pbe_exp_cap

    implicit none

    ! jd: Arguments
    real(kind=DP), intent(in    )  :: pot(:,:,:)
    type(FD_GRID_INFO), intent(in) :: mg_grid

    ! jd: Local variables
    integer       :: i1, i2, islab12
    integer       :: b_ion
    real(kind=DP) :: beta
    real(kind=DP) :: pot_here, steric_weight_here
    real(kind=DP) :: exp_term
    real(kind=DP) :: z_i
    real(kind=DP) :: c_i, c_i_bulk
    real(kind=DP) :: accum
    real(kind=DP) :: exp_cap

    ! -------------------------------------------------------------------------

    beta = 1.0_DP/(k_B*pub_is_pbe_temperature)

    if(pub_is_pbe_exp_cap == 0D0) then
       exp_cap = max_expcap
    else
       exp_cap = pub_is_pbe_exp_cap
    end if

    accum = 0D0
    do islab12 = 1, mg_grid%num_slabs12_pq
       do i2 = 1, mg_grid%pq2f
          do i1 = 1, mg_grid%pq1f
             pot_here = pot(i1,i2,islab12)
             steric_weight_here = steric_weight(i1,i2,islab12)
             do b_ion = 1, n_boltzmann_ions
                z_i = boltzmann_ions(b_ion)%charge
                c_i_bulk = boltzmann_ions(b_ion)%concentration
                exp_term = exp(min(exp_cap,-z_i * pot_here * beta))
                c_i = c_i_bulk * exp_term * steric_weight_here
                accum = accum + (c_i_bulk - c_i)
             end do
          end do
       end do
    end do

    call comms_reduce('SUM',accum)

    ! jd: Finally, include the kT in front and the grid weight
    implicit_solvent_boltzmann_osmotic_pressure = accum / beta * mg_grid%weight

  end function implicit_solvent_boltzmann_osmotic_pressure

  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  real(kind=DP) function implicit_solvent_boltzmann_accessibility_correction(&
       pot, mg_grid)
    !=========================================================================!
    !=========================================================================!

    use comms, only: comms_reduce
    use constants, only: k_B
    use finite_differences, only: FD_GRID_INFO
    use rundat, only: pub_is_pbe_temperature, pub_is_pbe_exp_cap

    implicit none

    ! jd: Arguments
    real(kind=DP), intent(in    )  :: pot(:,:,:)
    type(FD_GRID_INFO), intent(in) :: mg_grid

    ! jd: Local variables
    integer       :: i1, i2, islab12
    integer       :: b_ion
    real(kind=DP) :: beta
    real(kind=DP) :: pot_here, steric_weight_here, steric_pot_here
    real(kind=DP) :: exp_term
    real(kind=DP) :: z_i
    real(kind=DP) :: c_i, c_i_bulk
    real(kind=DP) :: accum
    real(kind=DP) :: exp_cap

    ! -------------------------------------------------------------------------

    beta = 1.0_DP/(k_B*pub_is_pbe_temperature)

    if(pub_is_pbe_exp_cap == 0D0) then
       exp_cap = max_expcap
    else
       exp_cap = pub_is_pbe_exp_cap
    end if

    accum = 0D0
    do islab12 = 1, mg_grid%num_slabs12_pq
       do i2 = 1, mg_grid%pq2f
          do i1 = 1, mg_grid%pq1f
             pot_here = pot(i1,i2,islab12)
             steric_weight_here = steric_weight(i1,i2,islab12)
             steric_pot_here = steric_pot(i1,i2,islab12)
             do b_ion = 1, n_boltzmann_ions
                z_i = boltzmann_ions(b_ion)%charge
                c_i_bulk = boltzmann_ions(b_ion)%concentration
                exp_term = exp(min(exp_cap,-z_i * pot_here * beta))
                c_i = c_i_bulk * exp_term * steric_weight_here
                accum = accum + c_i * steric_pot_here
                ! jd: @optimizeme -- can sum c_i and multiply sum by steric
             end do
          end do
       end do
    end do

    call comms_reduce('SUM',accum)

    ! jd: Mind the minus in front of the integral
    implicit_solvent_boltzmann_accessibility_correction = -accum*mg_grid%weight

  end function implicit_solvent_boltzmann_accessibility_correction
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  real(kind=DP) function implicit_solvent_boltzmann_mobile_energy(pot, &
       mg_grid)
    !=========================================================================!
    !=========================================================================!

    use comms, only: comms_reduce
    use constants, only: k_B
    use finite_differences, only: FD_GRID_INFO
    use rundat, only: pub_is_pbe_temperature, pub_is_pbe_exp_cap

    implicit none

    ! jd: Arguments
    real(kind=DP), intent(in    )  :: pot(:,:,:)
    type(FD_GRID_INFO), intent(in) :: mg_grid

    ! jd: Local variables
    integer       :: i1, i2, islab12
    integer       :: b_ion
    real(kind=DP) :: beta
    real(kind=DP) :: pot_here, steric_weight_here
    real(kind=DP) :: exp_term
    real(kind=DP) :: z_i
    real(kind=DP) :: c_i, c_i_bulk
    real(kind=DP) :: accum
    real(kind=DP) :: exp_cap

    ! -------------------------------------------------------------------------

    beta = 1.0_DP/(k_B*pub_is_pbe_temperature)

    if(pub_is_pbe_exp_cap == 0D0) then
       exp_cap = max_expcap
    else
       exp_cap = pub_is_pbe_exp_cap
    end if

    accum = 0D0
    do islab12 = 1, mg_grid%num_slabs12_pq
       do i2 = 1, mg_grid%pq2f
          do i1 = 1, mg_grid%pq1f
             pot_here = pot(i1,i2,islab12)
             steric_weight_here = steric_weight(i1,i2,islab12)
             do b_ion = 1, n_boltzmann_ions
                z_i = boltzmann_ions(b_ion)%charge
                c_i_bulk = boltzmann_ions(b_ion)%concentration
                exp_term = exp(min(exp_cap,-z_i * pot_here * beta))
                c_i = c_i_bulk * exp_term * steric_weight_here
                accum = accum + c_i * z_i * pot_here
             end do
          end do
       end do
    end do

    call comms_reduce('SUM',accum)

    ! jd: 1/2 is in front of the integral, and then there's the grid weight
    implicit_solvent_boltzmann_mobile_energy= 0.5_DP * accum * mg_grid%weight

  end function implicit_solvent_boltzmann_mobile_energy
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  real(kind=DP) function implicit_solvent_boltzmann_ion_atmosphere_entropy(pot,&
       mg_grid)
    !=========================================================================!
    !=========================================================================!

    use comms, only: comms_reduce
    use constants, only: k_B
    use finite_differences, only: FD_GRID_INFO
    use rundat, only: pub_is_pbe_temperature, pub_is_pbe_exp_cap

    implicit none

    ! jd: Arguments
    real(kind=DP), intent(in    )  :: pot(:,:,:)
    type(FD_GRID_INFO), intent(in) :: mg_grid

    ! jd: Local variables
    integer       :: i1, i2, islab12
    integer       :: b_ion
    real(kind=DP) :: beta
    real(kind=DP) :: pot_here, steric_weight_here
    real(kind=DP) :: exp_term
    real(kind=DP) :: z_i
    real(kind=DP) :: c_i, c_i_bulk
    real(kind=DP) :: accum
    real(kind=DP) :: exp_cap

    ! -------------------------------------------------------------------------

    beta = 1.0_DP/(k_B*pub_is_pbe_temperature)

    if(pub_is_pbe_exp_cap == 0D0) then
       exp_cap = max_expcap
    else
       exp_cap = pub_is_pbe_exp_cap
    end if

    accum = 0D0
    do islab12 = 1, mg_grid%num_slabs12_pq
       do i2 = 1, mg_grid%pq2f
          do i1 = 1, mg_grid%pq1f
             pot_here = pot(i1,i2,islab12)
             steric_weight_here = steric_weight(i1,i2,islab12)
             do b_ion = 1, n_boltzmann_ions
                z_i = boltzmann_ions(b_ion)%charge
                c_i_bulk = boltzmann_ions(b_ion)%concentration
                exp_term = exp(min(exp_cap,-z_i * pot_here * beta))
                c_i = c_i_bulk * exp_term * steric_weight_here
                if(c_i > 0.0) then
                   accum = accum + c_i * log(c_i/c_i_bulk)
                end if
             end do
          end do
       end do
    end do

    call comms_reduce('SUM',accum)

    ! jd: kT is in front of the integral, and then there's the grid weight
    implicit_solvent_boltzmann_ion_atmosphere_entropy = &
         accum / beta * mg_grid%weight

  end function implicit_solvent_boltzmann_ion_atmosphere_entropy
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  !---------------------------------------------------------------------------!
  !                     P r i v a t e   s u b r o u t i n e s                 !
  !---------------------------------------------------------------------------!

  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  subroutine init_steric_pot(elements, grid, cell)
    !=========================================================================!
    ! Generates the steric potential, stores it into the module-wide variable !
    ! 'steric_pot', then calculates the steric_weight, e^(-beta*steric_pot),  !
    ! also stored in a module-wide wariable.                                  !
    !                                                                         !
    ! Currently the steric potential can be:                                  !
    ! - soft-core: the repulsive part of the Lennard-Jones potential, i.e.    !
    !   A/r^12 centred around each ion, truncated and shifted to zero at the  !
    !   truncation radius.                                                    !
    ! - hard-core: \inf within a radius around each atom, 0 elsewhere.        !
    !              In practice 1 is stored in steric_pot, not \inf, to avoid  !
    !              IEEE734 traps.                                             !
    !                                                                         !
    ! The steric potential and weight are generated on the mg grid.           !
    ! ----------------------------------------------------------------------- !
    ! Arguments:                                                              !
    !   elements (in): Needed to get the centres of the ions.                 !
    !   grid (input):  Pass the fine grid.                                    !
    ! ----------------------------------------------------------------------- !
    ! Caveats:                                                                !
    !  - 'steric_pot' and 'steric_weight' are allocated here. Deallocation    !
    !    happens in implicit_solvent_exit_nonlin.                             !
    !  - a copy of the mg grid is constructed from 'grid', as the original is !
    !    unavailable (cannot use multigrid_methods here). Cannot pass 'mg'    !
    !    either, because 'grid' is needed in the visual_scalar call.          !
    ! ----------------------------------------------------------------------- !
    ! Written by Jacek Dziedzic, 07/2014.                                     !
    !=========================================================================!

    use cell_grid, only: GRID_INFO
    use comms, only: pub_on_root
!$  use comms, only: pub_my_node_id
    use constants, only: stdout, VERBOSE, PI, k_B
    use finite_differences, only: FD_GRID_INFO, finite_difference_set_geometry
    use geometry, only: POINT, magnitude, OPERATOR(-), OPERATOR(+), OPERATOR(*)
    use ion, only: ELEMENT
    use parallel_strategy, only: par=>pub_par
    use rundat, only: pub_is_pbe, pub_is_sc_steric_magnitude,&
         pub_is_steric_write, pub_is_sc_steric_cutoff, pub_is_hc_steric_cutoff,&
         pub_is_sc_steric_smoothing_alpha, pub_output_detail, &
         pub_is_multigrid_nlevels, pub_is_pbe_temperature
!$  use rundat, only: pub_threads_max
    use simulation_cell, only: CELL_INFO
    use timer, only: timer_clock
    use utils, only: utils_alloc_check, utils_abort, utils_assert, utils_erf, &
         utils_trace_in, utils_trace_out
    use visual, only: visual_scalarfield

    implicit none

    ! jd: Arguments
    type(ELEMENT), intent(in)        :: elements(par%nat)
    type(GRID_INFO), intent(in)      :: grid
    type(CELL_INFO), intent(in)      :: cell

    ! jd: Local variables
    type(FD_GRID_INFO) :: mg_grid
    integer       :: islab12, i1, i2, i3, ipt, I
    integer       :: ierr
    integer       :: multigrid_granularity
    type(POINT)   :: r, R_I
    real(kind=DP) :: boltzmann_beta
    real(kind=DP) :: d, accum
    real(kind=DP) :: shift ! jd: Shift of the potential to get zero at cutoff
    real(kind=DP) :: val_at_zero ! jd: Limit of the potential at d->0
    character(len=*), parameter :: myself = 'implicit_solvent_init_steric_pot'

    !------------------------------------------------------------------------

    call utils_trace_in(myself)
    call timer_clock(myself,1)

    if(pub_is_pbe == 'NONE') then
       call utils_abort(trim(myself)//' called with ''is_pbe none''.')
    end if

    ! jd: Initialise a copy of the MG grid. Ugly, I know.
    multigrid_granularity = 2**(pub_is_multigrid_nlevels+1)
    mg_grid = finite_difference_set_geometry(grid,multigrid_granularity)

    ! jd: Check if neither SC nor HC steric potential in effect
    if((pub_is_sc_steric_magnitude <= 0D0 .or. pub_is_sc_steric_cutoff <= 0D0) &
         .and. pub_is_hc_steric_cutoff <= 0D0) then
       steric_pot_type = 'X'
    else
       if(pub_is_hc_steric_cutoff > 0D0) then
          call utils_assert(pub_is_sc_steric_magnitude <= 0D0 .or. &
                pub_is_sc_steric_cutoff <= 0D0, &
                trim(myself)//': Cannot have both soft-core and hard-core &
                &steric potentials in effect.')
          steric_pot_type = 'H'
       else
          steric_pot_type = 'S'
       end if
    end if

    select case(steric_pot_type)
    case('X')
       if(pub_on_root) write(stdout,'(/a)') &
            'No steric potential will be applied!'
    case('H')
       if(pub_on_root) write(stdout,'(/a)',advance='no') &
            'Generating hard-core steric potential ...'
    case('S')
       if(pub_on_root) write(stdout,'(/a)',advance='no') &
            'Generating soft-core steric potential ...'
    case default
       call utils_abort(trim(myself)//': Unrecognized steric potential')
    end select

    allocate(steric_pot(mg_grid%ld1f,mg_grid%ld2f,mg_grid%max_slabs12), &
         stat=ierr)
    call utils_alloc_check(myself,'steric_pot',ierr)
    allocate(steric_weight(mg_grid%ld1f,mg_grid%ld2f,mg_grid%max_slabs12), &
         stat=ierr)
    call utils_alloc_check(myself,'steric_weight',ierr)

    ! jd: Take care of padding between pt and ld
    steric_pot = 0.0_DP
    steric_weight = 1.0_DP

    ! jd: If no steric potential desired, we just exit here with 0 everywhere,
    !     but mind to output it, if desired, and stop the timer
    if(steric_pot_type == 'X') goto 999

    if(steric_pot_type == 'S') then
       shift = pub_is_sc_steric_cutoff**(-12.0_DP) * &
            utils_erf(pub_is_sc_steric_smoothing_alpha * &
            pub_is_sc_steric_cutoff)**12.0_DP
       val_at_zero = 4096.0_DP * pub_is_sc_steric_magnitude * &
            pub_is_sc_steric_smoothing_alpha**12.0_DP / PI**6.0_DP

       if(pub_on_root .and. pub_output_detail == VERBOSE) then
          write(stdout,'(/a,e12.5,a)') &
               '- maximum value (at core centres):        ', val_at_zero,' Ha.'
          write(stdout,'(a,e12.5,a)') &
               '- constant shift (to get zero at cutoff): ', shift,' Ha.'
       end if
    end if

    if(steric_pot_type == 'H') then
       if(pub_on_root .and. pub_output_detail == VERBOSE) then
          write(stdout,'(/a,e12.5,a)') &
               '- radius:        ', pub_is_hc_steric_cutoff,' bohr.'
       end if
    end if

    ! jd: Generate steric potential on every point of fine grid
    !     First accumulate 1/r^12 * erf(alpha*r)^12
!$OMP PARALLEL DO NUM_THREADS(pub_threads_max) DEFAULT(NONE) &
!$OMP PRIVATE(ipt,i1,i2,islab12,i3,r,accum,I,R_I,d) &
!$OMP SHARED(par,elements,mg_grid,pub_is_sc_steric_cutoff, &
!$OMP      pub_is_hc_steric_cutoff, pub_is_sc_steric_magnitude, &
!$OMP      steric_pot,pub_my_node_id, val_at_zero, steric_pot_type, &
!$OMP      pub_is_sc_steric_smoothing_alpha, shift, pub_threads_max)
    do ipt=1,mg_grid%num_slabs12_pq*mg_grid%pq2f*mg_grid%pq1f
       i1 = modulo(ipt-1,mg_grid%pq1f) + 1
       i2 = modulo((ipt-i1)/mg_grid%pq1f,mg_grid%pq2f) + 1
       islab12 = (ipt-(i2-1)*mg_grid%pq1f-i1) / (mg_grid%pq1f*mg_grid%pq2f) + 1
       i3 = mg_grid%my_first_slab12 + islab12 - 1

       ! jd: @generalizeme when periodic or mixed BCs will be implemented
       r%X = real((i1-1),kind=DP) * mg_grid%d1f
       r%Y = real((i2-1),kind=DP) * mg_grid%d2f
       r%Z = real((i3-1),kind=DP) * mg_grid%d3f

       accum = 0.0_DP
       select case(steric_pot_type)
       case('S')
          ! jd: Soft-core potential
          do I = 1, par%nat

             R_I = elements(I)%centre
             d = magnitude(r-R_I)

             ! jd: Only calculate steric potential within a cutoff around each
             !     atom, to retain O(N) scaling
             if(d <= pub_is_sc_steric_cutoff) then
                if(d < 1D-10) then ! jd: Very close to gridpoint, avoid singularity
                   accum = val_at_zero / pub_is_sc_steric_magnitude
                else ! jd: Usual case
                   accum = accum + d**(-12.0_DP) * &
                        utils_erf(pub_is_sc_steric_smoothing_alpha*d)**12.0_DP
                end if
             end if

          end do ! over I

          ! jd: Multiply by prefactor (A), apply constant shift (from each atom)
          !     Values that are negative (rounding errors in erf) are zeroed.
          steric_pot(i1,i2,islab12) = &
               max(pub_is_sc_steric_magnitude * accum - par%nat * shift,0D0)

        case('H')
          ! jd: Hard-core potential
          do I = 1, par%nat

             R_I = elements(I)%centre
             d = magnitude(r-R_I)

             if(d <= pub_is_hc_steric_cutoff) then
                accum = 1D0
                exit ! jd: Enough if within one cutoff
             end if

          end do ! over I

          steric_pot(i1,i2,islab12) = accum

        end select

    end do ! ipt
!$OMP END PARALLEL DO

    ! jd: Transform steric potential to steric weight, favoured by DL_MG.
    boltzmann_beta = 1.0_DP / (k_B*pub_is_pbe_temperature)
    select case(steric_pot_type)
    case('S')
       steric_weight = exp(-boltzmann_beta*steric_pot)
    case('H')
       ! jd: Already generated as zeros and ones.
       !     Translate pot=0 -> weight of 1
       !               pot=1 -> weight of 0
       steric_weight = 1.0_DP - steric_pot
    case('X')
       ! jd: Already one everywhere. Just a sanity check.
       call utils_assert(maxval(steric_pot)==1.0_DP .and. &
            minval(steric_pot)==1.0_DP,trim(myself)//': Zero steric &
            &potential (1 steric weight) sanity check fail.', &
            minval(steric_weight),maxval(steric_weight))
    end select

    if(pub_on_root) then
       write(stdout,'(a)') ''
    end if

999 if(pub_is_steric_write) then
       call visual_scalarfield(steric_pot, grid, cell, &
            'Steric potential for PB equation, atomic units', &
            '_steric_pot')
       call visual_scalarfield(steric_weight, grid, cell, &
            'Steric weight for PB equation', &
            '_steric_weight')
    end if

    call timer_clock(myself,2)
    call utils_trace_out(myself)

  end subroutine init_steric_pot
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  subroutine boltzmann_ions_report
    !=========================================================================!
    ! Writes out a report on what Boltzmann ions are present in the system.   !
    ! ----------------------------------------------------------------------- !
    ! Arguments:                                                              !
    !   None.                                                                 !
    ! ----------------------------------------------------------------------- !
    ! Written by Jacek Dziedzic in 07/2014.                                   !
    !=========================================================================!

    use comms, only: pub_on_root
    use constants, only: stdout, CRLF, CONC_MOL_PER_L_TO_AU
    use rundat, only: pub_is_pbe
    use utils, only: utils_assert

    implicit none

    ! jd: Local variables
    integer :: i

    ! -------------------------------------------------------------------------

    call utils_assert(pub_is_pbe /= 'NONE', &
         'boltzmann_ions_report() must only be called when pub_is_pbe is not &
         &''none''.')

    if(boltzmann_ions_reported) return

    if(pub_on_root) then
       write(stdout,'(a)') &
            '----------------------------------------------------------'&
            //CRLF//&
            '|           Boltzmann ions in implicit solvent:          |'&
            //CRLF//&
            '+--------------------------------------------------------+'&
            //CRLF//&
            '|       Name | Charge |  Conc. (mol/L)  |  Conc. (a.u.)  |'
       do i=1, n_boltzmann_ions
          write(stdout,'(a2,a10,a2,f7.3,a6,e11.5,a3,e14.5,a3)') '| ', &
               adjustr(boltzmann_ions(i)%name), ' |',boltzmann_ions(i)%charge,&
               ' |    ',&
               boltzmann_ions(i)%concentration / CONC_MOL_PER_L_TO_AU,'  |', &
               boltzmann_ions(i)%concentration,'  |'
       end do
       write(stdout,'(a)') '---------------------------------------------------&
            &-------'
    end if

    boltzmann_ions_reported = .true.

  end subroutine boltzmann_ions_report
  !>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

end module is_solvation_boltzmann
