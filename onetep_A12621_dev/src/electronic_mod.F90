! -*- mode: F90 ; mode: font-lock ; column-number-mode: true -*-
!================================================================!
!                                                                !
!            Electronic energy optimisation module               !
!                                                                !
! This module contains routines used in the optimisation of the  !
! electronic energy with respect to the NGWFs and the density    !
! kernel.                                                        !
!----------------------------------------------------------------!
! Originally written by Chris-Kriton Skylaris in 2000.           !
! Subsequent modifications by Chris-Kriton Skylaris,             !
! Arash A. Mostofi, Peter D. Haynes and Nicholas D.M. Hine.      !
! Modified by Nicholas Hine to use the NGWF_REP container for    !
! NGWF-related data and matrices, October 2010.                  !
!================================================================!


module electronic

  use constants, only: DP
  use rundat, only: pub_debug_on_root

  implicit none

  private

  public :: electronic_energy
  public :: electronic_lagrangian

contains


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!



  real(kind=DP) function electronic_energy(denskern, pur_denskern, ham, &
       lhxc_fine, mu, edft, rep, ngwf_basis, hub_proj_basis, hub, &
       mdl, lnv_threshold, current_maxit_lnv, kernel_update, conv_status)

    !=============================================================!
    ! This function, given a set of NGWFs, returns the total      !
    ! energy after first optimising the density kernel in the     !
    ! LNV scheme (if required).                                   !
    !-------------------------------------------------------------!
    ! Written by Chris-Kriton Skylaris in June 2003.              !
    ! Spin polarised by Peter Haynes, July 2006                   !
    ! Modified for NLCC by Nicholas Hine, January 2009            !
    ! DFT+U added by David O'Regan, April 2008                    !
    ! Adapted for SPAM3 and function basis by Nicholas Hine,      !
    ! May-July 2009.                                              !
    ! Adapted for NGWF_REP by Nicholas Hine in October 2010.      !
    ! Modified to calculated the conduction energy by Laura       !
    ! Ratcliff in October 2010.                                   !
    ! Kernel DIIS added by Alvaro Ruiz Serrano, November 2010.    !
    ! EDA SCF-MI added by Max Phipps, November 2015.              !
    !=============================================================!

    use constants, only: DP, max_spins, paw_en_size
    use ensemble_dft, only: EDFT_MODEL, edft_calculate, edft_fermi_level, &
         edft_reorthogonalise_mo, edft_matrix_from_eigs, edft_diag_ngwf_ham
    use function_basis, only: FUNC_BASIS
    use fragment_scfmi, only: scfmi_construct_nonorth_kernel, denskern_R
    use hamiltonian, only: hamiltonian_dens_dep_matrices, &
         hamiltonian_build_matrix
    use hubbard_build, only: HUBBARD_MODEL
    use kernel, only: DKERN, kernel_purify, kernel_rescale, kernel_occ_check
    use kernel_diis, only: kernel_diis_calculate, &
         kernel_diis_build_idemp_dkn
    use lnv, only: lnv_denskernel_optimise_cg, lnv_calculate_mu
    use model_type, only: MODEL
    use ngwf_representation, only: NGWF_REP, NGWF_HAM
    use rundat, only: exact_lnv, pub_cond_calculate, pub_kernel_diis, &
         pub_eda_scfmi, &
         pub_edft, pub_edft_maxit, pub_maxit_kernel_occ_check, &
         pub_edft_smearing_width, pub_num_spins, pub_spin_fac, pub_foe, &
         pub_num_kpoints, PUB_1K
    use sparse, only: SPAM3, sparse_trace, sparse_copy, sparse_scale, &
         sparse_create, sparse_destroy, sparse_transpose, sparse_product
    use sparse_array, only: sparse_array_copy
    use utils, only: utils_assert, utils_alloc_check, utils_dealloc_check

    implicit none

    ! Arguments (inputs)
    type(NGWF_REP), intent(in) :: rep
    type(FUNC_BASIS), intent(in) :: ngwf_basis
    type(FUNC_BASIS), intent(in) :: hub_proj_basis
    type(HUBBARD_MODEL), intent(inout) :: hub
    type(MODEL), intent(in) :: mdl
    real(kind=DP), intent(in) :: lnv_threshold
    integer, intent(in) :: current_maxit_lnv
    logical, intent(in) :: kernel_update

    ! Arguments (outputs)
    type(DKERN), intent(inout) :: denskern
    type(DKERN), intent(inout) :: pur_denskern
    type(NGWF_HAM), intent(inout) :: ham
    real(kind=DP), intent(inout) :: lhxc_fine(mdl%fine_grid%ld1, &
         mdl%fine_grid%ld2, mdl%fine_grid%max_slabs12, pub_num_spins)
    real(kind=DP), intent(inout) :: mu(pub_num_spins, pub_num_kpoints)
    type(EDFT_MODEL), intent(inout) :: edft
    integer, optional, intent(out) :: conv_status

    ! Local variables
    real(kind=DP) :: hubbard_energy
    real(kind=DP) :: lhxc_energy
    real(kind=DP) :: paw_sphere_energies(paw_en_size)
    integer :: is
    integer :: cvstat
    integer :: occ_check_iter
    logical :: kernel_reset
    logical :: ham_update
    type(SPAM3),allocatable :: tmp_spam(:)
    integer :: ierr

    ! jme: KPOINTS_DANGER
    ! Some parts of this subroutine enforce a single k-point.
    call utils_assert(pub_num_kpoints == PUB_1K, &
         'Function electronic_energy not ready yet for more&
         & than one k-point.')

    ! Old version: smmd: initialise convergence status to -1
    ! dhpt: default should be that convergence is not reached
    ! dhpt: need to check if this works in all situations
    !if (present(conv_status)) conv_status = -1
    if (present(conv_status)) conv_status = 1

    ! ndmh: energy evaluation for COND calculations
    if (pub_cond_calculate) then

       if ((current_maxit_lnv > 0).and.(.not.pub_kernel_diis).and.(.not.pub_edft)) then

          if (kernel_update) then

             ! ndmh: loop for kernel occupancy checking if required
             occ_check_iter = 1
             do

                call lnv_denskernel_optimise_cg( &
                     denskern, pur_denskern, ham, lhxc_fine, mu, &
                     electronic_energy, rep, ngwf_basis, hub_proj_basis, hub, &
                     mdl, lnv_threshold, current_maxit_lnv, &
                     cvstat)

                ! ndmh: quit if we have already done more cycles than required
                if (occ_check_iter > pub_maxit_kernel_occ_check) &
                   exit

                ! ndmh: kernel not reset unless kernel_occ_check says so for at
                ! ndmh: least one spin
                kernel_reset = .false.
                if (pub_maxit_kernel_occ_check>0) then
                   ! ndmh: compare occupancy of kernel to that based on
                   ! ndmh: canonical purification
                   call kernel_occ_check(denskern%kern, kernel_reset, &
                        ham%ham, rep%overlap, rep%inv_overlap, rep%n_occ)
                end if

                ! ndmh: no need to continue if kernel was not reset. If it was, go
                ! ndmh: round again and re-do LNV.
                if (.not.kernel_reset) exit
                occ_check_iter = occ_check_iter + 1

             end do

          end if

          ! smmd: If lnv_threshold is reached then conv_status = 0,
          !       else conv_status .gt. 0
          if (present(conv_status)) conv_status = cvstat

          ! cks: purify denskern and scale before Lagrangian evaluation
          call kernel_purify(pur_denskern%kern, denskern, rep%overlap, &
               rep%inv_overlap, rep%n_occ)

          ! Set normalisation factor for revised LNV
          if (exact_lnv) call kernel_rescale(pur_denskern, rep%overlap, &
               rep%n_occ, silent=.true.)

       else if (pub_kernel_diis.or.(current_maxit_lnv < 0)) then

          ! ars: build idempotent kernel - scale appropriately
          call kernel_diis_build_idemp_dkn(pur_denskern%kern%m(:,PUB_1K), ham%ham, &
               rep%overlap, rep%n_occ)

       else if (pub_edft) then

          ! KPOINTS_DANGER: we are only considering 1 k-point here!!
          ! ars: if edft -> build kernel with smeared occupancies
          do is = 1, pub_num_spins

             ! ars: diagonalise ham and store e_i and M^\alpha_i
             call edft_diag_ngwf_ham(ham%ham(is), rep%overlap, edft%num, &
                  edft%mo(is), edft%h_evals(:,is))

             ! ars: check orthogonality of MOs and re-orthogonalise if necessary
             call edft_reorthogonalise_mo(edft%mo(is), denskern%kern%m(is,PUB_1K), &
                  ham%ham(is), rep%overlap, edft%num, edft%orth_residue(is))

             ! ars: build smeared occupancies (diagonal)
             call edft_fermi_level(edft%occ(:,is), edft%fermi(is), &
                  edft%integrated_ne(is), edft%h_evals(:,is), rep%n_occ(is,PUB_1K), &
                  edft%num, edft%nbands, pub_edft_smearing_width)

             ! ars: build density kernel
             call edft_matrix_from_eigs(denskern%kern%m(is,PUB_1K), edft%mo(is), &
                  edft%occ(:,is), edft%num)
          end do

          call sparse_array_copy(pur_denskern%kern, denskern%kern)

       end if

       ! lr408: Take trace of kernel with the projected Hamiltonian
       ! KPOINTS_DANGER: only 1 k-point considered!!
       electronic_energy = 0.0_DP
       do is = 1, pub_num_spins
          electronic_energy = electronic_energy + &
               sparse_trace(ham%ham(is), pur_denskern%kern%m(is,PUB_1K)) * pub_spin_fac
       end do

       ! rab207: print total energy obtained with final kernel
       ! only print if kernel has been updated
       if (kernel_update) call internal_print_final_state

       ! ndmh: skip the rest of this routine (non-COND code)
       return

    end if

    ! cks: optimise density kernel L using an LNV variant
    if ((current_maxit_lnv > 0).and.(kernel_update)&
         .and.(.not.pub_kernel_diis).and.(.not.pub_edft)) then

       ! ndmh: loop for kernel occupancy checking if required
       occ_check_iter = 1
       do

          call lnv_denskernel_optimise_cg( &
               denskern, pur_denskern, ham, lhxc_fine, mu, &
               electronic_energy, rep, ngwf_basis, hub_proj_basis, hub, &
               mdl, lnv_threshold, current_maxit_lnv, &
               cvstat)

          ! ndmh: quit if we have already done more cycles than required
          if (occ_check_iter > pub_maxit_kernel_occ_check) exit

          ! ndmh: kernel not reset unless kernel_occ_check says so for at
          ! ndmh: least one spin
          kernel_reset = .false.
          if (pub_maxit_kernel_occ_check>0) then
             ! ndmh: ccompare occupancy of kernel to that based on
             ! ndmh: canonical purification
             call kernel_occ_check(denskern%kern, kernel_reset, &
                  ham%ham, rep%overlap, rep%inv_overlap, rep%n_occ)
          end if

          ! ndmh: no need to continue if kernel was not reset. If it was, go
          ! ndmh: round again and re-do LNV.
          if (.not.kernel_reset) exit
          occ_check_iter = occ_check_iter + 1

       end do

       ! smmd: If lnv_threshold is reached then conv_status = 0,
       !       else conv_status .gt. 0
       if (present(conv_status)) conv_status = cvstat

       ! cks: purify denskern and scale before Lagrangian evaluation
       call kernel_purify(pur_denskern%kern, denskern, rep%overlap, &
            rep%inv_overlap, rep%n_occ)

       ! Set normalisation factor for revised LNV
       if (exact_lnv) call kernel_rescale(pur_denskern,rep%overlap, &
            rep%n_occ,silent=.true.)

    else if (pub_kernel_diis.and.kernel_update) then
       ! ars: optimise the density kernel using kernel DIIS

       electronic_energy = 0.0_DP
       call kernel_diis_calculate(electronic_energy, mu, pur_denskern%kern, &
            denskern%kern, ham, lhxc_fine, ngwf_basis, hub_proj_basis, hub, &
            rep, mdl)

    else if (pub_edft.and.kernel_update) then
       ! ars: optimise the density kernel using ensemble-DFT

       electronic_energy = 0.0_DP
       call edft_calculate(edft, denskern%kern, ham, rep, lhxc_fine, &
            ngwf_basis, hub_proj_basis, hub, mdl, pub_edft_maxit)
       call sparse_array_copy(pur_denskern%kern, denskern%kern)

       ! ars: we are minimising the free energy functional
       electronic_energy = edft%free_energy

    else ! ndmh: no density kernel optimisation

       ! ars: set flag for ham update
       ham_update = .false.
       if ((pub_kernel_diis).or. &
            ((current_maxit_lnv==0).and.kernel_update) .or. &
             (pub_eda_scfmi .eqv. .true.)) ham_update = .true.

       if ((.not.pub_kernel_diis).and.(.not.pub_edft)) then

          ! cks: purify denskern
          call kernel_purify(pur_denskern%kern, denskern, rep%overlap, &
               rep%inv_overlap, rep%n_occ, fixed_denskern=.true.)

          ! Set normalisation factor for revised LNV
          if (exact_lnv) call kernel_rescale(pur_denskern, rep%overlap, &
               rep%n_occ, silent=.true.)

       else if (pub_kernel_diis) then

          ! ars: rescale density kernel and pur_denskern
          call kernel_rescale(denskern, rep%overlap, rep%n_occ)
          call sparse_array_copy(pur_denskern%kern, denskern%kern)

       else if (pub_edft) then !(ars: assumes ngwf_cg_rotate=T)
          if(pub_foe) then
             allocate(tmp_spam(2),stat=ierr)
             call utils_alloc_check('electronic_energy','tmp_spam',ierr)
             call sparse_create(tmp_spam(1),ham%ham(1), edft%rot)
             call sparse_create(tmp_spam(2),edft%rot)
          end if


          ! ars: rescale rotated kernel, copy to pur_denskern and rebuild Hamiltonian
          call kernel_rescale(denskern, rep%overlap, rep%n_occ)
          call sparse_array_copy(pur_denskern%kern, denskern%kern)
          do is = 1, pub_num_spins
             if(.not.pub_foe) then
                ! ars: rebuild the Hamiltonian
                call edft_matrix_from_eigs(ham%ham(is), edft%mo(is), &
                     edft%h_evals(:,is), edft%num, rep%overlap)
             else
!                edft%rot^T * ham * edft%rot
!                call sparse_product(tmp_spam(1), ham%ham(is), edft%rot)
!                call sparse_transpose(tmp_spam(2), edft%rot)
!                call sparse_product(ham%ham(is), tmp_spam(2), tmp_spam(1))
                call sparse_product(tmp_spam(1), edft%old_ham(is), edft%rot)
                call sparse_transpose(tmp_spam(2), edft%rot)
                call sparse_product(ham%ham(is), tmp_spam(2), tmp_spam(1))
             end if
          end do

          if(pub_foe) then
             call sparse_destroy(tmp_spam(1))
             call sparse_destroy(tmp_spam(2))
             deallocate(tmp_spam,stat=ierr)
             call utils_dealloc_check('electronic_energy','tmp_spam',ierr)
          end if
       end if

       ! mjsp: If fragment SCF-MI
       if (pub_eda_scfmi) then

          ! mjsp: update denskern_R: proper representation of the density kernel in the
          ! full overlap
          call scfmi_construct_nonorth_kernel(pur_denskern%kern,rep)

          ! calculate the density using the denskern_R kernel
          call hamiltonian_dens_dep_matrices(ham, lhxc_fine, &
               electronic_energy, lhxc_energy, hubbard_energy, &
               paw_sphere_energies, rep, ngwf_basis, &
               hub_proj_basis, hub, denskern_R%kern, mdl, &
               ham_update, lhxc_fixed=.false.)

       else

          ! ndmh: calculate density dependent energies and matrices
          ! KPOINTS_DANGER: only 1st k-point passed!!
          call hamiltonian_dens_dep_matrices(ham, lhxc_fine, &
               electronic_energy, lhxc_energy, hubbard_energy, &
               paw_sphere_energies, rep, ngwf_basis, &
               hub_proj_basis, hub, pur_denskern%kern, mdl, &
               ham_update, lhxc_fixed=.false.)

       end if

       ! ars: update Hamiltonian in case of kernel-DIIS or EDFT
       if (ham_update) call hamiltonian_build_matrix(ham,rep)

       ! ndmh: to ensure first NGWF iteration uses correct mu, we
       ! ndmh: need to recalculate it if restarting
       if (((.not.pub_kernel_diis).and.(.not.pub_edft)).and. &
            ((current_maxit_lnv==0).and.kernel_update)) then

          ! Calculate chemical potential mu for current LNV scheme
          call lnv_calculate_mu(mu, denskern, ham, rep, ngwf_basis)

       end if

       ! ars: use free energy in EDFT calculations
       if (pub_edft) then
          edft%energy = electronic_energy
          electronic_energy = edft%energy + edft%entropy
       end if

    endif

    ! rab207: print total energy obtained with final kernel
    ! only print if kernel has been updated
    if (kernel_update) call internal_print_final_state

    contains

    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    subroutine internal_print_final_state

      use constants, only: NORMAL, stdout
      use comms, only: pub_on_root
      use kernel, only: kernel_rms_err, kernel_commutator, &
           kernel_occupancy_bounds, kernel_middle_occupancy,&
           kernel_bandgap
      use rundat, only: pub_output_detail, pub_kernel_track_mid_occ, &
           pub_num_kpoints

      implicit none

      ! internal
      real(kind=DP), &
           dimension(pub_num_spins, pub_num_kpoints) :: max_occ, mid_occ, min_occ
      real(kind=DP) :: rms_occ_err
      real(kind=DP) :: commutator
      real(kind=DP) :: bandgap(pub_num_spins)
      integer :: ik, isp
      character(len=22) :: msg(pub_num_spins, pub_num_kpoints)
      logical :: is_lnv

      ! jme: KPOINTS_DANGER
      ! Some parts of this subroutine enforce a single k-point.
      call utils_assert(pub_num_kpoints == PUB_1K, &
           'Subroutine internal_print_final_state not checked yet for more&
           & than one k-point.')

      is_lnv = .not.(pub_edft.or.pub_kernel_diis)

      msg(:,:) = '(OK)'

      ! compute commutator
      commutator = kernel_commutator(denskern, ham%ham, rep%overlap, &
           rep%inv_overlap)

      ! compute bandgap and occupancy information only when LNV
      if (is_lnv) then

         ! compute estimated bandgap
         bandgap = kernel_bandgap(denskern, ham%ham, rep%overlap,&
           rep%inv_overlap)

         rms_occ_err = kernel_rms_err(denskern, rep%overlap)
         call kernel_occupancy_bounds(max_occ, min_occ, denskern, rep%overlap)
         if (pub_kernel_track_mid_occ) then
            call kernel_middle_occupancy(mid_occ, denskern, rep%overlap)
            do ik = 1, pub_num_kpoints
               do isp = 1, pub_num_spins
                  if ((mid_occ(isp,ik)>0.15_DP).and.(mid_occ(isp,ik)<0.85_DP)) &
                    msg(isp,ik) = '(possible degeneracy)'
               end do
            end do
         end if
      end if


      ! rab207: report summary of calculation
      if (pub_output_detail >= NORMAL .and. pub_on_root) then
         write(stdout,'(//a)') &
              '>>> Density kernel optimised for the current NGWF basis:'
         write(stdout,'(5x,a)') repeat('~',59)
         if (pub_edft) then
            write(stdout,'(5x,a,es22.14,a)') &
                 'Total free energy           = ', electronic_energy, '  Eh'
         else
            write(stdout,'(5x,a,es22.14,a)') &
                 'Total energy                = ', electronic_energy, '  Eh'
         endif
         if (is_lnv) then
            if (.not.pub_cond_calculate) then
               if (pub_num_spins == 1) then
                  write(stdout,'(5x,a,es12.4,a)') &
                          'Estimated bandgap           = ', bandgap(1), '  Eh'
               else
                  write(stdout,'(5x,a,es12.4,a)') &
                          'Estimated bandgap (UP spin) = ', bandgap(1), '  Eh'
                  write(stdout,'(5x,a,es12.4,a)') &
                          'Estimated bandgap (DN spin) = ', bandgap(2), '  Eh'
               end if
            else
               if (pub_num_spins == 1) then
                  write(stdout,'(5x,a,es12.4,a)') &
                          'Estimated gap above Kc      = ', bandgap(1), '  Eh'
               else
                  write(stdout,'(5x,a,es12.4,a)') &
                          'Estimated gap above Kc (UP) = ', bandgap(1), '  Eh'
                  write(stdout,'(5x,a,es12.4,a)') &
                          'Estimated gap above Kc (DN) = ', bandgap(2), '  Eh'
               end if
            end if
            write(stdout,'(5x,a,es12.4)') &
                 'RMS occupancy error         = ', rms_occ_err
         endif
         write(stdout,'(5x,a,es12.4)') &
              '[H,K] commutator            = ', commutator

         if (is_lnv) then
            ! KPOINTS_DANGER: check that the information printed out makes sense!!
            do ik = 1, pub_num_kpoints
               if (pub_num_kpoints > 1) then
                  write(stdout, '(5x, a, I3)') 'k-point number', ik
               end if
               if (pub_num_spins == 1) then
                  write(stdout,'(5x,a,f7.3,a,f7.3,a)') &
                       'Occupancy bounds            = [',min_occ(1,ik),':',max_occ(1,ik),']'
                  if (pub_kernel_track_mid_occ) then
                     write(stdout,'(5x,a,f6.3,2x,a)') &
                       'Occupancy closest to 0.5    = ', mid_occ(1,ik), trim(msg(1,ik))
                  end if
               else
                  write(stdout,'(5x,a,f7.3,a,f7.3,a)') &
                       'UP spin occupancy bounds    = [',min_occ(1,ik),':',max_occ(1,ik),']'
                  if (pub_kernel_track_mid_occ) then
                     write(stdout,'(5x,a,f6.3,2x,a)') &
                       'UP spin occ. closest to 0.5 = ', mid_occ(1,ik), trim(msg(1,ik))
                  end if

                  write(stdout,'(5x,a,f7.3,a,f7.3,a)') &
                       'DN spin occupancy bounds    = [',min_occ(2,ik),':',max_occ(2,ik),']'
                  if (pub_kernel_track_mid_occ) then
                     write(stdout,'(5x,a,f6.3,2x,a)') &
                       'DN spin occ. closest to 0.5 = ', mid_occ(2,ik), trim(msg(2,ik))
                  endif
               end if
            end do
         end if
         write(stdout,'(5x,a/)') repeat('~',59)
      end if

    end subroutine internal_print_final_state

  end function electronic_energy


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  real(kind=DP) function electronic_lagrangian(Einput, overlap, denskern, &
       ham, mu, n_occ, edft)

    !========================================================!
    ! This function returns the value of the LNV Lagrangian. !
    !--------------------------------------------------------!
    ! Written by Chris-Kriton Skylaris in June 2003.         !
    ! Spin polarised by Peter Haynes, July 2006              !
    ! Kernel DIIS by Alvaro Ruiz Serrano, November 2010.     !
    !========================================================!

    use constants, only: DP, max_spins
    use ensemble_dft, only: EDFT_MODEL, edft_lagrangian
    use kernel_diis, only: kernel_diis_lagrangian
    use rundat, only: exact_lnv, pub_kernel_diis, pub_edft, pub_num_spins, &
         pub_spin_fac
    use sparse, only: SPAM3, sparse_trace
    use utils, only: utils_abort

    implicit none

    ! Arguments
    real(kind=DP), intent(in) :: Einput
    type(SPAM3),   intent(in) :: overlap
    type(SPAM3),   intent(in) :: denskern(pub_num_spins)
    type(SPAM3),   intent(in) :: ham(pub_num_spins)
    real(kind=DP), intent(in) :: mu(max_spins)
    integer,       intent(in) :: n_occ(max_spins)
    type(EDFT_MODEL), intent(in) :: edft ! ars

    ! Local variable
    integer :: is


    ! ars: W = electronic_lagrangian
    electronic_lagrangian = Einput

    if (pub_kernel_diis) then

       ! ars: W = E(K') - f*tr[H(K'SK'-K')], where K' = [Ne/tr(KS)]*K
       call kernel_diis_lagrangian(electronic_lagrangian, overlap, denskern, &
            ham)

    else if (pub_edft) then

       ! ars: A = E -TS - tr[HKS(XSX-X)] - mu*[\sum_i f_i - Ne]
       ! ars: note that edft%energy has been updated in electronic_energy
       electronic_lagrangian = edft_lagrangian(edft, n_occ)

    elseif (exact_lnv) then

       ! ars: W = E(K'), where K' = [Ne/tr(KS)]*K
       electronic_lagrangian = electronic_lagrangian

    elseif (.not.exact_lnv) then

       ! ars: W = E(K) - f*mu*(tr(LS)-Nocc)
       do is=1,pub_num_spins
          electronic_lagrangian = electronic_lagrangian - &
               pub_spin_fac * mu(is) * (sparse_trace(denskern(is),overlap) - &
               real(n_occ(is),kind=DP))
       end do

    else

       ! ars: incorrect logic for kernel minimisation - ONETEP stop
       call utils_abort('Error in electronic_lagrangian: incorrect kernel &
            &optimisation method.')
    end if

  end function electronic_lagrangian


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


end module electronic
