#!/bin/bash --login

echo $PATH

# defaults
ngx=17
ngy=17
ngz=17
npx=1
npy=1
npz=1
agg_level=0
iter=40
nthreads=1
slice_index=-1
slice_section=-1
test_name=poisson   #laplace is deprecated
model_name=sin_eps  #laplace #sin_eps #parabolic_rho
tol=1.e-5
exename=../dl_mg_test.exe

# A string with command options
options=$@

# An array with all the arguments
arguments=($options)

# Loop index
index=0

for argument in $options
  do
    # Incrementing index
    #increases with one on $0 
    #this must be done better
    index=$((index + 1))

    # The conditions
    # check if we have a flag
    if [ "${argument:0:1}" = - ] && ((iter > 1))
    then
      case $argument in
        -g)   ngx="${arguments[index]}" 
              ngy="${arguments[index+1]}"  
              ngz="${arguments[index+2]}"  ;;
        -p)   npx="${arguments[index]}" 
              npy="${arguments[index+1]}" 
              npz="${arguments[index+2]}"  ;;
        -nth) nthreads="${arguments[index]}" ;;
        -iter) iter="${arguments[index]}"  ;;
        -exe) exename="${arguments[index]}"  ;;
        -nompi) nompi=1 ;;
        -tol) tol="${arguments[index]}"  ;;
        -agg) agg_level="${arguments[index]}"  ;;
        -model) model_name="${arguments[index]}"  ;;
          *)  echo "unknown flag $argument" ; exit  1;;
      esac
    fi
  done

echo "grid sizes: $ngx $ngy $ngz"
echo "mpi topo  : $npx $npy $npz"

nproc=$((npx * npy * npz))

cat > input << EndOfInput
# global grid sizes
$ngx $ngy $ngz

# MPI grid
$npx $npy $npz

# periodicity
F F F
# aggregation level
$agg_level
# number of iterations
$iter
# write a plane in a given section
$slice_index $slice_section
# test name (laplace poisson ...), model
$test_name $model_name
# tol
$tol
EndOfInput

# run the code
echo "runnig with $exename with $nproc processors"
if [ -n "$nthreads" ]
then
  export OMP_NUM_THREADS="$nthreads"
  echo " using OpenMP with $nthreads threads "
fi

echo "using $exename in $nproc MPI tasks"
if [ -z "$nompi" ]
then
  mpiexec -n $nproc $exename
else
  $exename
fi
