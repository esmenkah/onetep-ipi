#!/bin/bash --login

# set of tests that cover various grid size, MPI topologies
# and number of open threads accross a set of simple problems,
#see below
#
# Lucian Anton
# July 2014
#

# defaults
test_list_default="linear_phi sin_eps pbc wirex wirez pbe_sphere_hardcore_steric"
MPIEXEC=nompi
parmode=mpi+omp

for arg in "$@"
do
    case $arg in
	-model=*)
	    test_list="$(echo $arg | sed -e 's/-[a-z]*=//' -e 's/,/ /' )"
	    ;;
	-v) verbose=1 ;;
	-mpiexec=*)
	    MPIEXEC=${arg##*=}
	    ;;
        -mpiopts=*)
            MPIOPTS="${arg##*=}"
            ;;
	-par-mode=*)
	    parmode=${arg##*=}
	    ;;
	*) echo "Unknown flag $arg! quitting ..."; exit 1;;
    esac
done

# grid sizes
nx=33
ny=25
nz=41

 MPI_period="F F F"
agg_level=0
niter=30
tol=1.e-5

exe=../dl_mg_test.exe

case $parmode in
    mpi+omp)
	topolist="1-1-1 1-1-8 1-8-1 8-1-1 2-2-2"
	threadlist="1 2 3"
	;;
    omp)
	topolist="1-1-1"
	threadlist="1 2 3"
	;;
    serial)
	topolist="1-1-1"
	threadlist=1
	;;
    *) echo "Unknown parallel mode! quitting ..."; exit 1;;
esac

# global test counter
counter=0
# index for failed runs ( as collected from log file)
ifail=0
#count the numner of failed  execution commands
nrunfailed=0

# tolerace for the deviation between the parallel and serial runs
tol_par=1.e-12

#shift necessary foer PBC which uses n<D> - 1 grid points
dnx=0; dny=0; dnz=0

# test list : linear_phi sin_eps pbc pbe
if [ "$test_list" = "all" -o -z "$test_list" ]
then
    test_list="$test_list_default"
fi
echo "testing $test_list"

for model in $test_list
do
    counter_model=0
    for mpi_topo in $topolist
    do
	if [ "$model" = "pbc" ]
	then
	    dnx=1; dny=1; dnz=1
	    MPI_period="T T T"
	elif [ "$model" = "wirex" ]
	then
	    dnx=1; dny=0; dnz=0
	    MPI_period="T F F"
	elif [ "$model" = "wirez" ]
	then
	    dnx=0; dny=0; dnz=1
	    MPI_period="F F T"
	else
	    dnx=0; dny=0; dnz=0
	    MPI_period="F F F"
	fi
	np=(${mpi_topo//-/ })

	cat > input << EndOfInput
# global grid sizes
$((nx-dnx)) $((ny-dny)) $((nz-dnz))

# MPI grid
${np[0]} ${np[1]} ${np[2]}

# periodicity
$MPI_period

# aggregation level
$agg_level

# number of iterations
$niter

# write a plane in a given section
-1 -1

# test name (laplace poisson ...), model
poisson $model
# tol
$tol

# PBE temp, linearized, use steric
300.0 F T
EndOfInput
	for nth in $threadlist
	do

	    counter=$(( ++counter ))
	    counter_model=$(( ++counter_model ))

	    export DL_MG_LOG_FILE=dl_mg_test_suite_"$model"_"$mpi_topo"_t"$nth"

	    flog=$DL_MG_LOG_FILE
	    fout=out_dl_mg_suite_test.$counter

	    export OMP_NUM_THREADS=$nth
	    nproc=$((np[0] * np[1] * np[2]))

            if [ "$MPIEXEC" = nompi ]
            then
              if [ "$parmode" = mpi+omp ]
              then
                echo "inconsistentcy between MPIEXEC and parallel mode : $MPIEXEC $parmode"; exit 1
              else
                $exe  > $fout 2>&1
              fi
            else
	      $MPIEXEC -n $nproc $MPIOPTS  $exe > $fout 2>&1
            fi

	    if [  $? -ne 0  ]
	    then
		echo "failed execution with model $model, MPI $mpi_topo, threads $nth"
		((++nrunfailed))
		continue
	    fi
#collect some reference values
# was Newton iteration used ? Then we need to jump the MG report
	    have_newton=$(grep -i "Newton.*report" $flog)
	    if [ -n "$have_newton" ]
	    then
		# more elaborate version; get both values in one go
                rs_a=($(awk '/Newton.*report/,0 {if ($0 ~ /residual norm/) r=$4
                                                 if ($0 ~ /vector *norm/)  s=$3}
                              END {print r,s}' $flog))
		#residual=$(awk '/Newton.*report/,0 {if ($0 ~ /residual norm/) r= $4}' $flog)
		#solution=$(awk '/Newton.*report/,0 {if ($0 ~ /vector   norm/  print $3}' $flog)
	    else
		rs_a=($(awk '{if ($0 ~ /residual norm/) r=$4
                              if ($0 ~ /vector *norm/)  s=$3}
                             END {print r,s}' $flog))
	    fi
	    residual=${rs_a[0]}
	    solution=${rs_a[1]}
	    #residual=$(grep 'residual norm' $flog | awk '{print $4}')
	    #solution=$(grep 'vector   norm' $flog | awk '{print $3}')

# attention, failed value relies on a certail output format in the log file
	    failed=$(grep 'dl_mg_info: failed to converge' $flog)
	    if [ -n "$failed" ]
            then
               failed_list[ifail]="$model ${mpi_topo} $nth"
	       ((++ifail))
	       failed=''
	    fi
	    if (( counter_model == 1 ))
	    then
		residual_ref=${residual}
		solution_ref=${solution}
		res_diff=""
		sol_diff=""
		err_flag=""
	    else
		res_diff=$( echo $residual $residual_ref | awk '{printf("%20.12e", $1-$2)}' )
		sol_diff=$( echo $solution $solution_ref | awk '{printf("%20.12e", $1-$2)}' )
		ar=$( echo $res_diff | awk '{print ($1>0 ? $1 : -$1)}')
		as=$( echo $sol_diff | awk '{print ($1>0 ? $1 : -$1)}')
		err_flag=$( echo $ar $as | awk '{if ( $1 > '$tol_par' || $2 > '$tol_par' ) print "!!!"}')
		if [ -n "$err_flag" ]
		then
		    i=${#parallel_deviations[@]}
		    parallel_deviations[i]="$model ${mpi_topo} $nth"
		fi
	    fi

	    if [ -n "$verbose" ]
	    then
		sol_err=$(grep 'solution error' $fout | awk '{print $3}')
		echo " "
		echo "test no $counter $counter_model"
		echo "parmeters  model $model"
		echo "MPI $mpi_topo, threads $nth"
		echo "error $sol_err, residual $residual $sol_diff $res_diff $err_flag"
	    fi
	    #we might need to keep these files
	    #rm $fout $flog
	done
    done
done

if (( ifail > 0 ))
then
    echo "The folowing computations have failed"
    for ((i=0; i<ifail;++i))
    do
	echo "${failed_list[i]}"
    done
fi

if (( nrunfailed > 0 ))
then
    echo "There were $nrunfailed failed runs"
fi

if (( nrunfailed == 0 && ifail == 0))
then
    echo ""
    echo "No failed runs!"
    echo ""
fi

echo "tolerance for deviations between parallel runs: $tol_par".
echo ""

n=${#parallel_deviations[@]}
if (( n > 0))
then
    echo "The following parallel runs differ significantly from the serial version"
    for ((i=0; i<n;++i))
    do echo  ${parallel_deviations[i]}
    done
fi
