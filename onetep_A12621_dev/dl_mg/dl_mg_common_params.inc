! public parameters which are needed across solver
! putting them in a  module won't work because I want the user application
! to access dl_mg only with one 'use' statement, i.e.
!
! use  dl_mg
!
! If dl_mg would use a separate module for these parameters,
! some compilers will require that module in the include directory

! Lucian Anton August 2013
!
! update December 2014

! boundary conditions
integer, parameter :: DL_MG_BC_PERIODIC = 0, DL_MG_BC_NEWMANN = 1, DL_MG_BC_DIRICHLET = 2

! error codes
integer, parameter :: DL_MG_SUCCESS = 0

! lengths of error/info strings
integer, parameter :: DL_MG_MAX_ERROR_STRING = 511, DL_MG_VERSION_STRING_LENGTH=18
