!> data types and subroutines for error handling
module dl_mg_errors
  use dl_mg_common_data, only : DL_MG_MAX_ERROR_STRING
  implicit none
  public

!> Warning : If a new error code is added nerror must be increased by one and
!! a corressponding error message must be added in error_list array.
!! This is awkward but I cannot think of a better solution.

  integer, parameter :: DL_MG_ERR_UNSPECIFIED    = 1, & !< use for cases which
       !! have no errors codes defined
       !! or when aborting on error.
       !! Extended error message can be added via msg
       !! argument of handle_error, which is printed
       !! only if execution is aborted
       DL_MG_ERR_INIT            = 2, &
       DL_MG_ERR_NEUMANN_BC      = 3, &
       DL_MG_ERR_UNKNOWN_BC      = 4, &
       DL_MG_ERR_MPI_TOPO        = 5, &
       DL_MG_ERR_MPI_COMM_DUP    = 6, &
       DL_MG_ERR_MPI_TOPO_BC     = 7, &
       DL_MG_ERR_GRID_EMPTY      = 8, &
       DL_MG_ERR_PROLONG_MAP     = 9, &
       DL_MG_ERR_DEBYE           = 10, &
       DL_MG_ERR_NOINIT_POISSON  = 11, &
       DL_MG_ERR_NOINIT_PBE      = 12, &
       DL_MG_ERR_DERPOT          = 13, &
       DL_MG_ERR_MOD_BC          = 14, &
       DL_MG_ERR_RES_RATIO       = 15, &
       DL_MG_ERR_NITER           = 16, &
       DL_MG_ERR_NEWTON_TYPE     = 17, &
       DL_MG_ERR_NEWTON_DAMP     = 18, &
       DL_MG_ERR_NEWTON_DAMP_DERFUN = 19

     !DL_MG_ERR_SMOOTHER_NEWTON_SOR = 8, &
     !DL_MG_ERR_NEWTON_LIN          = 9, &

  integer, parameter :: nerror = 19

  type errm
     character(len=DL_MG_MAX_ERROR_STRING) m
  end type errm

  type(errm), parameter :: error_list(nerror) = (/ &
       !! [error-codes]
       ! DL_MG_ERR_UNSPECIFIED = 1
       errm("Unspecified error") , &
       ! Dl_MG_ERR_INIT = 2
       errm("dl_mg_init called before dl_mg_free"), &
       ! DL_MG_ERR_NEUMANM_BC = 3
       errm("Neumann boundary condition is not implemented"), &
       !DL_MG_ERR_UNKNOWN_BC = 4
       errm("dl_mg_init: wrong boundary condition specification &
       & in input. It must be one of : DL_MG_BC_PERIODIC, DL_MG_BC_DIRICHLET"),&
       !DL_MG_ERR_MPI_TOPO = 5
       errm("input communicator does not have a cartesian topology"), &
       !DL_MG_ERR_MPI_COMM_DUP = 6
       errm("duplicate communicator does not have a &
       &cartesian topology"), &
       !DL_MG_ERR_MPI_TOPO_BC = 7
       errm("boundary conditions inconsisten with MPI topology"),&
       ! DL_MG_ERR_GRID_EMPTY = 8
       errm("empty inner grid in set_mg_grids"),&
       ! DL_MG_ERR_PROLONG_MAP = 9
       errm("build_prolong_map: inactive site left behind"), &
       ! DL_MG_ERR_DEBYE = 10
       errm("dl_mg_init_nonlin: Debye lenght squared is negative"), &
       !DL_MG_ERR_NOINIT_POISSON = 11
       errm("dl_mg_solver_poisson is called before dl_mg_init"), &
       ! DL_MG_ERR_NOINIT_PBE = 12
       errm("dl_mg_solver_pbe called before initialisation subroutines"), &
       ! DL_MG_ERR_DERPOT = 13
       errm("dl_mg_solver_pbe: argument der_pot can be used &
       & only together with steric weight in this release"), &
       ! DL_MG_ERR_MOD_BC = 14
       errm("Dirichlet boundaries values were modified by the solver"), &
       ! DL_MG_ERR_RES_RATIO = 15
       errm("Consecutive residuals ratio is too large.&
       & It is possible that the round-off limit was reached.&
       & Check the residual ratio distribution histogram."), &
       ! DL_MG_ERR_NITER = 16
       errm("Computation failed to converge after the prescribed number of iteration"), &
       ! DL_MG_ERR_NEWTON_TYPE = 17
       errm("wrong eq_type in Newton's MG"), &
       ! DL_MG_ERR_NEWTON_DAMP = 18
       errm("compute_damping_parameter : failed to find a damping parameter for&
       & Newton method"), &
       ! DL_MG_ERR_NEWTON_DAMP_DERFUN = 19
       errm("compute_damping_parameter : functional derivative is non-negative") /)
       !! [error-codes]
  private error_list, nerror, error_abort, errm

contains

  subroutine handle_error(err_code, ierror, msg)
    use dl_mg_common_data, only : DL_MG_SUCCESS, abort_on_failure
    implicit none
    integer, intent(in) :: err_code !< error code
    integer, optional, intent(out) :: ierror !< value to be return to the caller
    character(len=*), optional, intent(in) :: msg !< suplmentary message
                                                  !! used only if the execution aborts
                                                  !! on error


    if (present(ierror) .and. .not. abort_on_failure) then
       ierror = err_code
    else
       if (err_code /= DL_MG_SUCCESS) then
          call error_abort(err_code, msg)
       endif
    endif
  end subroutine handle_error

  subroutine error_abort(error_code, msg)
    use dl_mg_mpi_header
    use dl_mg_common_data, only : mg_comm, report_unit
    implicit none
    integer, intent(in) :: error_code
    character(len=*), optional, intent(in) :: msg

    integer ierr
    character(len=DL_MG_MAX_ERROR_STRING) s

    call get_errmsg(error_code, s)
    if (present(msg)) then
       s = trim(s)//" "//msg
    endif

    write(report_unit,*) "DL_MG fatal error: "//s
    write(0,*) "DL_MG fatal error: "//s

#ifdef MPI
    call MPI_abort(mg_comm, error_code,ierr)
#else
    stop
#endif

   end subroutine error_abort

   subroutine get_errmsg(err_code, msg)
     use dl_mg_common_data, only : DL_MG_SUCCESS
     implicit none
     integer, intent(in) :: err_code
     character(len=DL_MG_MAX_ERROR_STRING), intent(out) :: msg

     integer i
     character(len=15) c

     if ( err_code < 0 .or. err_code > nerror ) then
        write(c,*) err_code
        msg=trim(adjustl(c))//": wrong error code?"
     else if (err_code == DL_MG_SUCCESS) then
        msg = "successful execution"
     else
        msg = error_list(err_code)%m
     end if
   end subroutine get_errmsg

   function check_assertion(assertion, err_code)
     implicit none

     logical, intent(in) :: assertion !< logical expression whose value is
                                      !! is returned
     integer, optional, intent(in) :: err_code !< used for testing the error reporting;
                                               !! if missing and the code is compiled
                                               !! with DL_MG_TEST_ERR_CODES the assertion
                                               !! fails with a choosen probability

     logical check_assertion

#ifdef DL_MG_TEST_ERR_CODES
     integer err_code_to_test, len, dt(8)
     character(len=63) val
     real x
#endif

     check_assertion = assertion

#ifdef DL_MG_TEST_ERR_CODES
     ! this is used to simulate an error at a selected point in
     ! the code for error testing
     call get_environment_variable("DL_MG_TEST_ERR_CODE", value=val, &
          length = len)
     if (len > 0) then
        read(val, *) err_code_to_test
        if ( present(err_code)) then
           if (err_code == err_code_to_test)  check_assertion = .false.
        else
           ! pick at random a failure point
           call date_and_time(values=dt) ! take time miliseconds
           if (mod(dt(8),10) < 1 ) check_assertion = .false.
        endif
     end if
#endif
   end function check_assertion

end module dl_mg_errors
