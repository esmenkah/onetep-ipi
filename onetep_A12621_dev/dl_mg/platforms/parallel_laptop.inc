#============================================================
# Makefile variable for laptop (gfortran + MPICH2)
#    
# Lucian Anton 
# created: 20/04/12
#============================================================

FC := mpif90
F77 := mpif77

COMP := gnu

USE_OPENMP = yes

MPIFLAGS          := -DMPI

ifdef USE_INCLUDE_MPI
  MPIFLAGS += -DUSE_INCLUDE_MPIF
endif


OMPFLAGS_gnu_yes  := -fopenmp
OMPFLAGS_gnu_no   := 
FFLAGS_gnu_opt    := -O3

FFLAGS_gnu_profile  := -DUSE_TIMER $(FFLAGS_gnu_opt)

# enable lots of prints in V cycle
ifdef RELAX_PRINT
  FRELAX_PRINT := -DDLMG_RELAX_PRINT
endif

FFLAGS_gnu_debug  :=  $(FRELAX_PRINT) -DDL_MG_TEST_ERR_CODE -O0 -g -fbounds-check -Wall -Wuninitialized -Wno-unused -finit-real=nan -ffpe-trap=invalid,zero,overflow -fsignaling-nans -fdump-core -fbacktrace

# -DDUMP_DATA -DUSE_DUMP
#FFLAGS_gnu_debug := -g -fcheck=all -Wall -finit-real=snan -ffpe-trap=invalid,zero,overflow -fsignaling-nans -fdump-core 
#FFLAGS_gnu_debug := -g -fcheck=all -Wall -ffpe-trap=invalid,zero,overflow -fsignaling-nans 

FFLAGS       := $(MPIFLAGS) $(FFLAGS_$(COMP)_$(BUILD)) $(OMPFLAGS_$(COMP)_$(USE_OPENMP))


