# wrapper for ONETEP config files
#
#Lucian Anton may 2013

include $(ONETEP_ROOT)/config/conf.$(ONETEP_ARCH)

FC := $(F90)

ifeq ($(ONETEP_TARGET),debug)
  FFLAGS += $(DEBUGFLAGS) 
else
  FFLAGS += $(OPTFLAGS) $(OPENMPFLAGS)
endif

